import os
import time
from subprocess import Popen

import pandas as pd
import paramiko
from datetime import datetime
import mysql.connector as mysql
import pyautogui
from sqlalchemy import create_engine

from utils.NetmikoGeneral import autogui_wait, autogui_double_click, autogui_right_click, autogui_left_click, \
    autogui_save_file
from utils.general import need_update
from utils.telegram_bot import telegram_send_message


def collect_ne(regional_info, passwords, restart_auto_process):
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/tx_ne_param{regional_info}'
    db_connection = create_engine(db_connection_str)
    if restart_auto_process:
        db = mysql.connect(
            host="10.26.82.160",
            user=passwords['sql_user'],
            passwd=passwords['sql_password'],
            database=f'tx_ne_param{regional_info}',
            allow_local_infile=True
        )
        cursor = db.cursor()
        exclude = 'truncate AUTOMATION_PROGRESS'
        cursor.execute(exclude)

    df = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)

    return df


def run_grep_check(regional_info, passwords, configs,restart_auto_process,vendors):

    # coleta DF existente para começar de onde parou, se restart automation = True da truncate no AUTOMATION
    df_existent = collect_ne(regional_info, passwords,restart_auto_process)

    # verifica se o BACOS: AUTOMATION_PROGRESS tem conteudo
    todo = df_existent.empty

    # caso haja sites no AUTOMATION_PROCESS verifica se é pra atualizar
    if not todo:
        todo = need_update(df_existent.iloc[0]['STARTED'])

    # executa coleta do grep baseado nas regiões passadas em config
    if todo:
        state = configs['ufs']
        print(f'============>GREP COLLECT - {state} - STARTED<============')
        telegram_send_message(f'GREP COLLECT - {state} - STARTED')
        host = passwords['gesa1_host']
        port = 22
        username = passwords['gesa1_user']
        password = passwords['gesa1_password']

        # define estados para nordeste
        if regional_info == '_other':
            ufs = ['al', 'ba', 'ce', 'ma', 'pb', 'pe', 'pi', 'rn', 'se']
            df = pd.DataFrame()
            for uf in ufs:
                command = f'grep m-br-{uf}- /etc/hosts'
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect(host, port, username, password)
                stdin, stdout, stderr = ssh.exec_command(command)
                lines = stdout.readlines()
                lines = [segments.split()[:3] for segments in lines]
                df_uf = pd.DataFrame(lines, columns=['IP', 'Name', 'Vendor'])
                df = df.append(df_uf)

        else:
            # roda grep para todos o estados definidos em config
            ufs = configs['ufs']

            Popen(f'powershell putty.exe {username}@{host} -pw {password}')

            autogui_wait(f'general\\able_terminal_1.PNG', 3600)

            autogui_double_click('general\\putty_bar_1.PNG', 10)

            basepath = os.path.dirname(__file__)

            directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))

            time.sleep(0.5)
            pyautogui.keyDown('ctrl')
            pyautogui.press('l')
            pyautogui.keyUp('ctrl')

            if autogui_right_click('general\\putty_bar_1.PNG', 10):
                pass

            time.sleep(0.5)

            if autogui_left_click('general\\clear_screen.PNG', 10):
                pass

            print(f'Grep on follows UFs: {ufs}')
            for cur_uf in ufs:
                time.sleep(0.5)

                autogui_wait(f'general\\able_terminal_1.PNG', 10)

                pyautogui.hotkey('ctrl', 'alt', 'n')

                autogui_wait('general\\notepad.PNG', 10)

                #pyautogui.typewrite(f'grep m-br-{cur_uf}- /etc/hosts')
                texto = f'cat /etc/hosts |egrep "m-br-{cur_uf}-|i-br-{cur_uf}-" '
                pyautogui.typewrite(texto)

                pyautogui.hotkey('ctrl', 'a')

                time.sleep(1)

                pyautogui.hotkey('ctrl', 'x')

                time.sleep(1)

                pyautogui.hotkey('alt', 'tab')

                autogui_right_click(f'general\\able_terminal_1.PNG', 10)

                time.sleep(1)

                pyautogui.hotkey('enter')
                time.sleep(0.5)

            autogui_wait(f'general\\able_terminal_1.PNG', 10)

            file_name = 'grep.txt'

            full_file_name = autogui_save_file(directory_path, file_name, gesa_id=1)

            autogui_wait(f'general\\able_terminal_1.PNG', 10)

            pyautogui.typewrite('exit')

            time.sleep(1)

            pyautogui.hotkey('enter')
            time.sleep(0.5)

            file = open(full_file_name, 'r')

            lines = file.readlines()

            command_grep = '-br-'

            # filtra apenas as linhas que contem a string m-br-
            filtered_lines = [x for x in lines if x.strip().__contains__(command_grep)]

            ''' TRATAMENTO PARA EXCLUIR LINHAS COM COMANDO GREP REMOVIDO, NÃO NECESSÁRIO NA VERSÃO ATUAL (travata linhas de indices, onde os blocos iniciavam e acabavam)
            all_index = [x for x, i in enumerate(lines) if command_grep in i]
            for idx, cur_index in enumerate(all_index):
                start_idx = cur_index + 1

                if cur_index == all_index[-1]:
                    cur_lines = lines[start_idx:-1]
                else:
                    cur_lines = lines[start_idx:all_index[idx + 1]]

            # comandos estavam dentro do for em all_index acima
            cur_lines = lines[0:len(all_index) - 1]
            filtered_lines.extend(cur_lines)'''


            filtered_lines = [segments.split()[:3] for segments in filtered_lines]

            df = pd.DataFrame(filtered_lines, columns=['IP', 'Name', 'Vendor'])

            file.close()

            os.remove(full_file_name)

        # >>>> filtra equipamentos pela hierarquia GWS < GWD < GWC
        #accepted_patterns = ['gwc', 'gwd', 'gws']
        accepted_patterns = ['gwc', 'gwd', 'gws', 'hl4', 'hl5', 'hl5g', 'hl5s', 'hl5d']
        pattern = '|'.join(accepted_patterns)
        df = df[df['Name'].str.contains(pattern, na=True)]
        # >>>> filtra equipamentos pela hierarquia GWS < GWD < GWC

        # >>>> filtra equipamentos pelo VENDOR
        df['Vendor'] = df['Vendor'].str.upper()
        df = df[df['Vendor'].isin(vendors)]
        df.dropna(subset=['Vendor'], inplace=True)
        # >>>> filtra equipamentos pelo VENDOR

        # >>>> INSERE DATA NO DATAFRAME
        now = datetime.now().date().strftime("%Y-%m-%d")
        df['DATE'] = now
        df.drop_duplicates(inplace=True)

        # >>>> GERANDO QUERY PARA INSERIR ELEMENTOS COLETADOS PELO GREP NA TABELA AUTOMATION_PROGRESS
        query = f'INSERT INTO AUTOMATION_PROGRESS (IP, NE_NAME, VENDOR, STARTED) VALUES'
        for index, row in df.iterrows():
            query = query + f'("{row["IP"]}","{row["Name"]}", "{row["Vendor"]}", "{now}"), '

        query = query[:-2]

        db = mysql.connect(
            host="10.26.82.160",
            user=passwords['sql_user'],
            passwd=passwords['sql_password'],
            database=f'tx_ne_param{regional_info}',
            allow_local_infile=True
        )

        cursor = db.cursor()

        # >>>> LIMPANDO TABELA EM CASO DE ATUALIZAÇÃO
        exclude = 'truncate AUTOMATION_PROGRESS'
        cursor.execute(exclude)

        # >>>> INSERINDO NOVOS ELEMENTOS NA TABELA
        cursor.execute(query)
        db.commit()

        # >>>> GERANDO CSV PARA SUBIR NA TABELA NE_GREP
        basepath = os.path.dirname(__file__)
        directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
        csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
        grep_file = os.path.abspath(os.path.join(csv_path, f'grep_table.csv'))
        now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        grep_done_file = os.path.abspath(os.path.join(csv_path, 'done', f'grep_table{now}.csv'))
        df.to_csv(grep_file, mode='a', header=False, index=False)
        upload_grep_file = grep_file.replace('\\', '/')

        # >>>> SUBINDO CSV NA TABELA NE_GREP
        sql = f'LOAD DATA LOCAL INFILE "{upload_grep_file}" REPLACE INTO TABLE NE_GREP\n' \
            f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
            f'LINES TERMINATED BY \'\r\n\';'

        cursor.execute(sql)
        db.commit()

        # LIMPANDO ELEMENTOS NA TABELA NE_GREP COM MAIS DE 3 MESES
        sql = f'DELETE FROM NE_GREP WHERE DATE < DATE_SUB(NOW(), INTERVAL 3 MONTH);'
        cursor.execute(sql)
        db.commit()

        if os.path.exists(grep_done_file):
            os.remove(grep_done_file)
        os.rename(grep_file, grep_done_file)
        print('============>GREP COLLECT FINISHED<============')
        telegram_send_message('GREP COLLECT FINISHED!')

