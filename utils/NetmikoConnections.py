from netmiko import redispatch
import time


def netmiko_ssh(gesa, jump_server_connection, passwords, ip, vendor, tacacs=True):
    '''
    Função responsável pela tentativa de conexão via ssh nas caixas

    :param gesa: id do jump_server que estamos conectado
    :param jump_server_connection: obj da conexão netmiko com o jump_server
    :param passwords: dict com usuário e senha para conexão
    :param ip: ip da caixa que vamos conectar
    :param vendor: qual o vendor para definir user e senha sem tacacs
    :param tacacs: boolean que define se iremos conectar por tacacs ou sem.
    :return: retona a conexão e o nome do vendor verificado

    '''

    vendor = vendor.lower()
    # define user e password
    if tacacs:
        user = passwords[f'gesa{gesa}_user']
        password = passwords[f'gesa{gesa}_password']
    else:
        user = passwords[f'{vendor}_user']
        password = passwords[f'{vendor}_password']

    real_vendor = None
    try:
        try:
            output = jump_server_connection.send_command(f"ssh {user}@{ip}",
                                                         expect_string=r'\)\?|d:|\$|er:')
        except:
            jump_server_connection.write_channel(chr(3))
            return None, real_vendor

        # verifica a tela
        screen = get_screen(output)

        if screen == 'Connection refused':
            return None, real_vendor

        elif screen == 'failed':
            return None, real_vendor

        elif screen == 'yes_no':
            try:
                output += jump_server_connection.send_command('yes', expect_string=r'd:')
                output = jump_server_connection.send_command(f"{password}",
                                                             expect_string=r'\)\?|word:|\$|\>|#', cmd_verify=False)
                screen = get_screen(output)
                if screen == 'request failed':
                    return None, real_vendor

            except:
                jump_server_connection.write_channel(chr(3))
                return None, real_vendor
            # check vendor
            if '>' in output:
                if 'oriant' in output:
                    real_vendor = 'TELLABS'
                    return 'SSH', real_vendor
                else:
                    real_vendor = 'HUAWEI'
                    return 'SSH', real_vendor
            elif '#' in output:
                # verifica se é nokia, alcatel ou cisco
                output = jump_server_connection.send_command('show version', expect_string=r'#', cmd_verify=False)
                if 'Cisco' in output:
                    real_vendor = 'CISCO'
                    return 'SSH', real_vendor
                elif 'Nokia' in output:
                    real_vendor = 'NOKIA'
                    return 'SSH', real_vendor
                elif 'ALCATEL' in output:
                    real_vendor = 'ALCATEL'
                    return 'SSH', real_vendor
                else:
                    return 'SSH', 'UNKNOWN'
            else:
                jump_server_connection.write_channel(chr(3))
                return None, real_vendor

        elif screen == 'password':
            try:
                output = jump_server_connection.send_command(f"{password}",
                                                             expect_string=r'\)\?|word:|\$|\>|#|er:|\(es\)',
                                                             cmd_verify=False)
            except:
                jump_server_connection.write_channel(chr(3))
                return None, real_vendor

            screen = get_screen(output)
            if screen == 'failed':
                return None, real_vendor

            if screen == 'answer':
                output = jump_server_connection.send_command('y', expect_string=r'\)\?|word:|\$|\>|#|er:',
                                                             cmd_verify=False)

            if screen == 'password':
                jump_server_connection.write_channel(chr(3))
                return None, real_vendor

            # check vendor
            if '>' in output:
                if 'oriant' in output:
                    real_vendor = 'TELLABS'
                    return 'SSH', real_vendor
                else:
                    real_vendor = 'HUAWEI'
                    return 'SSH', real_vendor
            elif '#' in output:
                # verifica se é nokia, alcatel ou cisco
                output = jump_server_connection.send_command('show version', expect_string=r'#')
                if 'Cisco' in output:
                    real_vendor = 'CISCO'
                    return 'SSH', real_vendor
                elif 'Nokia' in output:
                    real_vendor = 'NOKIA'
                    return 'SSH', real_vendor
                elif 'ALCATEL' in output:
                    real_vendor = 'ALCATEL'
                    return 'SSH', real_vendor
                else:
                    return 'SSH', 'UNKNOWN'

        else:
            jump_server_connection.write_channel(chr(3))
            return None, real_vendor

    except Exception as e:
        raise e


def get_screen(output):
    '''

    Função que recebe o output da tentativa de conexão e retorna qual tela temos no
    prompt

    :param output: string com o retorno da leitura do prompt após conexão na caixa
    :return: retorna o nome da tela que se encontra

    '''
    screen_dict = {')?': 'yes_no',
                   'assword:': 'password',
                   'nswer:': 'answer',
                   'in:': 'login',
                   'name:': 'username',
                   'refused': 'Connection refused',
                   'failed': 'failed'}
    for inner_screen in screen_dict:
        if inner_screen in output:
            return screen_dict[inner_screen]

    return None


def netmiko_telnet(gesa, jump_server_connection, passwords, ip, ne_name, vendor, tacacs=True):
    '''
    Função responsável pela tentativa de conexão via telnet nas caixas

    :param gesa: id do jump_server que estamos conectado
    :param jump_server_connection: obj da conexão netmiko com o jump_server
    :param passwords: dict com usuário e senha para conexão
    :param ip: ip da caixa que vamos conectar
    :param ne_name: nome da caixa para criar padrão para busca do terminal
    :param vendor: qual o vendor para definir user e senha sem tacacs
    :param tacacs: boolean que define se iremos conectar por tacacs ou sem.
    :return: retona a conexão e o nome do vendor verificado

    '''
    vendor = vendor.lower()
    # define user e password
    if tacacs:
        user = passwords[f'gesa{gesa}_user']
        password = passwords[f'gesa{gesa}_password']
        jump_server_connection.username = user
        jump_server_connection.password = password
    else:
        user = passwords[f'{vendor}_user']
        password = passwords[f'{vendor}_password']
        jump_server_connection.username = user
        jump_server_connection.password = password

    # define padrôes para login
    accepted_patterns = [f'{ne_name}>', f'{ne_name}#', '--More--']
    pattern = '|'.join(accepted_patterns)

    real_vendor = None
    try:
        try:
            # roda comando manualmente
            command = f"telnet {ip} \r\n"
            jump_server_connection.write_channel(command)
            output = jump_server_connection.telnet_login()

        except Exception as e:
            jump_server_connection.write_channel(chr(3))
            jump_server_connection.write_channel(chr(26))
            return None, real_vendor

        # check vendor
        if '>' in output:
            if 'oriant' in output:
                real_vendor = 'TELLABS'
                return 'TELNET', real_vendor
            else:
                real_vendor = 'HUAWEI'
                return 'TELNET', real_vendor
        elif '#' in output:
            # verifica se é nokia ou cisco
            jump_server_connection.write_channel('show version' + '\r\n')
            time_out = 10
            while time_out > 0:
                try:
                    time.sleep(1)
                    output = jump_server_connection.read_until_pattern(pattern, read_timeout=20)
                    break
                except:
                    time_out -= 1

            if 'Cisco' in output:
                real_vendor = 'CISCO'
            elif 'Nokia' in output:
                real_vendor = 'NOKIA'
            elif 'ALCATEL' in output:
                real_vendor = 'ALCATEL'
            else:
                real_vendor = 'unknown'

            # se a tela for More
            while True:
                if '--More--' in output:
                    jump_server_connection.write_channel(chr(3))
                    jump_server_connection.write_channel(chr(26))
                    output = jump_server_connection.read_until_pattern(pattern)
                else:
                    break

            return 'TELNET', real_vendor

        else:
            jump_server_connection.write_channel('logout \r\n')
            jump_server_connection.write_channel('quit \r\n')
            jump_server_connection.write_channel(chr(3))
            jump_server_connection.write_channel(chr(26))
            return None, real_vendor


    except Exception as e:
        raise e
