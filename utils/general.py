import logging
import os
from datetime import datetime
import re
import time

import pandas as pd
import wexpect
from sqlalchemy import create_engine
import mysql.connector as mysql


def replace(old, new, str, caseinsentive=False):
    if caseinsentive:
        return str.replace(old, new)
    else:
        return re.sub(re.escape(old), new, str, flags=re.IGNORECASE)


def need_update(value):

    # caso receba none em value
    if not value:
        return True

    #verifica se o ultimo grep foi a 15 dias ou mais
    else:
        now = datetime.now().date()
        d = now - value
        if d.days >= 15:
            return True
        else:
            return False


def get_full_mac_table_from_csv(file_path):
    df_existent_mac = pd.read_csv(file_path, header=None)
    df_existent_mac.columns = ['NE_NAME', 'INTERFACE', 'IP_LOCAL', 'MAC_LOCAL', 'IP_RECEIVED', 'MAC_RECEIVED', 'VRF',
                               'DATE']
    # patterns = {'GE': 'GigabitEthernet'}
    # df_existent_mac = df_existent_mac.replace({'INTERFACE': patterns}, regex=True)
    df_existent_mac['INTERFACE'] = df_existent_mac['INTERFACE'].str.replace('GE', 'GigabitEthernet')
    df_existent_mac['IP_NEXT_HOP'] = ''
    df_existent_mac['MAC_NEXT_HOP'] = ''
    df_existent_mac['NE_NAME_NEXT_HOP'] = ''
    df_existent_mac['INTERFACE_NEXT_HOP'] = ''
    df_existent_mac['COST'] = ''
    df_existent_mac['PEER'] = ''
    df_existent_mac['PREFERENTIAL'] = ''
    df_existent_mac = df_existent_mac[['NE_NAME', 'INTERFACE', 'IP_LOCAL', 'MAC_LOCAL', 'IP_RECEIVED',
                                       'MAC_RECEIVED', 'NE_NAME_NEXT_HOP', 'INTERFACE_NEXT_HOP', 'IP_NEXT_HOP',
                                       'MAC_NEXT_HOP', 'COST', 'PEER', 'PREFERENTIAL', 'VRF', 'DATE']]
    for index, row in df_existent_mac.iterrows():
        received_mac = row['MAC_RECEIVED']
        received_ip = row['IP_RECEIVED']
        local_mac = row['MAC_LOCAL']
        local_ip = row['IP_LOCAL']
        local_ne_name = row['NE_NAME']
        local_interface = row['INTERFACE']
        index_list = list(df_existent_mac[(df_existent_mac['MAC_LOCAL'] == received_mac) & (
                df_existent_mac['IP_LOCAL'] == received_ip)].index)
        if index_list:
            for current_index in index_list:
                df_existent_mac.at[current_index, 'IP_NEXT_HOP'] = local_ip
                df_existent_mac.at[current_index, 'MAC_NEXT_HOP'] = local_mac
                df_existent_mac.at[current_index, 'NE_NAME_NEXT_HOP'] = local_ne_name
                df_existent_mac.at[current_index, 'INTERFACE_NEXT_HOP'] = local_interface

    return df_existent_mac


def send_sql_commit(sql, cursor, regional_info, passwords):
    try:
        cursor.execute(sql)
        return cursor
    except:
        try:
            hr = datetime.now().strftime('%H:%M:%S')
            print(f'Some Error Occurred! >> | SQL connection error << ({hr})')
            db = mysql.connect(
                host="10.26.82.160",
                user=passwords['sql_user'],
                passwd=passwords['sql_password'],
                database=f'tx_ne_param{regional_info}',
                allow_local_infile=True
            )

            db.autocommit = True

            cursor = db.cursor()

            cursor.execute(sql)
            return cursor
        except:
            print(f'Some Error Occurred! >> | SQL connection error - attempt 2 << ({hr})')
            print(f'SQL query: {sql}')
            return None


def restart_automation(passwords):
    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database='tx_ne_param',
        allow_local_infile=True
    )

    cursor = db.cursor()

    #cursor.execute("SELECT STARTED FROM tx_ne_param.AUTOMATION_PROGRESS ORDER BY STARTED DESC LIMIT 1;")
    cursor.execute("truncate tx_ne_param.AUTOMATION_PROGRESS;")

    #row = cursor.fetchall()
    #date_started = str(row[0][0])

    #cursor.execute(f"UPDATE tx_ne_param.AUTOMATION_PROGRESS SET STARTED = '2021-01-01' "
    #               f"WHERE STARTED = '{date_started}';")

    db.commit()


def get_last_dated_df(df):
    df_ne_names = list(set(df['NE_NAME'].to_list()))

    for cur_ne_name in df_ne_names:
        cur_last_date = df[df['NE_NAME'] == cur_ne_name].iloc[0]['DATE']
        df = df.loc[~((df['NE_NAME'] == cur_ne_name) & (df['DATE'] != cur_last_date))]

    return df


def get_site_name(row):
    site_name = row['EquipB'].split('_')[0]
    if len(site_name) == 5:
        site_name = site_name[1:-1]
    elif len(site_name) == 0:
        site_name = 'NO_SITE_NAME'
    else:
        site_name = row['EquipB']

    return site_name


def remove_strokes(row, column):
    if row[column][0] == '-':
        return row[column][1:].strip()
    elif row[column][-1] == '-':
        return row[column][:-2].strip()
    else:
        return row[column].strip()


def remove_parentheses(row, column):
    if '(' in row[column]:
        result = row[column]
        result = result.split('(')[0].strip()
        return result
    else:
        return row[column].strip()


def remove_parentheses_string(site_name):
    if not site_name:
        return 'ERROR'
    if '(' in site_name:
        result = site_name
        result = result.split('(')[0].strip()
        return result
    else:
        return site_name.strip()


def remove_strokes_string(site_name):
    if not site_name:
        return 'ERROR'
    if site_name[0] == '-':
        return site_name[1:].strip()
    elif site_name[-1] == '-':
        return site_name[:-2].strip()
    else:
        return site_name.strip()


def validate_link(source, target):
    if source == '':
        return False
    elif source == '-':
        return False
    elif target == '':
        return False
    elif target == '-':
        return False
    elif source[:3].upper() == 'RNC':
        return False
    elif target[:3].upper() == 'RNC':
        return False
    elif ('M-BR-' in source.upper()) and ('M-BR-' in target.upper()):
        return False
    elif not any([('/' in source), ('/' in target), ('_' in source), ('_' in target)]):
        return False
    elif target == source:
        return False
    else:
        return True


def get_media_tx(row):
    media_tx = 'Fiber'
    if '/' in row['SOURCE_ELEMENT'] or '_' in row['SOURCE_ELEMENT']:
        source_radio = True
    else:
        source_radio = False

    if '/' in row['TARGET_ELEMENT'] or '_' in row['TARGET_ELEMENT']:
        target_radio = True
    else:
        target_radio = False

    if source_radio and target_radio:
        if row['SOURCE_ELEMENT'].split('/')[0] == row['TARGET_ELEMENT'].split('/')[0]:
            media_tx = 'Fiber'
        else:
            media_tx = 'Microwave'
    else:
        media_tx = 'Fiber'

    return media_tx


def get_attribute(lines, attr):
    attr_result = [x for x in lines if attr in x][0]
    attr_result = attr_result.strip().split()[-1].strip()

    return attr_result


def get_passwords():
    '''define pasta padrão com arquivos de config, le e salva atributos nas variáveis'''
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    general_path = os.path.abspath(os.path.join(directory_path, 'general'))
    file = os.path.abspath(os.path.join(general_path, 'passwords.txt'))

    file = open(file, 'r')
    lines = file.readlines()

    gesa1_host = get_attribute(lines, 'gesa1_host')

    gesa2_host = get_attribute(lines, 'gesa2_host')

    gesa3_host = get_attribute(lines, 'gesa3_host')

    gesa1_user = get_attribute(lines, 'gesa1_user')

    gesa2_user = get_attribute(lines, 'gesa2_user')

    gesa3_user = get_attribute(lines, 'gesa3_user')

    gesa1_password = get_attribute(lines, 'gesa1_password')

    gesa2_password = get_attribute(lines, 'gesa2_password')

    gesa3_password = get_attribute(lines, 'gesa3_password')

    sql_user = get_attribute(lines, 'sql_user')

    sql_password = get_attribute(lines, 'sql_password')

    huawei_user = get_attribute(lines, 'huawei_user')
    huawei_password = get_attribute(lines, 'huawei_password')

    cisco_user = get_attribute(lines, 'cisco_user')
    cisco_password = get_attribute(lines, 'cisco_password')

    passwords = {'gesa1_host': gesa1_host,
                 'gesa2_host': gesa2_host,
                 'gesa3_host': gesa3_host,
                 'gesa1_user': gesa1_user,
                 'gesa2_user': gesa2_user,
                 'gesa3_user': gesa3_user,
                 'gesa1_password': gesa1_password,
                 'gesa2_password': gesa2_password,
                 'gesa3_password': gesa3_password,
                 'sql_user': sql_user,
                 'sql_password': sql_password,
                 'huawei_user': huawei_user,
                 'huawei_password': huawei_password,
                 'cisco_user': cisco_user,
                 'cisco_password': cisco_password,
                 }

    return passwords


def get_configs():
    '''define pasta padrão com arquivos de config, le e salva atributos nas variáveis'''
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    general_path = os.path.abspath(os.path.join(directory_path, 'general'))
    file = os.path.abspath(os.path.join(general_path, 'config.txt'))

    file = open(file, 'r')
    lines = file.readlines()

    # procura linha com ufs, splita e salva a lista de UFs no dict.
    ufs = [x for x in lines if 'ufs' in x][0]
    ufs = ufs.split('=')[-1].strip()
    ufs = ufs.strip().split(', ')

    configs = {'ufs': ufs}

    return configs


def get_logger():
    #====================logger
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "logger"))
    logger_file = os.path.abspath(os.path.join(directory_path, 'std.log'))
    # now we will Create and configure logger
    logging.basicConfig(filename=logger_file,
                        format='%(asctime)s %(message)s',
                        filemode='a')
    # Let us Create an object
    logger = logging.getLogger()
    # Now we are going to Set the threshold of logger to DEBUG
    #logger.setLevel(logging.)
    return logger
