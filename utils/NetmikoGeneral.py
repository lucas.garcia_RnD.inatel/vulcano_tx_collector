from datetime import datetime
import os
import time
from inspect import getframeinfo, currentframe
import pandas as pd

import pyautogui

from utils.general import get_logger


def autogui_wait(file, time_wait):
    time.sleep(0.5)
    timeout = 0
    while True:
        if pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9):
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1


def autogui_double_click(file, time_wait):
    timeout = 0
    while True:
        pos = pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9)
        if pos:
            pyautogui.doubleClick(pos)
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1


def autogui_right_click(file, time_wait):
    timeout = 0
    while True:
        pos = pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9)
        if pos:
            #pyautogui.click(f'prints\\{file}', button='right')
            pyautogui.click(pos, button='right')
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1


def autogui_left_click(file, time_wait):
    timeout = 0
    while True:
        pos = pyautogui.locateOnScreen(f'prints\\{file}', confidence=0.9)
        if pos:
            pyautogui.click(pos)
            return True
        else:
            time.sleep(1)
            if timeout >= time_wait:
                print(f'TIME OUT ON {file}')
                return False
            else:
                timeout += 1


def autogui_error(gesa_id,where,step):
    if autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 2):
        pass
    else:
        pyautogui.keyDown('ctrl')
        pyautogui.press('c')
        pyautogui.keyUp('ctrl')
        time.sleep(1)
        pyautogui.keyDown('ctrl')
        pyautogui.press('z')
        pyautogui.keyUp('ctrl')
        time.sleep(1)
    if autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 2):
        pass
    else:
        pyautogui.typewrite('quit')
        pyautogui.hotkey('enter')
        if autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 2):
            pass
        else:
            pyautogui.typewrite('exit')
            pyautogui.hotkey('enter')
            if autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 2):
                pass
            else:
                pyautogui.typewrite('logout')
                pyautogui.hotkey('enter')
                if autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 2):
                    pass

    time.sleep(2)
    hr = datetime.now().strftime('%H:%M:%S')
    logger = get_logger()
    logger.warning(f'Some Error Occurred! >> {where} | {step} << ({hr})')
    print(f'Some Error Occurred! >> {where} | {step} << ({hr})')


def autogui_save_file(directory_path, file_name, gesa_id):
    autogui_right_click(f'general\\putty_bar_{gesa_id}.PNG', 10)

    autogui_left_click('general\\copy_text.PNG', 10)

    pyautogui.hotkey('ctrl', 'alt', 'n')

    autogui_wait('general\\notepad.PNG', 10)

    pyautogui.hotkey('ctrl', 'v')

    time.sleep(0.5)

    full_file_name = os.path.abspath(os.path.join(directory_path, file_name))

    pyautogui.hotkey('ctrl', 's')

    autogui_wait('general\\save_as.PNG', 10)

    pyautogui.typewrite(full_file_name)

    pyautogui.hotkey('enter')

    time.sleep(2)

    if not pyautogui.locateOnScreen('prints\\general\\notepad.PNG', confidence=0.9):
        pyautogui.hotkey('left')
        time.sleep(1)
        pyautogui.hotkey('enter')
        autogui_wait('general\\notepad.PNG', 10)
    else:
        autogui_left_click('general\\notepad.PNG', 5)

    pyautogui.hotkey('alt', 'f4')


    pyautogui.click()


    autogui_left_click(f'general\\putty_bar_{gesa_id}.PNG', 10)

    print(f'{full_file_name} saved')

    return full_file_name


def autogui_tellabs_wait(real_vendor, gesa_id,sleep=5):
    if not pyautogui.locateOnScreen(f'prints\\CISCO\\able_terminal.PNG', confidence=0.9):
        timeout = sleep
        while timeout != 0:
            if pyautogui.locateOnScreen(f'prints\\{real_vendor}\\more.PNG', confidence=0.9):
                pyautogui.hotkey('space')
                time.sleep(1)
                while True:
                    if pyautogui.locateOnScreen(f'prints\\{real_vendor}\\more.PNG', confidence=0.9):
                        pyautogui.hotkey('space')
                        time.sleep(1)
                    else:
                        break
                timeout = 0
                print(f'Done...')
            else:
                if not pyautogui.locateOnScreen(f'prints\\CISCO\\able_terminal.PNG', confidence=0.9):
                    print(f'Waiting for -More- count: {timeout} sec')
                    time.sleep(1)
                    timeout -= 1
                else:
                    return True

    if autogui_wait(f'CISCO\\able_terminal.PNG', 10):
        return True
    else:
        print(f'Error on -More- screen limit...')
        return False

def ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version, real_vendor, gesa_id):
    version_dict = {'NE_INIT': ne_init,
                    'UF': uf,
                    'NE_NAME': ne_name,
                    'IP': ip,
                    'VENDOR': vendor,
                    'REAL_VENDOR': real_vendor,
                    'OM_LINK': 'True_[Fail on Collect]',
                    'CONNECTED_VIA': connected_via,
                    'GESA': f'GESA_{gesa_id}',
                    'DATE': now}
    insert_df = pd.DataFrame([version_dict])
    df_version = pd.concat([df_version, insert_df], ignore_index=True)
    df_version.to_csv(ne_metro_file_path, mode='a', header=False, index=False)



def ne_metro_log_update(ne_name, ne_metro_file_path):
    df = pd.read_csv(ne_metro_file_path, header=None)
    columns = ['NE_INIT', 'UF', 'NE_NAME', 'REAL_NE_NAME', 'IP', 'VENDOR', 'REAL_VENDOR',
               'HW_VERSION', 'SFW_VERSION', 'LICENSE', 'OM_LINK', 'CONNECTED_VIA', 'GESA',
               'DATE']
    df.columns = columns
    df.loc[df['NE_INIT'] == ne_name, 'OM_LINK'] = 'True_[Fail on Collect]'
    df.to_csv(ne_metro_file_path, mode='a', header=False, index=False)
