import pyautogui
import time
import os
from subprocess import Popen
import pandas as pd
import mysql.connector as mysql
from sqlalchemy import create_engine
from datetime import datetime
import traceback

from utils.autogui_connections import ssh_connect, telnet_connect, autogui_clear_screen
from utils.NetmikoGeneral import autogui_wait, autogui_error, autogui_double_click, autogui_right_click, \
    autogui_left_click, autogui_save_file, autogui_tellabs_wait, ne_metro_log, ne_metro_log_update
from utils.general import send_sql_commit
from utils.parsers import get_peers_cisco, get_peers_huawei
from inspect import currentframe, getframeinfo

from utils.telegram_bot import telegram_send_message
from utils.general import get_logger

def get_progress(passwords,regional_info,attempt):
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/tx_ne_param{regional_info}'
    db_connection = create_engine(db_connection_str)

    df_todo = pd.read_sql(f"SELECT * FROM AUTOMATION_PROGRESS", con=db_connection)
    total = len(df_todo)

    df_todo = pd.read_sql(f"SELECT * FROM AUTOMATION_PROGRESS WHERE COLLECTED = 'True' ", con=db_connection)
    collected = len(df_todo)

    df_todo = pd.read_sql(f"SELECT * FROM AUTOMATION_PROGRESS WHERE CONNECTED_VIA IS NULl and COL_ATTEMPT != 0", con=db_connection)
    failed = len(df_todo)

    return total, collected, failed

def get_df_todo(attempt, regional_info, passwords, vendors):

    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/tx_ne_param{regional_info}'
    db_connection = create_engine(db_connection_str)
    df_todo = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)

    # se for teste
    teste = False

    if not teste:

        #filtra elementos por vendor
        df_todo = df_todo[df_todo['VENDOR'].isin(vendors)]

        # filtra elementos que não foram coletados ainda
        df_todo = df_todo[df_todo['COLLECTED'].isna()]

        # filtra elementos em que a tentativa é menor ou igual a atual
        df_todo['COL_ATTEMPT'] = df_todo['COL_ATTEMPT'].astype(int)
        df_todo = df_todo[df_todo['COL_ATTEMPT'] <= int(attempt)]


    else:
        # testes elementos huawei FUSION RJ
        ne_names = [ 'i-br-rj-cbf-cbo-hl4-01'
                    ,'i-br-rj-cps-cps-hl4-01'
                    ,'i-br-rj-cps-cps-hl4-02'
                    ,'i-br-rj-ioy-eim-hl5g-01'
                    ,'i-br-rj-ioy-jrs-hl5g-01'
                    ,'i-br-rj-ioy-rtn-hl5d-01'
                    ,'i-br-rj-mesq-bda-hl5g-01'
                    ,'i-br-rj-mesq-cre-hl5g-01'
                    ,'i-br-rj-mesq-ste-hl5g-01'
                    ,'i-br-rj-mesq-vln-hl5g-01'
                    ,'i-br-rj-niu-k40-hl4-01'
                    ,'i-br-rj-niu-k40-hl5d-01'
                    ,'i-br-rj-nri-mft-hl5g-01'
                    ,'i-br-rj-nri-nit-hl4-01'
                    ,'i-br-rj-nri-tbn-hl5g-01'
                    ,'i-br-rj-pts-cid-hl5g-01'
                    ,'i-br-rj-pts-mdm-hl5g-01'
                    ,'i-br-rj-pts-ptb-hl5d-01'
                    ,'i-br-rj-pts-vcu-hl5g-01'
                    ,'i-br-rj-rios-caz-hl4-01']

        df_todo = df_todo[df_todo['NE_NAME'].isin(ne_names)]

    return df_todo

def vrf_check(row):
    if row['VRF_x'] == row['VRF_y']:
        return row['VRF_x']
    elif row['VRF_x'] == 'NULL':
        return row['VRF_y']
    elif row['VRF_y'] == 'NULL':
        return row['VRF_x']

def first_file_analysis_huawei(ne_name, uf, vendor, ne_init, ip, now, log, connected_via, ne_metro_file_path,
                               df_version, real_vendor, gesa_id):
    try:

        lines_first = log.split('\n')
        print(f'====>Getting Version, license, peers, vpn instances - HUAWEI!<=====')

        # REAL_NE_NAME CHECK
        ne_name_identifier = [i for i, s in enumerate(lines_first) if f'<{ne_name}>' in s]
        if ne_name_identifier:
            real_ne_name = ne_name
        else:
            first_command = [s for s in lines_first if '>' in s][0]
            real_ne_name = first_command.split('>')[0].replace('<', '')


        # VERSION CHECK

        init_version = [i for i, s in enumerate(lines_first) if 'display version' in s][0] + 1
        lines_version = lines_first[init_version:]
        end_version = [i for i, s in enumerate(lines_version) if 'display' in s][0]
        lines_version = lines_version[:end_version]

        version = lines_version[1].split(' (')
        version_hw = ' '.join(version[2].split(' ')[:-1])
        version_sf = version[1].split(' ')[-1]
        license = version[2].split(' ')[-1].split(')')[0]

        version_dict = {'NE_INIT': ne_init,
                        'UF': uf,
                        'NE_NAME': ne_name,
                        'REAL_NE_NAME': real_ne_name,
                        'IP': ip,
                        'VENDOR': vendor,
                        'REAL_VENDOR': real_vendor,
                        'HW_VERSION': version_hw,
                        'SFW_VERSION': version_sf,
                        'LICENSE': license,
                        'OM_LINK': 'True',
                        'CONNECTED_VIA': connected_via,
                        'GESA': f'GESA_{gesa_id}',
                        'DATE': now}
        insert_df = pd.DataFrame([version_dict])
        df_version = pd.concat([df_version, insert_df], ignore_index=True)
        df_version.to_csv(ne_metro_file_path, mode='a', header=False, index=False)

        # VPN INSTANCES
        try:
            init_vpn_instances = [i for i, s in enumerate(lines_first) if 'display ip vpn-instance' in s][0] + 1
            if not '>' in lines_first[init_vpn_instances]:
                lines_vpn_instances = lines_first[init_vpn_instances:]
                end_vpn_instances = [i for i, s in enumerate(lines_vpn_instances) if '>' in s][0]
                lines_vpn_instances = lines_vpn_instances[:end_vpn_instances]

                init_vpn_instance = [i for i, s in enumerate(lines_vpn_instances) if 'VPN-INSTANCE NAME' in s.upper()][0] + 1
                lines_vpn = lines_vpn_instances[init_vpn_instance:]
                vpn_instances = [x.split()[0] for x in lines_vpn]
            else:
                vpn_instances = []
        except:
            vpn_instances = []
            pass

        # GET PEERS
        try:
            init_peers = [i for i, s in enumerate(lines_first) if 'display bgp vpnv4 all peer' in s][0] + 1
            lines_peers = lines_first[init_peers:]
            end_peers = [i for i, s in enumerate(lines_peers) if '>' in s][0]
            lines_peers = lines_peers[:end_peers]

            response = '\n'.join(lines_peers)
            peers = get_peers_huawei(response)
        except:
            peers = []
            pass

        print(f'====>Done!<=====')
        return peers, vpn_instances
    except Exception as e:
        raise e

def first_file_analysis_cisco(ne_name, uf, vendor, ne_init, ip, now, log, connected_via, ne_metro_file_path,
                               df_version, real_vendor, gesa_id):
    try:

        lines_first = log.split('\n')
        print(f'====>Getting ne info peers, vrf - cisco!<=====')

        # REAL_NE_NAME CHECK

        ne_name_identifier = [i for i, s in enumerate(lines_first) if f'{ne_name}#' in s]
        if ne_name_identifier:
            real_ne_name = ne_name
        else:
            first_command = [s for s in lines_first if '#' in s][0]
            real_ne_name = first_command.split('#')[0]

        # VERSION CHECK

        init_version = [i for i, s in enumerate(lines_first) if 'show version' in s][0] + 1
        lines_version = lines_first[init_version:]
        end_version = [i for i, s in enumerate(lines_version) if 'show interface' in s][0]
        lines_version = lines_version[:end_version]

        response_version = '\n'.join(lines_version)
        version_hw = 'NULL'
        version_sf = 'NULL'

        #versions
        versions_hw_list = ['ASR920','ASR900','ASR901','ASR903','ASR 9010','NCS 540']
        versions_sw_list = ['IOS XR','IOS XE','IOS Software']

        for hw_version in versions_hw_list:
            if hw_version in response_version:
                version_hw = hw_version

        for sw_version in versions_sw_list:
            if sw_version in response_version:
                version_sf = sw_version

        # DEPRECATED

        # try:
        #     if ('VERSION' in lines_version[0].upper()) and (len(lines_version[0].split(', ')) > 2):
        #         version = lines_version[0].split(', ')
        #     else:
        #         version = lines_version[2].split(', ') if len(lines_version[2].split(', ')) >= 2 else lines_version[1].split(', ')
        #
        #     if 'ASR 9010' in response_version:
        #         version_hw = 'ASR 9010'
        #         version_sf = version[1].split(' ')[1].strip()
        #         version_sf = version_sf.split('[')[0]
        #
        #     else:
        #         version_hw = version[1].split(' ')[0]
        #         if version_hw == '901':
        #             version_hw = 'ASR901'
        #         version_sf = version[2].split(' ')[1]
        # except:
        #     pass

        license = 'NULL'

        version_dict = {'NE_INIT': ne_init,
                        'UF': uf,
                        'NE_NAME': ne_name,
                        'REAL_NE_NAME': real_ne_name,
                        'IP': ip,
                        'VENDOR': vendor,
                        'REAL_VENDOR': real_vendor,
                        'HW_VERSION': version_hw,
                        'SFW_VERSION': version_sf,
                        'LICENSE': license,
                        'OM_LINK': 'True',
                        'CONNECTED_VIA': connected_via,
                        'GESA': f'GESA_{gesa_id}',
                        'DATE': now}

        insert_df = pd.DataFrame([version_dict])
        df_version = pd.concat([df_version, insert_df], ignore_index=True)

        df_version.to_csv(ne_metro_file_path, mode='a', header=False, index=False)

        # VRFs
        try:
            init_vrf = [i for i, s in enumerate(lines_first) if 'show running-config | include vrf' in s][0] + 1
            lines_vrf = lines_first[init_vrf:]
            end_vrf = [i for i, s in enumerate(lines_vrf) if 'show' in s][0]
            lines_vrf = lines_vrf[:end_vrf]
            vrf_list = lines_vrf[:-1]
            vrf_list = [s for s in vrf_list if s.startswith('vrf') or s.startswith(' vrf')]
            vrf_list = [x.strip().split()[-1] for x in vrf_list]
            vrf_list = list(set(vrf_list))
        except:
            vrf_list = []
            pass

        # GET PEERS
        try:
            init_peers = [i for i, s in enumerate(lines_first) if 'show ip bgp vpnv4 all summary' in s][0] + 1
            lines_peers = lines_first[init_peers:]
            end_peers = [i for i, s in enumerate(lines_peers) if '#' in s][0]
            lines_peers = lines_peers[:end_peers]

            response = '\n'.join(lines_peers)
            peers = get_peers_cisco(response)
        except:
            peers = []
            pass

        print(f'====>Done!<=====')
        return vrf_list, peers
    except Exception as e:
        raise e

def first_file_analysis_nokia(ne_name, uf, vendor, ne_init, ip, now, log, connected_via, ne_metro_file_path,
                               df_version, real_vendor, gesa_id):
    try:

        lines_first = log.split('\n')
        print(f'====>Getting ne info - nokia!<=====')

        # REAL_NE_NAME CHECK

        ne_name_identifier = [i for i, s in enumerate(lines_first) if f'{ne_name}#' in s]
        if ne_name_identifier:
            real_ne_name = ne_name
        else:
            first_command = [s for s in lines_first if '#' in s][0]
            real_ne_name = first_command.split('#')[0]

        # VERSION CHECK

        init_version = [i for i, s in enumerate(lines_first) if 'show version' in s][0] + 1
        lines_version = lines_first[init_version:]
        end_version = [i for i, s in enumerate(lines_version) if 'show port' in s][0]
        lines_version = lines_version[:end_version]
        response_version = '\n'.join(lines_version)
        version_hw = 'NULL'
        version_sf = 'NULL'

        for line in lines_version:
            if 'both/hops' in line:
                split_line = line.split('both/hops')
                version_sf = split_line[0].strip()
                version_hw = split_line[1].split('Copyright')[0].strip()


        license = 'NULL'

        version_dict = {'NE_INIT': ne_init,
                        'UF': uf,
                        'NE_NAME': ne_name,
                        'REAL_NE_NAME': real_ne_name,
                        'IP': ip,
                        'VENDOR': vendor,
                        'REAL_VENDOR': real_vendor,
                        'HW_VERSION': version_hw,
                        'SFW_VERSION': version_sf,
                        'LICENSE': license,
                        'OM_LINK': 'True',
                        'CONNECTED_VIA': connected_via,
                        'GESA': f'GESA_{gesa_id}',
                        'DATE': now}

        insert_df = pd.DataFrame([version_dict])
        df_version = pd.concat([df_version, insert_df], ignore_index=True)

        df_version.to_csv(ne_metro_file_path, mode='a', header=False, index=False)

        # VRFs
        init = [i for i, s in enumerate(lines_first) if 'show version' in s][0] + 1
        lines_analysis = lines_first[init_version:]
        end = [i for i, s in enumerate(lines_analysis) if 'show port' in s][0]
        lines_version = lines_analysis[:end_version]
        try:
            init_vrf = [i for i, s in enumerate(lines_first) if 'show running-config | include vrf' in s][0] + 1
            lines_vrf = lines_first[init_vrf:]
            end_vrf = [i for i, s in enumerate(lines_vrf) if 'show' in s][0]
            lines_vrf = lines_vrf[:end_vrf]
            vrf_list = lines_vrf[:-1]
            vrf_list = [s for s in vrf_list if s.startswith('vrf') or s.startswith(' vrf')]
            vrf_list = [x.strip().split()[-1] for x in vrf_list]
            vrf_list = list(set(vrf_list))
        except:
            vrf_list = []
            pass

        # GET PEERS
        try:
            init_peers = [i for i, s in enumerate(lines_first) if 'show ip bgp vpnv4 all summary' in s][0] + 1
            lines_peers = lines_first[init_peers:]
            end_peers = [i for i, s in enumerate(lines_peers) if '#' in s][0]
            lines_peers = lines_peers[:end_peers]

            response = '\n'.join(lines_peers)
            peers = get_peers_cisco(response)
        except:
            peers = []
            pass

        print(f'====>Done!<=====')
        return vrf_list, peers
    except Exception as e:
        raise e

def first_file_analysis_tellabs(ne_name, uf, vendor, ne_init, ip, now, log, connected_via, ne_metro_file_path,
                               df_version, real_vendor, gesa_id):
    try:
        lines_first = log.split('\n')
        print(f'====>Getting ne info peers, vrf - tellabs!<=====')

        # REAL_NE_NAME CHECK

        ne_name_identifier = [i for i, s in enumerate(lines_first) if f'{ne_name}>' in s]
        if ne_name_identifier:
            real_ne_name = ne_name
        else:
            first_command = [s for s in lines_first if '#' in s][0]
            real_ne_name = first_command.split('#')[0]

        # VERSION CHECK
        # lines_version = [s for s in lines_first if 'CORIANT' in s.upper()][0]
        # lines_version = [s for s in lines_version.split(' ') if s]
        #
        # version_num = [i for i, s in enumerate(lines_version) if s.upper() == 'CORIANT'][0] + 1
        #
        # version_hw = f'Coriant {lines_version[version_num]}'

        # GET VRFS
        try:
            init_vrfs = [i for i, s in enumerate(lines_first) if 'show ip interface brief' in s][0] + 1
            lines_vrfs = lines_first[init_vrfs:]
            end_vrfs = [i for i, s in enumerate(lines_vrfs) if '#' in s][0]
            lines_vrfs = lines_vrfs[:end_vrfs]

            lines_vrfs = [x for x in lines_vrfs if 'VRF' in x.upper()]
            vrfs = [x.split()[-1] for x in lines_vrfs]
            vrfs = list(set(vrfs))
        except:
            vrfs = []

        # GET PEERS
        try:
            init_peers = [i for i, s in enumerate(lines_first) if 'show ip bgp vpnv4 all summary' in s][0] + 1
            lines_peers = lines_first[init_peers:]
            end_peers = [i for i, s in enumerate(lines_peers) if '#' in s][0]
            lines_peers = lines_peers[:end_peers]

            response = '\n'.join(lines_peers)
            peers = get_peers_cisco(response)
        except:
            peers = []

        license = 'NULL'
        version_hw = '-'
        version_sf = '-'

        version_dict = {'NE_INIT': ne_init,
                        'UF': uf,
                        'NE_NAME': ne_name,
                        'REAL_NE_NAME': real_ne_name,
                        'IP': ip,
                        'VENDOR': vendor,
                        'REAL_VENDOR': real_vendor,
                        'HW_VERSION': version_hw,
                        'SFW_VERSION': version_sf,
                        'LICENSE': license,
                        'OM_LINK': 'True',
                        'CONNECTED_VIA': connected_via,
                        'GESA': f'GESA_{gesa_id}',
                        'DATE': now}

        insert_df = pd.DataFrame([version_dict])
        df_version = pd.concat([df_version, insert_df], ignore_index=True)

        df_version.to_csv(ne_metro_file_path, mode='a', header=False, index=False)

        print(f'====>Done!<=====')
        return vrfs, peers
    except Exception as e:
        raise e

def autogui_process(df_todo, definitive, gesa_id, cursor, attempt, regional_info, passwords):

    pyautogui.FAILSAFE = False
    logger = get_logger()

    now = datetime.now().date().strftime("%Y-%m-%d")

    # abre tela do putty do GESA com usuario e senha definidos na config
    Popen(f"powershell putty.exe {passwords[f'gesa{gesa_id}_user']}@{passwords[f'gesa{gesa_id}_host']} "
          f"-pw {passwords[f'gesa{gesa_id}_password']}")

    # tela inicial do putty para inserir o token do datablink... timeout de 3600 segundos pra gesa 1 necessario token
    autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 3600)

    # deixa tela do putty em tela cheia (double click na barra do programa)
    if True:
        autogui_double_click(f'general\\putty_bar_{gesa_id}.PNG', 10)

    # define diretórios que serão utilizados para trabalhar
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    ne_metro_file_path = os.path.abspath(os.path.join(csv_path, 'ne_metro_table.csv'))

    # reseta index do dataframe
    df_todo.reset_index(inplace=True, drop=True)

    # pega total de elementos que vai coletar
    total = len(df_todo['NE_NAME'])

    #define status inicial porcentagem
    last_por = -1
    session_sucess = 0

    for index, row in df_todo.iterrows():


        #keep alive telegram sender
        times = datetime.now()
        porcentagem = int(((index + 1) * 100) / total)
        totalicus, collected, failed = get_progress(passwords, regional_info, attempt)
        if porcentagem != last_por and porcentagem %2 == 0:
            last_por = porcentagem
            telegram_send_message(f'Keep alive report:\n Gesa: {attempt} \n Progress: {porcentagem}%  \n '
                                  f'Total site number: {totalicus} \n Attempt site number: {total} \n '
                                  f'Attempt processed: {index+1} \n '
                                  f'Attempt success sites: {session_sucess} \n '
                                  f'Total collected: {collected} \n Total failed: {failed}')


        #criando dataframe primário
        df_version = pd.DataFrame(columns=['NE_INIT', 'UF', 'NE_NAME', 'REAL_NE_NAME', 'IP', 'VENDOR', 'REAL_VENDOR',
                                           'HW_VERSION', 'SFW_VERSION', 'LICENSE', 'OM_LINK', 'CONNECTED_VIA', 'GESA',
                                           'DATE'])

        # definindo parametros para coleta
        ip = row['IP']
        vendor = row['VENDOR']
        ne_name = row['NE_NAME']
        ne_init = ne_name.split('-')[4].upper()
        uf = ne_name.split('-')[2].upper()
        start = datetime.now()
        vendor_tacats = str(vendor).lower()

        # iniciando o processo
        print(f'Starting {ne_name} ({vendor}): {index + 1}\\{total}')
        #telegram_send_message(f'Starting {ne_name} ({vendor}): {index + 1}\\{total}')
        logger.warning(f'Starting {ne_name} ({vendor}): {index + 1}\\{total}')

        sql = f"UPDATE AUTOMATION_PROGRESS SET NE_NAME = '{ne_name}' WHERE NE_NAME = '{ne_name}'"

        cursor = send_sql_commit(sql, cursor, regional_info, passwords)
        if not cursor:
            print(f'Fatal SQL error... program stopped!')
            telegram_send_message(f'Fatal SQL error... program stopped!')
            exit()

        #limpando tela
        autogui_clear_screen(gesa_id)

        #DEFINE LOCAL USER COMO FALSE
        local_user = False
        # tentativa SSH gesa1_user/pass
        print(f'Trying SSH gesa1_user/pass: ')
        real_vendor, connected_via = ssh_connect(passwords["gesa1_user"], passwords["gesa1_password"], ip, gesa_id,local_user)
        # tentativa Telnet gesa1_user/pass
        if not real_vendor:
            print(f'Trying Telnet gesa1_user/pass: ')
            real_vendor, connected_via = telnet_connect(passwords["gesa1_user"], passwords["gesa1_password"], ip, gesa_id,local_user)
        # tentativa SSH non_tacats
        if not real_vendor:
            if vendor_tacats == 'huawei' or vendor_tacats == 'cisco':
                print(f'Trying SSH non_tacats: ')
                local_user = True
                real_vendor, connected_via = ssh_connect(passwords[f"{vendor_tacats}_user"], passwords[f"{vendor_tacats}_password"], ip, gesa_id,local_user)
            else:
                print(f'Vendor {vendor_tacats} - skip non-tacats try...')
        # tentativa Telnet non_tacats
        if not real_vendor:
            if vendor_tacats == 'huawei' or vendor_tacats == 'cisco':
                print(f'Trying Telnet non_tacats: ')
                local_user = True
                real_vendor, connected_via = telnet_connect(passwords[f"{vendor_tacats}_user"], passwords[f"{vendor_tacats}_password"], ip, gesa_id,local_user)
            else:
                print(f'Vendor {vendor_tacats} - skip non-tacats try...')
        # caso seja nokia
        if real_vendor == 'NOKIA_ALCATEL':
            version_dict = {'NE_INIT': ne_init,
                            'UF': uf,
                            'NE_NAME': ne_name,
                            'IP': ip,
                            'VENDOR': vendor,
                            'REAL_VENDOR': real_vendor,
                            'OM_LINK': 'True',
                            'CONNECTED_VIA': connected_via,
                            'GESA':gesa_id,
                            'DATE': now}
            insert_df = pd.DataFrame([version_dict])
            df_version = pd.concat([df_version, insert_df], ignore_index=True)
            df_version.to_csv(ne_metro_file_path, mode='a', header=False, index=False)
            sql = f"UPDATE AUTOMATION_PROGRESS SET COLLECTED = 'False', COL_ATTEMPT = {attempt}, OM_LINK = 'True' WHERE NE_NAME = '{ne_name}'"
            cursor = send_sql_commit(sql, cursor, regional_info, passwords)
            if not cursor:
                print(f'Fatal SQL error... program stoped!')
                exit()
            continue

        if not real_vendor:
            if definitive:
                version_dict = {'NE_INIT': ne_init,
                                'UF': uf,
                                'NE_NAME': ne_name,
                                'IP': ip,
                                'VENDOR': vendor,
                                'OM_LINK': 'False',
                                'DATE': now}
                insert_df = pd.DataFrame([version_dict])
                df_version = pd.concat([df_version, insert_df], ignore_index=True)
                df_version.to_csv(ne_metro_file_path, mode='a', header=False, index=False)
                sql = f"UPDATE AUTOMATION_PROGRESS SET COLLECTED = 'False', COL_ATTEMPT = {attempt}, OM_LINK = 'False'  WHERE NE_NAME = '{ne_name}'"
                cursor = send_sql_commit(sql, cursor, regional_info, passwords)
                if not cursor:
                    print(f'Fatal SQL error... program stoped!')
                    exit()
                continue
            else:
                sql = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {attempt} WHERE NE_NAME = '{ne_name}'"
                cursor = send_sql_commit(sql, cursor, regional_info, passwords)
                if not cursor:
                    print(f'Fatal SQL error... program stoped!')
                    telegram_send_message(f'Fatal SQL error... program stopped!')
                    exit()
                continue



        if real_vendor == 'HUAWEI':
            autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10)

            pyautogui.typewrite('screen-length 0 temporary')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now,connected_via, ne_metro_file_path, df_version, real_vendor, gesa_id)
                continue

            pyautogui.typewrite('display version')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('display arp all')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('display interface brief')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 30):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('display ip vpn-instance')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('display bgp vpnv4 all peer')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            file_name = f'{ne_name}_first.txt'

            full_file_name = autogui_save_file(directory_path, file_name, gesa_id)

            try:
                peers, vpn_instances = first_file_analysis_huawei(ne_name, uf, vendor, ne_init, ip, now, full_file_name,
                                                                  connected_via, ne_metro_file_path, df_version,
                                                                  real_vendor, gesa_id)
            except:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                print(f'Error on process Huawei file, GoTo next!')
                continue

            pyautogui.typewrite(f'display interface')
            pyautogui.hotkey('enter')

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            if not '-gwc-' in ne_name or not '-hl4-' in ne_name:
                for peer in peers:
                    pyautogui.typewrite(f'display ip routing-table {peer}')
                    pyautogui.hotkey('enter')

                    if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                        pass
                    else:
                        autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                        ne_metro_log_update(ne_name, ne_metro_file_path)
                        continue

            pyautogui.typewrite(f'display current interface')
            pyautogui.hotkey('enter')

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            pyautogui.typewrite('display ip routing-table')
            pyautogui.hotkey('enter')
            time.sleep(2)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            for vpn_instance in vpn_instances:
                pyautogui.typewrite(f'display ip routing-table vpn-instance {vpn_instance}')
                pyautogui.hotkey('enter')
                time.sleep(2)

                if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                    pass
                else:
                    autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                    ne_metro_log_update(ne_name, ne_metro_file_path)
                    continue
            # CHECANDO PTP somente rede FUSION
            if 'i-br-' in ne_name:
                pyautogui.typewrite('display ptp all')
                pyautogui.hotkey('enter')
                time.sleep(2)
                if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                    pass
                else:
                    autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                    ne_metro_log_update(ne_name, ne_metro_file_path)
                    continue

            file_name = f'{ne_name}.txt'

            autogui_save_file(directory_path, file_name, gesa_id)

        elif real_vendor == 'CISCO':
            autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10)

            pyautogui.typewrite('terminal length 0')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show version')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now,connected_via, ne_metro_file_path, df_version, real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show interface transceiver detail')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 300):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now,connected_via, ne_metro_file_path, df_version, real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show ip interface brief')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 30):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now,connected_via, ne_metro_file_path, df_version, real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show running-config | include vrf')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now,connected_via, ne_metro_file_path, df_version, real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show ip bgp vpnv4 all summary')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now,connected_via, ne_metro_file_path, df_version, real_vendor, gesa_id)
                continue

            file_name = f'{ne_name}_first.txt'

            full_file_name = autogui_save_file(directory_path, file_name, gesa_id)

            try:
                vrf_list, peers = first_file_analysis_cisco(ne_name, uf, vendor, ne_init, ip, now, full_file_name,
                                                            connected_via, real_vendor, gesa_id, df_version,
                                                            ne_metro_file_path)
            except:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show arp')
            pyautogui.hotkey('enter')
            time.sleep(1)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            for vrf in vrf_list:
                pyautogui.typewrite(f'show arp vrf {vrf}')
                pyautogui.hotkey('enter')
                time.sleep(1)

                if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 30):
                    pass
                else:
                    autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                    ne_metro_log_update(ne_name, ne_metro_file_path)
                    continue

            pyautogui.typewrite(f'show interfaces')
            pyautogui.hotkey('enter')

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            if not '-gwc-' in ne_name or not '-hl4-' in ne_name:
                for peer in peers:
                    pyautogui.typewrite(f'show ip route {peer}')
                    pyautogui.hotkey('enter')

                    if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                        pass
                    else:
                        autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                        ne_metro_log_update(ne_name, ne_metro_file_path)
                        continue

            if '-gwc-' in ne_name:
                pyautogui.typewrite('show running-config interface')
                pyautogui.hotkey('enter')

            else:
                pyautogui.typewrite('show running-config | section interface')
                pyautogui.hotkey('enter')

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            pyautogui.typewrite('show ip route')
            pyautogui.hotkey('enter')
            time.sleep(2)

            if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            for current_vrf in vrf_list:
                pyautogui.typewrite(f'show ip route vrf {current_vrf}')
                pyautogui.hotkey('enter')
                time.sleep(2)

                if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                    pass
                else:
                    autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                    ne_metro_log_update(ne_name, ne_metro_file_path)
                    continue
            # CHECANDO PTP somente rede FUSION
            if 'i-br-' in ne_name:
                pyautogui.typewrite('Show ptp platform servo')
                pyautogui.hotkey('enter')
                time.sleep(2)
                if autogui_wait(f'{real_vendor}\\able_terminal.PNG', 60):
                    pass
                else:
                    autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                    ne_metro_log_update(ne_name, ne_metro_file_path)
                    continue

            file_name = f'{ne_name}.txt'

            autogui_save_file(directory_path, file_name, gesa_id)

        elif real_vendor == 'TELLABS':
            autogui_wait(f'HUAWEI\\able_terminal.PNG', 10)

            pyautogui.typewrite('en')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'CISCO\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('terminal length 200')
            pyautogui.hotkey('enter')
            time.sleep(0.5)

            if autogui_wait(f'CISCO\\able_terminal.PNG', 10):
                pass
            else:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show ip interface brief')
            pyautogui.hotkey('enter')
            time.sleep(2)

            if not autogui_tellabs_wait(real_vendor, gesa_id):
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show ip bgp vpnv4 all summary')
            pyautogui.hotkey('enter')
            time.sleep(2)

            if not autogui_tellabs_wait(real_vendor, gesa_id):
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            file_name = f'{ne_name}_first.txt'

            full_file_name = autogui_save_file(directory_path, file_name, gesa_id)

            try:
                vrf_list, peers = first_file_analysis_tellabs(full_file_name, ne_name, ne_init, uf, ip, vendor,
                                                              real_vendor, connected_via, gesa_id, now, df_version,
                                                              ne_metro_file_path)
            except:
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connected_via, ne_metro_file_path, df_version,
                             real_vendor, gesa_id)
                continue

            pyautogui.typewrite('show ip interface description')
            pyautogui.hotkey('enter')
            time.sleep(3)

            if not autogui_tellabs_wait(real_vendor, gesa_id):
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            pyautogui.typewrite('show arp')
            pyautogui.hotkey('enter')
            time.sleep(3)

            if not autogui_tellabs_wait(real_vendor, gesa_id):
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            for cur_vrf in vrf_list:
                pyautogui.typewrite(f'show arp vrf {cur_vrf}')
                pyautogui.hotkey('enter')
                time.sleep(3)

                if not autogui_tellabs_wait(real_vendor, gesa_id):
                    autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                    ne_metro_log_update(ne_name, ne_metro_file_path)
                    continue

            if not '-gwc-' in ne_name or not '-hl4-' in ne_name:
                for cur_peer in peers:
                    pyautogui.typewrite(f'show ip route {cur_peer}')
                    pyautogui.hotkey('enter')
                    time.sleep(3)

                    if not autogui_tellabs_wait(real_vendor, gesa_id):
                        autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                        ne_metro_log_update(ne_name, ne_metro_file_path)
                        continue

            pyautogui.typewrite('show interface')
            pyautogui.hotkey('enter')
            time.sleep(1)

            if not autogui_tellabs_wait(real_vendor, gesa_id, 360):
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            pyautogui.typewrite('show running-config | begin Interfaces')
            pyautogui.hotkey('enter')
            time.sleep(1)

            if not autogui_tellabs_wait(real_vendor, gesa_id, 360):
                autogui_error(gesa_id, f'{real_vendor} Collect', getframeinfo(currentframe()).lineno)
                ne_metro_log_update(ne_name, ne_metro_file_path)
                continue

            file_name = f'{ne_name}.txt'

            autogui_save_file(directory_path, file_name, gesa_id)


        pyautogui.typewrite('quit')
        pyautogui.hotkey('enter')

        if autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 10):
            pass
        else:
            pyautogui.typewrite('exit')
            pyautogui.hotkey('enter')
            if autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 10):
                pass
            else:
                pyautogui.typewrite('logout')
                pyautogui.hotkey('enter')

        time.sleep(1)
        autogui_wait(f'general\\able_terminal_{gesa_id}.PNG', 10)

        sql = f"UPDATE AUTOMATION_PROGRESS SET VENDOR = '{real_vendor}', GESA = 'GESA_{gesa_id}'," \
              f" CONNECTED_VIA = '{connected_via}', OM_LINK = 'True', COLLECTED = 'True' WHERE NE_NAME = '{ne_name}'"
        cursor = send_sql_commit(sql, cursor, regional_info, passwords)
        if not cursor:
            print(f'Fatal SQL error... program stoped!')
            telegram_send_message(f'Fatal SQL error... program stopped!')
            exit()
        #increment sucess rate
        session_sucess += 1

        final = datetime.now()
        delta = final - start
        delta_time = delta
        delta = str(delta).split('.')[0]
        print(f'Collected {ne_name} in {delta}')
        porcentagem = int(((index + 1) * 100) / total)
        eta = (total - (index + 1)) * delta_time
        eta = str(eta).split('.')[0]
        if porcentagem != last_por and porcentagem %2 == 0:
            telegram_send_message(f'Collect status report:\n Progress: {porcentagem}% \n Eta: {eta} \n')

        logger.warning(f'Collected {ne_name} in {delta}')

    time.sleep(5)
    pyautogui.typewrite('exit')
    pyautogui.hotkey('enter')
    time.sleep(5)


def autogui_collect(definitive=False, gesa_id='', attempt=1, regional_info='', passwords='', vendors=None, configs=''):

    if vendors is None:
        vendors = []

    # Verifica sites para coletar baseado em quais vendors foram passados como argumento
    df_todo = get_df_todo(attempt, regional_info, passwords, vendors)

    if not df_todo.empty:

        # Conta quantos elementos vai coletar
        total_df = len(df_todo['NE_NAME'])
        state = configs['ufs']

        print(f'============>COLLECT {state} STARTED<============')
        telegram_send_message(f'Start collect {total_df} sites: attempt => {attempt} - {state} STARTED')
        print(f"{total_df} Remaining")

        db = mysql.connect(
            host="10.26.82.160",
            user=passwords['sql_user'],
            passwd=passwords['sql_password'],
            database=f'tx_ne_param{regional_info}',
            allow_local_infile=True
        )

        db.autocommit = True
        cursor = db.cursor()


        # processa o collect com o df_todo restante
        if total_df > 0:
            autogui_process(df_todo, definitive, gesa_id, cursor, attempt, regional_info, passwords)

        print('============>COLLECT FINISHED<============')
        telegram_send_message(f'Finalizada coleta tentativa: attempt => {attempt} - {state}')

