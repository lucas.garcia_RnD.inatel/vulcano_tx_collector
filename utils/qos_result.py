import pandas as pd
import re
import os


def replace(old, new, str, caseinsentive=False):
    if caseinsentive:
        return str.replace(old, new)
    else:
        return re.sub(re.escape(old), new, str, flags=re.IGNORECASE)


def get_int_desc_huawei(response):
    response = response.split('\n')
    response = [x for x in response if 'escription' in x]
    if len(response) >= 1:
        response = response[0]
    else:
        return 'N/A'
    response = response.split(':')
    if len(response) >= 2 and response[1].strip():
        return response[1].strip().replace("\'", '')
    else:
        return 'N/A'


def get_int_desc_cisco(response):
    if 'DESCRIPTION' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'DESCRIPTION' in x.upper()][0]
        response = replace('DESCRIPTION', 'DESCRIPTION', response)
        response = response.strip().split('DESCRIPTION:')[-1].strip()

        return response
    else:
        return 'NULL'


df_list = []
df_list_index = 0
directory_path = 'D:\\desktop_legacy\\automation_collect\\'
ne_files = [f for f in os.listdir(directory_path) if os.path.isfile(os.path.join(directory_path, f))]

for cur_ne_file in ne_files:
    file_path = f'{directory_path}{cur_ne_file}'
    ne_name = cur_ne_file.split('.txt')[0].strip()
    try:
        first_file = open(f'{file_path}', 'r')
        lines_analysis = first_file.readlines()
        first_file.close()

        if any([x for x in lines_analysis if f'{ne_name}>' in x]):
            vendor = 'huawei'
        elif any([x for x in lines_analysis if f'{ne_name}#' in x]):
            vendor = 'cisco'
        else:
            print(f'ERROR ON {file_path}')
            continue

        if vendor == 'huawei':
            init_interfaces = [i for i, s in enumerate(lines_analysis) if 'display interface brief' in s][0] + 1
            lines_interfaces = lines_analysis[init_interfaces:]
            end_interfaces = [i for i, s in enumerate(lines_interfaces) if 'display' in s][0]
            lines_interfaces = lines_interfaces[:end_interfaces]
            indices = [i for i, s in enumerate(lines_interfaces) if 'nterface' in s]
            header = lines_interfaces[indices[0]]
            columns = header.split()
            quantity = len(columns)
            del lines_interfaces[0:(indices[0] + 1)]
            df_lines_interfaces = [segments.split()[:quantity] for segments in lines_interfaces]
            df_interfaces = pd.DataFrame(df_lines_interfaces, columns=columns)
            df_interfaces.dropna(subset=[columns[1]], inplace=True)
            interfaces = df_interfaces['Interface'].to_list()
            end_triggers = [s.split('(')[0] + ' current state' for s in interfaces]
            if '-gwc-' in ne_name:
                end_triggers.append('>\n')
            else:
                end_triggers.append('display')
            interface_qos_check = []
            end_triggers_qos = ['interface ' + s.split('(')[0] + '\n' for s in interfaces]
            init_all_interfaces = [i for i, s in enumerate(lines_analysis) if 'display interface' in s][1] + 1
            lines_all_interfaces = lines_analysis[init_all_interfaces:]

            interface_dict = {}

            for interface in interfaces:
                if '(' in interface:
                    interface = interface.split('(')[0]
                init_interface = [i for i, s in enumerate(lines_all_interfaces) if f'{interface} current state' in s][0]
                lines_interface = lines_all_interfaces[init_interface:]
                end_interface = [i for i, s in enumerate(lines_interface) if any(end in s for end in end_triggers)][1]
                lines_interface = lines_interface[:end_interface]
                response = '\n'.join(lines_interface)
                description = get_int_desc_huawei(response)
                if 'SERIAL' in interface.upper():
                    continue
                interface_dict.update({interface: description})
                ne_name_frag = ne_name.split('-')[-3].upper()
                if any(x for x in [f'T{ne_name_frag}', f'M{ne_name_frag}', f'W{ne_name_frag}', 'M-BR', 'M_BR'] if
                       x in description.upper()):
                    interface_qos_check.append(interface)

            # QOS CHECK

            init_all_qos = [i for i, s in enumerate(lines_analysis) if 'display current' in s][0] + 1
            lines_all_qos = lines_analysis[init_all_qos:]
            end_all_qos = [i for i, s in enumerate(lines_all_qos) if 'display' in s][0] + 1
            lines_all_qos = lines_all_qos[:end_all_qos]
            qos_info = []
            for qos_check in end_triggers_qos:
                init_qos = [i for i, s in enumerate(lines_all_qos) if s == qos_check]
                if init_qos:
                    init_qos = init_qos[0]
                    lines_qos = lines_all_qos[init_qos:]
                    end_qos = [i for i, s in enumerate(lines_qos) if
                               len(s.strip().split()) > 1 and s.strip().split()[0].upper() == 'INTERFACE']
                    if len(end_qos) >= 2:
                        end_qos = end_qos[1]
                    else:
                        end_qos = [i for i, s in enumerate(lines_qos) if 'display' in s][0]
                    lines_qos = lines_qos[:end_qos]
                    response_version = '\n'.join(lines_qos)
                    qos_info.append(response_version)

            interface_qos_check = [y.split('.')[0] for y in interface_qos_check]
            interface_qos_check = list(set(interface_qos_check))

            patterns = ['port-queue be', 'port-queue af1', 'port-queue af2', 'port-queue af3',
                        'port-queue af4', 'port-queue ef', 'port-queue cs6']

            for current_qos_check in interface_qos_check:
                qos_finder = 'interface ' + current_qos_check + '\n'
                qos_response = [s for s in qos_info if qos_finder in s]
                df_list.append(
                    {'NE_NAME': ne_name, 'INTERFACE': current_qos_check, 'DESCRIPTION': interface_dict[current_qos_check]})
                if qos_response:
                    qos_response = qos_response[0]
                    if all(x.upper() in qos_response.upper() for x in patterns):
                        qos_result = 'OK'
                    else:
                        qos_result = 'NOK'
                    df_list[df_list_index].update({'QOS_STATUS': qos_result})
                    qos_response = qos_response.split('\n')
                    qos_response = [x for x in qos_response if 'port-queue' in x]
                    for current_qos_param in qos_response:
                        qos_param = current_qos_param.strip().split('port-queue')[-1].strip().split(' ')[0].strip()
                        param_content = ' '.join(current_qos_param.strip().split('port-queue')[-1].strip().split(' ')[1:])
                        df_list[df_list_index].update({f'QOS_{qos_param.upper()}': param_content})

                df_list_index += 1

        elif vendor == 'cisco':
            interface_dict = {}
            init_interfaces = [i for i, s in enumerate(lines_analysis) if 'show ip interface brief' in s][0] + 1
            lines_interfaces = lines_analysis[init_interfaces:]
            end_interfaces = [i for i, s in enumerate(lines_interfaces) if 'show' in s][0]
            lines_interfaces = lines_interfaces[:end_interfaces]
            indices = [i for i, s in enumerate(lines_interfaces) if 'nterface' in s]
            header = lines_interfaces[indices[0]]
            columns = header.split()
            quantity = len(columns)
            del lines_interfaces[0:(indices[0] + 1)]
            lines_interfaces = [line.replace('administratively down', 'administratively-down') for line in lines_interfaces]
            df_lines_interfaces = [segments.split()[:quantity] for segments in lines_interfaces]
            df_interfaces = pd.DataFrame(df_lines_interfaces, columns=columns)
            df_interfaces.dropna(subset=[columns[1]], inplace=True)
            interfaces = df_interfaces['Interface'].to_list()

            end_triggers = []

            for end_trigger in interfaces:
                if (not ('TenGig' in end_trigger)) and ('TE' in end_trigger[:2].upper()):
                    end_triggers.append(replace('Te', 'TenGigabitEthernet', end_trigger) + ' is ')
                else:
                    end_triggers.append(end_trigger + ' is ')
            if '-gwc-' in ne_name:
                end_triggers.append('#\n')
            else:
                end_triggers.append('show')

            end_triggers_qos = []
            interface_qos_check = []

            for end_trigger in interfaces:
                if (not ('TenGig' in end_trigger)) and ('TE' in end_trigger[:2].upper()):
                    end_triggers_qos.append('interface ' + replace('Te', 'TenGigabitEthernet', end_trigger) + '\n')
                else:
                    end_triggers_qos.append('interface ' + end_trigger + '\n')

            init_all_interfaces = [i for i, s in enumerate(lines_analysis) if 'show interfaces' in s][0] + 1
            lines_all_interfaces = lines_analysis[init_all_interfaces:]

            for interface in interfaces:
                ip_address = df_interfaces[df_interfaces['Interface'] == interface]['IP-Address'].iloc[0]
                if (not ('TenGig' in interface)) and ('TE' in interface[:2].upper()):
                    interface_founder = replace('Te', 'TenGigabitEthernet', interface)
                else:
                    interface_founder = interface
                if [i for i, s in enumerate(lines_all_interfaces) if f'{interface_founder} is ' in s]:
                    init_interface = [i for i, s in enumerate(lines_all_interfaces) if f'{interface_founder} is ' in s][0]
                    lines_interface = lines_all_interfaces[init_interface:]
                    end_interface = [i for i, s in enumerate(lines_interface) if any(end in s for end in end_triggers)][1]
                    lines_interface = lines_interface[:end_interface]
                    response = '\n'.join(lines_interface)
                    description = get_int_desc_cisco(response)
                    if ('BDI' in interface.upper()) or ('VPN' in interface.upper()):
                        continue
                    interface_dict.update({interface: description})
                    ne_name_frag = ne_name.split('-')[-3].upper()
                    if any(x for x in [f'T{ne_name_frag}', f'M{ne_name_frag}', f'W{ne_name_frag}', 'M-BR', 'M_BR'] if
                           x in description.upper()):
                        interface_qos_check.append(interface)
            # QOS CHECK
            if '-gwc-' in ne_name:
                init_all_qos = [i for i, s in enumerate(lines_analysis) if 'show running-config interface' in s][0] + 1
            else:
                init_all_qos = [i for i, s in enumerate(lines_analysis) if 'show running-config | section interface' in s][
                                   0] + 1
            lines_all_qos = lines_analysis[init_all_qos:]
            end_all_qos = [i for i, s in enumerate(lines_all_qos) if '#\n' in s][0] + 1
            lines_all_qos = lines_all_qos[:end_all_qos]
            qos_info = []
            for qos_check in end_triggers_qos:
                init_qos = [i for i, s in enumerate(lines_all_qos) if s == qos_check]
                if init_qos:
                    init_qos = init_qos[0]
                    lines_qos = lines_all_qos[init_qos:]
                    end_qos = [i for i, s in enumerate(lines_qos) if
                               len(s.strip().split()) > 1 and s.strip().split()[0].upper() == 'INTERFACE']
                    if len(end_qos) >= 2:
                        end_qos = end_qos[1]
                    else:
                        end_qos = [i for i, s in enumerate(lines_qos) if '#' in s][0]
                    lines_qos = lines_qos[:end_qos]
                    response_version = '\n'.join(lines_qos)
                    qos_info.append(response_version)
            interface_qos_check = [y.split('.')[0] for y in interface_qos_check]
            interface_qos_check = list(set(interface_qos_check))
            for current_qos_check in interface_qos_check:
                if current_qos_check.upper()[:2] == 'VL':
                    qos_finder = f'service instance {"".join(filter(str.isdigit, current_qos_check))}'
                else:
                    if (not ('TenGig' in current_qos_check)) and ('TE' in current_qos_check[:2].upper()):
                        qos_finder = 'interface ' + replace('Te', 'TenGigabitEthernet', current_qos_check) + '\n'
                    else:
                        qos_finder = 'interface ' + current_qos_check + '\n'
                qos_response = [s for s in qos_info if qos_finder in s]
                df_list.append(
                    {'NE_NAME': ne_name, 'INTERFACE': current_qos_check, 'DESCRIPTION': interface_dict[current_qos_check]})
                if qos_response:
                    qos_response = qos_response[0]
                    all_finds = [m.start() for m in re.finditer('SERVICE-POLICY', qos_response.upper())]
                    if '-gwc-' in ne_name:
                        if len(all_finds) >= 1:
                            qos_result = 'OK'
                        else:
                            qos_result = 'NOK'
                    else:
                        if len(all_finds) >= 2:
                            qos_result = 'OK'
                        else:
                            qos_result = 'NOK'

                    df_list[df_list_index].update({'QOS_STATUS': qos_result})
                    qos_response = qos_response.split('\n')
                    qos_response = [x for x in qos_response if 'SERVICE-POLICY' in x.upper()]

                    for current_qos_param in qos_response:
                        qos_param = current_qos_param.strip().split('service-policy')[-1].strip().split(' ')[0].strip()
                        param_content = ' '.join(
                            current_qos_param.strip().split('service-policy')[-1].strip().split(' ')[1:])
                        df_list[df_list_index].update({f'QOS_{qos_param.upper()}': param_content})

                df_list_index += 1

        print(f'DONE: {cur_ne_file}')
    except:
        print(f'ERROR ON {cur_ne_file}')

df = pd.DataFrame(df_list)
df.fillna('-', inplace=True)
df.to_csv(f'{directory_path}qos_response.csv', index=False)

