# importing all required libraries
import os

import telegram_send
import telegram
import logging



# Function to send message
from utils.general import get_logger


def telegram_send_message(text):
    '''para utilizar a mensagem particular, deve-se:
    terminal -> telegram-send --configure
    cole seu token do bot
    pegue a senha que foi gerada e envie para o seu bot no telegram.
    pronto, agora ele vai receber as msgs do telegram_send
    '''
    #enviando msg particular
    #telegram_send.send(messages=[text])
    #enviando msg para o grupo
    telegram_send_grp_message(text)

def telegram_send_grp_message(msg):
    logger = get_logger()
    logger.warning(msg)
    """
    Send a message to a telegram user or group specified on chatId
    chat_id must be a number!
    verificar o id do chat através do link
    https://api.telegram.org/bot5299784181:AAFbDVrLV8UFdm3y-sP7eiUVdqwgmxlTnow/getUpdates
    """
    # token that can be generated talking with @BotFather on telegram
    token = '5299784181:AAFbDVrLV8UFdm3y-sP7eiUVdqwgmxlTnow'

    # enviando msg para o grupo do telegram com o id -769253007
    bot = telegram.Bot(token=token)
    bot.sendMessage(chat_id=-769253007, text=msg)
