from utils.NetmikoProcess import autogui_collect
from utils.csv_parser import run_csv_generation
from utils.csv_upload import ne_metro_csv_upload, interface_csv_upload, mac_csv_upload, routing_table_csv_upload, \
    full_routing_table_csv_upload, simple_chart_table_csv_upload, metro_steps_table_csv_upload, \
    radio_steps_table_csv_upload, full_steps_table_csv_upload
from utils.general import restart_automation, get_passwords, get_configs, get_logger
from utils.grep_collector import run_grep_check
from utils.telegram_bot import telegram_send_message
from datetime import datetime
import traceback

if __name__ == '__main__':

    # define se trunca automation progress forçado
    restart_auto_process = False

    # FLAG PARA RESETAR AUTOMATION_PROGRESS
    infinity_run = False

    # Flags para etapas do processo
    grep = True
    collect = True
    csv_generate = True
    csv_upload = True
    chart_update = True

    # flag para regionais, default = CO, SP, RJ e ES
    regional_default = True
    regional_nordeste = False #FALSE

    # define texto base para seleção da tabela no BACOS
    if regional_default:
        regional_info = ''
    elif regional_nordeste:
        regional_info = '_other'
    else:
        regional_info = ''

    # funções para leitura das configurações em ../media/general/
    passwords = get_passwords()
    configs = get_configs()
    logger = get_logger()


    try:
        while True:

            # conecta no gesa e roda grep para todas as UFs mensionadas no arquivo config.txt
            if grep:
                # define lista de vendors para grep
                vendors = ['HUAWEI', 'CISCO', 'TELLABS', 'CORIANT']
                run_grep_check(regional_info, passwords, configs, restart_auto_process,vendors)

            # coleta logs em todos os equipamentos listados pelo grep (HUAWEI, CISCO, TELLABS)
            if collect:
                # define lista de vendors para coleta
                vendors = ['HUAWEI', 'CISCO']
                autogui_collect(definitive=False, gesa_id=1, attempt=1, regional_info=regional_info, passwords=passwords, vendors=vendors, configs=configs)
                vendors = ['TELLABS', 'CORIANT']
                autogui_collect(definitive=False, gesa_id=2, attempt=2, regional_info=regional_info, passwords=passwords, vendors=vendors,configs=configs)
                vendors = ['HUAWEI', 'CISCO','TELLABS', 'CORIANT']
                autogui_collect(definitive=True, gesa_id=3, attempt=3, regional_info=regional_info, passwords=passwords, vendors=vendors, configs=configs)

                ne_metro_csv_upload(regional_info, passwords)

            if csv_generate:
                run_csv_generation(regional_info, passwords)

            if csv_upload:
                telegram_send_message('CSV Upload Start!')
                interface_csv_upload(regional_info, passwords)
                mac_csv_upload(regional_info, passwords)
                routing_table_csv_upload(regional_info, passwords)
                full_routing_table_csv_upload(regional_info, passwords)

            if chart_update and regional_default:
                telegram_send_message('Chart Update Start!')
                simple_chart_table_csv_upload(passwords)
                metro_steps_table_csv_upload(passwords)
                radio_steps_table_csv_upload(passwords)
                full_steps_table_csv_upload(passwords)

            if infinity_run and chart_update and csv_upload and csv_generate:
                restart_automation(passwords)
                telegram_send_message('Ready to restart process!')
                break
            else:
                break

    except Exception as e:
        traceback.print_exc()
        telegram_send_message('FATAL ERROR! Program stoped. Please check the MR2.')
        logger.error(e, exc_info=True)
        print('Check std.log')


