import os
import re
import time
import traceback

import pandas as pd
from datetime import datetime
from sqlalchemy import create_engine
import ipaddress

from utils.general import replace, get_full_mac_table_from_csv, get_logger
from utils.parsers import check_if_float, get_int_utilization_cisco, get_int_rx_error_cisco, get_int_tx_error_cisco, \
    get_int_mac_cisco, get_int_mtu_cisco, get_int_type_cisco, get_int_desc_cisco, check_if_int, \
    get_huawei_transciever_infos, get_int_desc_huawei, get_int_mac_huawei, get_int_mtu_huawei, get_int_type_huawei, \
    get_int_speed_huawei, get_int_negotiation_huawei, get_duplex_info, get_int_ip_add, get_int_and_cost_cisco, \
    get_peers_cisco, get_int_and_cost_huawei, get_peers_huawei, get_mpls_ip_and_id, get_int_mtu_tellabs, \
    get_int_mac_tellabs, get_int_type_tellabs, get_int_speed_tellabs, get_int_negotiation_tellabs
from utils.telegram_bot import telegram_send_message


def vrf_check(row):
    if row['VRF_x'] == row['VRF_y']:
        return row['VRF_x']
    elif row['VRF_x'] == 'NULL':
        return row['VRF_y']
    elif row['VRF_y'] == 'NULL':
        return row['VRF_x']


def get_last_destination_ip(row):
    destination = row['DESTINATION']
    mask = int(row['MASK'])
    ip_range = f'{destination}/{str(mask)}'
    n = ipaddress.IPv4Network(ip_range)

    return str(n[-1])


def get_only_destination(row):
    if '/' in row['Destination/Mask']:
        destination = row['Destination/Mask'].split('/')[0].strip()
        return destination
    else:
        return row['Destination/Mask']


def get_only_mask(row):
    if '/' in row['Destination/Mask']:
        destination = row['Destination/Mask'].split('/')[1].strip()
        return destination
    else:
        return 'NULL'


def file_read_huawei(file_path, ne_name, now, save_path, csv_path):

    try:
        first_file = open(f'{file_path}', 'r')
        lines_analysis = first_file.readlines()
        first_file.close()
        start = datetime.now()

        # +=============================   NE_METRO_MAC PROCESS   ==================================+

        init_mac = [i for i, s in enumerate(lines_analysis) if 'display arp all' in s][0] + 1
        lines_mac = lines_analysis[init_mac:]
        end_mac = [i for i, s in enumerate(lines_mac) if 'display' in s][0]
        lines_mac = lines_mac[:end_mac]
        indices = [i for i, s in enumerate(lines_mac) if '----' in s]
        lines_mac = lines_mac[indices[0] + 1:indices[1]]
        df_content = [[segments.split()[0], segments.split()[1], segments.split()[2], segments.split()[4],
                       segments.split()[5]] if len(
            segments.split()) >= 6 else [segments.split()[0], segments.split()[1], segments.split()[2],
                                         segments.split()[4],
                                         'NULL'] for segments in
                      lines_mac if len(segments.split()) >= 3]
        columns = ['IP', 'MAC', 'A_OR_B', 'INTERFACE', 'VRF']
        df_mac = pd.DataFrame(df_content, columns=columns)
        df_mac_a = df_mac[df_mac['A_OR_B'] == 'I'].copy()
        df_mac_a = df_mac_a[['INTERFACE', 'IP', 'MAC', 'VRF']]
        df_mac_a.columns = ['INTERFACE', 'IP_A', 'MAC_A', 'VRF']
        df_mac_b = df_mac[df_mac['A_OR_B'] != 'I'].copy()
        df_mac_b.drop(columns=['A_OR_B'], inplace=True)
        df_mac_b = df_mac_b[['INTERFACE', 'IP', 'MAC', 'VRF']]
        df_mac_b.columns = ['INTERFACE', 'IP_B', 'MAC_B', 'VRF']
        df_mac_full = df_mac_a.merge(df_mac_b, on='INTERFACE', how='outer')
        df_mac_full.fillna('NULL', inplace=True)
        df_mac_full['VRF'] = df_mac_full.apply(lambda row: vrf_check(row), axis=1)
        df_mac_full.drop(columns=['VRF_x', 'VRF_y'], inplace=True)

        df_mac_full.insert(loc=0, column='NE_NAME', value=ne_name)
        df_mac_full.insert(loc=7, column='DATE', value=now)

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'mac_table.csv'))
        df_mac_full.to_csv(current_csv_path, mode='a', header=False, index=False)

        # +=============================   NE_METRO_INT PROCESS   ==================================+

        init_interfaces = [i for i, s in enumerate(lines_analysis) if 'display interface brief' in s][0] + 1
        lines_interfaces = lines_analysis[init_interfaces:]
        end_interfaces = [i for i, s in enumerate(lines_interfaces) if 'display' in s][0]
        lines_interfaces = lines_interfaces[:end_interfaces]
        indices = [i for i, s in enumerate(lines_interfaces) if 'nterface' in s]
        header = lines_interfaces[indices[0]]
        columns = header.split()
        quantity = len(columns)
        del lines_interfaces[0:(indices[0] + 1)]
        df_lines_interfaces = [segments.split()[:quantity] for segments in lines_interfaces]
        df_interfaces = pd.DataFrame(df_lines_interfaces, columns=columns)
        df_interfaces.dropna(subset=[columns[1]], inplace=True)
        interfaces = df_interfaces['Interface'].to_list()
        end_triggers = [s.split('(')[0] + ' current state' for s in interfaces]
        # if '-gwc-' in ne_name:
        #     end_triggers.append('>\n')
        # else:
        end_triggers.append('>display')
        interface_qos_check = []
        end_triggers_qos = ['interface ' + s.split('(')[0] + '\n' for s in interfaces]
        init_all_interfaces = [i for i, s in enumerate(lines_analysis) if 'display interface' in s][1] + 1
        lines_all_interfaces = lines_analysis[init_all_interfaces:]
        end_all_interfaces = [i for i, s in enumerate(lines_all_interfaces) if 'display' in s][0] + 1
        lines_all_interfaces = lines_all_interfaces[:end_all_interfaces]

        interface_triggers = [i for i, s in enumerate(lines_all_interfaces) if any(end in s for end in end_triggers)]


        interfaces_csv_df = pd.DataFrame(
            columns=['NE_NAME', 'INTERFACE', 'IP', 'TUNNEL_ID', 'PEER_TUNNEL_IP', 'PHY_STATUS', 'LOG_STATUS',
                     'DESCRIPTION', 'RX_UTI', 'TX_UTI', 'RX_ERROR', 'TX_ERROR', 'RX_POWER', 'TX_POWER', 'RX_SENSE_HI',
                     'RX_SENSE_LO', 'TX_SENSE_HI', 'TX_SENSE_LO', 'QOS', 'MTU', 'MAC_A', 'TYPE', 'DUPLEX', 'SPEED',
                     'NEGOTIATION','VRF','NE_PTP_STATUS','INT_PTP_CONFIG', 'DATE'])

        for i, interface in enumerate(interfaces):
            if i == 1505:
                pause = 10
            if i < len(interfaces):
                phy_status = df_interfaces[df_interfaces['Interface'] == interface]['PHY'].iloc[0]
                log_status = df_interfaces[df_interfaces['Interface'] == interface]['Protocol'].iloc[0]
                rx_uti = df_interfaces[df_interfaces['Interface'] == interface]['InUti'].iloc[0]
                tx_uti = df_interfaces[df_interfaces['Interface'] == interface]['OutUti'].iloc[0]
                rx_error = df_interfaces[df_interfaces['Interface'] == interface]['inErrors'].iloc[0]
                tx_error = df_interfaces[df_interfaces['Interface'] == interface]['outErrors'].iloc[0]
                rx_error = check_if_int(rx_error)
                tx_error = check_if_int(tx_error)
                if '(' in interface:
                    interface = interface.split('(')[0]
                #init_interface = [i for i, s in enumerate(lines_all_interfaces) if f'{interface} current state' in s][0]
                lines_interface = lines_all_interfaces[interface_triggers[i]:interface_triggers[i+1]]
                #end_interface = [i for i, s in enumerate(lines_interface) if any(end in s for end in end_triggers)][1]
                #lines_interface = lines_interface[:interface_triggers[i+1]]
                response = '\n'.join(lines_interface)
                tx_power, tx_sense_high, tx_sense_low, rx_power, rx_sense_high, rx_sense_low = get_huawei_transciever_infos(
                    response)
                description = get_int_desc_huawei(response)
                ne_name_frag = ne_name.split('-')[-3].upper()
                if any(x for x in [f'T{ne_name_frag}', f'M{ne_name_frag}', f'W{ne_name_frag}', 'M-BR', 'M_BR','I-BR','I_BR'] if
                       x in description.upper()):
                    interface_qos_check.append(interface)
                mac_a = get_int_mac_huawei(response)
                mtu = get_int_mtu_huawei(response)
                duplex = get_duplex_info(response)
                ip_address = get_int_ip_add(response)
                if 'NUL' in interface.upper():
                    ne_type = 'N/A'
                else:
                    ne_type = get_int_type_huawei(response)
                    if (ne_type == 'Unknown') and (rx_power != 'NULL'):
                        ne_type = 'Optical'
                    elif (ne_type == 'Unknown') and (rx_power == 'NULL') and (tx_power == 'NULL'):
                        ne_type = 'Empty'
                if any(x for x in ['N/A', 'Logical'] if ne_type == x):
                    speed = 'N/A'
                else:
                    speed = get_int_speed_huawei(response)
                if any(x for x in ['N/A', 'Logical'] if ne_type == x):
                    negotiation = 'N/A'
                else:
                    negotiation = get_int_negotiation_huawei(response)
                    if not negotiation:
                        if speed != 'N/A':
                            try:
                                negotiation_speed = speed.upper().replace('G', '000')
                                negotiation = f'Speed {negotiation_speed}'
                            except:
                                pass

                interface_dict = {'NE_NAME': ne_name,
                                  'INTERFACE': interface,
                                  'IP': ip_address,
                                  'PHY_STATUS': phy_status,
                                  'LOG_STATUS': log_status,
                                  'DESCRIPTION': description,
                                  'RX_UTI': rx_uti,
                                  'TX_UTI': tx_uti,
                                  'RX_ERROR': rx_error,
                                  'TX_ERROR': tx_error,
                                  'RX_POWER': rx_power,
                                  'TX_POWER': tx_power,
                                  'RX_SENSE_HI': rx_sense_high,
                                  'RX_SENSE_LO': rx_sense_low,
                                  'TX_SENSE_HI': tx_sense_high,
                                  'TX_SENSE_LO': tx_sense_low,
                                  'MTU': mtu,
                                  'MAC_A': mac_a,
                                  'TYPE': ne_type,
                                  'DUPLEX': duplex,
                                  'SPEED': speed,
                                  'NEGOTIATION': negotiation,
                                  'VRF':'',
                                  'NE_PTP_STATUS':'',
                                  'INT_PTP_CONFIG':'',
                                  'DATE': now}

                interfaces_csv_df = pd.concat([interfaces_csv_df,pd.DataFrame(interface_dict,index=[0])],ignore_index=True)

        # QOS CHECK

        init_all_qos = [i for i, s in enumerate(lines_analysis) if 'display current interface' in s][0] + 1
        lines_all_qos = lines_analysis[init_all_qos:]
        end_all_qos = [i for i, s in enumerate(lines_all_qos) if 'display' in s][0] + 1
        lines_all_qos = lines_all_qos[:end_all_qos]
        qos_info = []
        #=========vrf
        vrf_dict = {}
        vrf_lines = [s for s in lines_all_qos if 'interface' in s or 'ip binding vpn-instance' in s]
        for i, vrf in enumerate(vrf_lines):
            try:
                if 'ip binding vpn-instance' in vrf_lines[i+1]:
                    vrf_dict[vrf_lines[i].strip().split()[1]] = vrf_lines[i+1].strip().split()[-1]
            except:
                pass
        for current_interface in interfaces:
            if current_interface in vrf_dict.keys():
                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'VRF'] = vrf_dict[current_interface]
        #=========end vrf
        #=========ptp display ptp all
        if "i-br-" in ne_name:
            init_ptp = [i for i, s in enumerate(lines_analysis) if 'display ptp all' in s][0] + 1
            lines_ptp = lines_analysis[init_ptp:]
            end_ptp = [i for i, s in enumerate(lines_ptp) if ne_name in s][0]
            lines_ptp = lines_ptp[:end_ptp]
            #check ptp status
            ptp_status_line = []
            ptp_status_line_enabled = []
            ptp_status_line = [s for s in lines_ptp if 'Time lock success' in s]
            ptp_status_line_enabled = [s for s in lines_ptp if 'PTP state' in s]
            if 'enable' in ptp_status_line_enabled[0]:
                if 'Time lock success :yes' in ptp_status_line[0] or 'Time lock success  :yes' in ptp_status_line[0]:
                    interfaces_csv_df.loc[interfaces_csv_df['NE_NAME'] == ne_name, 'NE_PTP_STATUS'] = 'LOCKED'
                else:
                    interfaces_csv_df.loc[interfaces_csv_df['NE_NAME'] == ne_name, 'NE_PTP_STATUS'] = 'FREE RUNNING'
            else:
                interfaces_csv_df.loc[interfaces_csv_df['NE_NAME'] == ne_name, 'NE_PTP_STATUS'] = 'Not Configured'

            #check interface ptp configuration
            try:
                init_ptp_int_lines = [i for i, s in enumerate(lines_ptp) if 'Port info' in s][0] + 2
                lines_ptp_int = lines_ptp[init_ptp_int_lines:]
                #end_ptp_int_lines = [i for i, s in enumerate(lines_ptp_int) if 'Statistics' in s][0]
                #lines_ptp_int = lines_ptp[:end_ptp_int_lines]
                ptp_int_lines = [s for s in lines_ptp_int if 'T-BC' in s]
                int_ptp_dict = {}
                for ptp_int in ptp_int_lines:
                    #split das interfaces
                    int_ptp_dict[ptp_int.split()[0].strip()] = ptp_int.split()[1].strip()
                #checando e jogano no DF
                for current_interface in interfaces:
                    if '(' in current_interface:
                        current_interface = current_interface.split('(')[0]
                    if current_interface in int_ptp_dict.keys():
                        interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'INT_PTP_CONFIG'] = int_ptp_dict[current_interface]
            except:
                #caso não haja portas configuradas
                pass
            #=========end ptp

        for qos_check in end_triggers_qos:
            init_qos = [i for i, s in enumerate(lines_all_qos) if s == qos_check]
            if init_qos:
                init_qos = init_qos[0]
                lines_qos = lines_all_qos[init_qos:]
                end_qos = [i for i, s in enumerate(lines_qos) if
                           len(s.strip().split()) > 1 and s.strip().split()[0].upper() == 'INTERFACE']
                if len(end_qos) >= 2:
                    end_qos = end_qos[1]
                else:
                    end_qos = [i for i, s in enumerate(lines_qos) if 'display' in s][0]
                lines_qos = lines_qos[:end_qos]
                response_version = '\n'.join(lines_qos)
                qos_info.append(response_version)
        interface_qos_check = [y.split('.')[0] for y in interface_qos_check]
        interface_qos_check = list(set(interface_qos_check))
        patterns = ['port-queue be', 'port-queue af1', 'port-queue af2', 'port-queue af3',
                    'port-queue af4', 'port-queue ef', 'port-queue cs6']
        for current_qos_check in interface_qos_check:
            qos_finder = 'interface ' + current_qos_check + '\n'
            qos_response = [s for s in qos_info if qos_finder in s]
            if qos_response:
                qos_response = qos_response[0]
                if all(x.upper() in qos_response.upper() for x in patterns):
                    qos_result = 'OK'
                else:
                    qos_result = 'NOK'

                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_qos_check, 'QOS'] = qos_result

        for current_interface in interfaces:
            config_finder = 'interface ' + current_interface + '\n'
            config_response = [s for s in qos_info if config_finder in s]
            if config_response:
                config_response = config_response[0]
            else:
                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'PEER_TUNNEL_IP'] = 'NULL'

                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'TUNNEL_ID'] = 'NULL'
                continue

            mpls_ip, mpls_id = get_mpls_ip_and_id(config_response)

            interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'PEER_TUNNEL_IP'] = mpls_ip

            interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'TUNNEL_ID'] = mpls_id

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'interface_table.csv'))
        interfaces_csv_df.to_csv(current_csv_path, mode='a', header=False, index=False)

        # +=============================   NE_METRO_FULL_ROUTING PROCESS   ==================================+

        init_vpn_instances = [i for i, s in enumerate(lines_analysis) if 'display ip vpn-instance' in s][0] + 1
        lines_vpn_instances = lines_analysis[init_vpn_instances:]
        end_vpn_instances = [i for i, s in enumerate(lines_vpn_instances) if 'display' in s][0]
        lines_vpn_instances = lines_vpn_instances[:end_vpn_instances]
        lines_vpn_instances = [i for i in lines_vpn_instances if i != '\n']
        vpn_instances_table = [i for i, s in enumerate(lines_vpn_instances) if 'VPN-INSTANCE NAME' in s.upper()]
        if vpn_instances_table:
            init_vpn_instance = vpn_instances_table[0] + 1
            lines_vpn = lines_vpn_instances[init_vpn_instance:]
            vpn_instances = [x.split()[0] for x in lines_vpn]
            vpn_instances.insert(0, 'Global')
        else:
            vpn_instances = ['Global']

        columns = ['Destination/Mask', 'Proto', 'Pre', 'Cost', 'Flags', 'NextHop', 'Interface', 'VPN_Instance']
        df_content = []

        for vpn_instance in vpn_instances:
            try:
                if vpn_instance == 'Global':
                    command = 'display ip routing-table\n'
                else:
                    command = f'display ip routing-table vpn-instance {vpn_instance}'
                init_route_table = [i for i, s in enumerate(lines_analysis) if command in s][0] + 1
                lines_route_table = lines_analysis[init_route_table:]
                end_route_table = [i for i, s in enumerate(lines_route_table) if '>' in s][0]
                lines_route_table = lines_route_table[:end_route_table]
                if [i for i, s in enumerate(lines_route_table) if 'Destination/Mask' in s]:
                    init_route_table = [i for i, s in enumerate(lines_route_table) if 'Destination/Mask' in s][0] + 2
                    lines_route_table = lines_route_table[init_route_table:]
                    content_initial = [segments.split()[:7] for segments in lines_route_table]
                    for segment in content_initial:
                        if len(segment) >= 7:
                            continue
                        elif len(segment) == 6:
                            if '/' in segment[0]:
                                segment.insert(4, 0)
                            else:
                                segment.insert(0, 'NULL')
                    [segment.append(vpn_instance) for segment in content_initial]
                    df_content = df_content + content_initial
            except:
                pass

        df_routing_table = pd.DataFrame(df_content, columns=columns)
        df_routing_table.insert(0, 'NE_NAME', ne_name)

        df_routing_table['DESTINATION'] = df_routing_table.apply(lambda row: get_only_destination(row), axis=1)
        df_routing_table['MASK'] = df_routing_table.apply(lambda row: get_only_mask(row), axis=1)
        df_routing_table = df_routing_table[
            ['NE_NAME', 'Interface', 'DESTINATION', 'MASK', 'NextHop', 'Proto', 'VPN_Instance', 'Pre', 'Cost', 'Flags']]

        df_routing_table.columns = ['NE_NAME', 'INTERFACE', 'DESTINATION', 'MASK', 'NEXT_HOP', 'PROTOCOL', 'VRF', 'PRE',
                                    'COST', 'FLAGS']

        df_routing_table['DATE'] = now
        current_csv_path = os.path.abspath(os.path.join(csv_path, 'full_routing_table_table.csv'))
        df_routing_table.to_csv(current_csv_path, mode='a', header=False, index=False)

        os.rename(file_path, save_path)
        final = datetime.now()
        delta = final - start
        delta = str(delta).split('.')[0]
        print(f'Done: {ne_name} in {delta} ({final.strftime("%H:%M:%S")})')

    except Exception as e:
        print(e)
        logger = get_logger()
        logger.warning(f'Error: {ne_name}')
        logger.error(e, exc_info=True)
        print(f'Error: {ne_name}')
        traceback.print_exc()


def file_read_huawei_routing_table(file_path, source_df, ne_name, csv_path):

    try:
        first_file = open(f'{file_path}', 'r')
        lines_analysis = first_file.readlines()
        first_file.close()
        start = datetime.now()

        init_peers = [i for i, s in enumerate(lines_analysis) if 'display bgp vpnv4 all peer' in s][0] + 1
        lines_peers = lines_analysis[init_peers:]
        end_peers = [i for i, s in enumerate(lines_peers) if '>' in s][0]
        lines_peers = lines_peers[:end_peers]
        response = '\n'.join(lines_peers)
        peers = get_peers_huawei(response)
        lowest_cost = 9999999
        routing_table_df = pd.DataFrame(
            columns=['NE_NAME', 'INTERFACE', 'IP_LOCAL', 'MAC_LOCAL', 'IP_RECEIVED', 'MAC_RECEIVED', 'NE_NAME_NEXT_HOP',
                     'INTERFACE_NEXT_HOP', 'IP_NEXT_HOP', 'MAC_NEXT_HOP', 'COST', 'PEER', 'PREFERENTIAL', 'VRF',
                     'DATE'])
        for current_peer in peers:
            init_current_peer = \
                [i for i, s in enumerate(lines_analysis) if f'display ip routing-table {current_peer}' in s][0] + 1
            lines_current_peer = lines_analysis[init_current_peer:]
            end_current_peer = [i for i, s in enumerate(lines_current_peer) if '>' in s][0]
            lines_current_peer = lines_current_peer[:end_current_peer]
            response = '\n'.join(lines_current_peer)
            interface, cost = get_int_and_cost_huawei(response)
            int_df = source_df[(source_df['INTERFACE'] == interface) & (source_df['NE_NAME'] == ne_name)].copy()
            int_df['COST'] = cost
            int_df['PEER'] = current_peer
            int_df['PREFERENTIAL'] = 'False'
            #routing_table_df = routing_table_df.append(int_df, ignore_index=True)
            routing_table_df = pd.concat([routing_table_df,int_df],ignore_index=True)
            if int(cost) < lowest_cost:
                lowest_cost = int(cost)

        routing_table_df['COST'] = routing_table_df['COST'].astype(int)
        routing_table_df.loc[routing_table_df['COST'] == lowest_cost, 'PREFERENTIAL'] = 'True'
        current_csv_path = os.path.abspath(os.path.join(csv_path, 'routing_table_table.csv'))
        routing_table_df.to_csv(current_csv_path, mode='a', header=False, index=False)
        final = datetime.now()
        delta = final - start
        delta = str(delta).split('.')[0]
        print(f'Done routing-table: {ne_name} in {delta}')

    except Exception as e:
        print(e)
        logger = get_logger()
        logger.warning(f'Error: {ne_name}')
        logger.error(e, exc_info=True)
        print(f'Error on routing-table: {ne_name}')


def file_read_cisco(file_path, ne_name, now, save_path, csv_path):

    try:
        first_file = open(f'{file_path}', 'r')
        lines_analysis = first_file.readlines()
        first_file.close()
        start = datetime.now()

        # +=============================   NE_METRO_MAC PROCESS   ==================================+

        # TRANSCIEVER CHECK
        init_transciever = [i for i, s in enumerate(lines_analysis) if 'show interface transceiver detail' in s][0] + 1
        lines_transciever = lines_analysis[init_transciever:]
        end_transciever = [i for i, s in enumerate(lines_transciever) if 'show' in s][0]
        lines_transciever = lines_transciever[:end_transciever]
        columns = ['Interface', 'Optical Power', 'High Alarm', 'High Warn', 'Low Warn', 'Low Alarm']
        indices = [i for i, s in enumerate(lines_transciever) if '----' in s]
        indices = indices[-2:]
        if not indices:
            df_tx = pd.DataFrame(columns=columns)
            df_rx = pd.DataFrame(columns=columns)
        else:
            list_tx = lines_transciever[indices[0] + 1:indices[1]]
            empty_line = [i for i, s in enumerate(list_tx) if s == '\n'][0]
            list_tx = list_tx[:empty_line]
            df_content = []
            for segments in list_tx:
                segment_list = segments.split()
                if '-' in segment_list:
                    segment_list.remove('-')
                if '--' in segment_list:
                    segment_list.remove('--')
                if '---' in segment_list:
                    segment_list.remove('---')
                if '+' in segment_list:
                    segment_list.remove('+')
                if '++' in segment_list:
                    segment_list.remove('++')
                if '+++' in segment_list:
                    segment_list.remove('+++')
                df_content.append(segment_list)
            df_tx = pd.DataFrame(df_content, columns=columns)
            list_rx = lines_transciever[indices[1] + 1:]
            empty_line = [i for i, s in enumerate(list_rx) if s == '\n'][0]
            list_rx = list_rx[:empty_line]
            df_content = []
            for segments in list_rx:
                segment_list = segments.split()
                if '-' in segment_list:
                    segment_list.remove('-')
                if '--' in segment_list:
                    segment_list.remove('--')
                if '---' in segment_list:
                    segment_list.remove('---')
                if '+' in segment_list:
                    segment_list.remove('+')
                if '++' in segment_list:
                    segment_list.remove('++')
                if '+++' in segment_list:
                    segment_list.remove('+++')
                df_content.append(segment_list)
            df_rx = pd.DataFrame(df_content, columns=columns)
            df_rx['Interface'] = df_rx['Interface'].str.replace('Gi','GigabitEthernet')
            df_tx['Interface'] = df_tx['Interface'].str.replace('Gi','GigabitEthernet')

        # MAC
        init_vrf = [i for i, s in enumerate(lines_analysis) if 'show running-config | include vrf' in s][0] + 1
        lines_vrf = lines_analysis[init_vrf:]
        end_vrf = [i for i, s in enumerate(lines_vrf) if 'show' in s][0]
        lines_vrf = lines_vrf[:end_vrf]
        if not [i for i, s in enumerate(lines_vrf) if 'vrf' in s]:
            vrf_list = ['NULL']
        else:
            vrf_list = lines_vrf[:-1]
            vrf_list = [s for s in vrf_list if s.startswith('vrf') or s.startswith(' vrf')]
            vrf_list = [x.strip().split()[-1] for x in vrf_list]
            vrf_list = list(set(vrf_list))
            vrf_list.insert(0, 'NULL')
        df_content = []
        cpu_type = False
        for vrf in vrf_list:
            if vrf == 'NULL':
                command = 'show arp'
            else:
                command = f'show arp vrf {vrf}'
            init_mac = [i for i, s in enumerate(lines_analysis) if command in s]
            if not init_mac:
                continue
            init_mac = init_mac[0] + 1
            lines_mac = lines_analysis[init_mac:]
            if not (('show' in lines_mac[0]) or ('show' in lines_mac[1]) or ('show' in lines_mac[2])):
                end_mac = [i for i, s in enumerate(lines_mac) if '#' in s][0]
                lines_mac = lines_mac[:end_mac]
                if any([s for s in lines_mac if '--------' in s]):
                    cpu_type = True
                    index_mac = [i + 1 for i, s in enumerate(lines_mac) if 'Address' in s]
                    final_index = index_mac[-1]
                    mac_content = []
                    for current_index in index_mac:
                        lines_mac_current = lines_mac[current_index:]
                        if not current_index == final_index:
                            end_mac_current = [i for i, s in enumerate(lines_mac_current) if s == '\n'][0]
                            lines_mac_current = lines_mac_current[:end_mac_current]
                        mac_content = mac_content + lines_mac_current
                    lines_mac = mac_content
                else:
                    init_mac = [i for i, s in enumerate(lines_mac) if 'Protocol' in s][0] + 1
                    lines_mac = lines_mac[init_mac:]
                content_initial = [segments.split()[:6] for segments in lines_mac]
                [segment.append(vrf) for segment in content_initial]
                df_content = df_content + content_initial
        if cpu_type:
            columns = ['IP', 'A_OR_B', 'MAC', 'Protocol', 'TYPE', 'INTERFACE', 'VRF']
        else:
            columns = ['Protocol', 'IP', 'A_OR_B', 'MAC', 'TYPE', 'INTERFACE', 'VRF']
        df_mac = pd.DataFrame(df_content, columns=columns)
        df_mac_a = df_mac[df_mac['A_OR_B'] == '-'].copy()
        df_mac_a = df_mac_a[['INTERFACE', 'IP', 'MAC', 'VRF']]
        df_mac_a.columns = ['INTERFACE', 'IP_A', 'MAC_A', 'VRF']
        df_mac_b = df_mac[df_mac['A_OR_B'] != '-'].copy()
        df_mac_b.drop(columns=['A_OR_B'], inplace=True)
        df_mac_b = df_mac_b[['INTERFACE', 'IP', 'MAC', 'VRF']]
        df_mac_b.columns = ['INTERFACE', 'IP_B', 'MAC_B', 'VRF']
        df_mac_full = df_mac_a.merge(df_mac_b, on='INTERFACE', how='outer')
        df_mac_full.fillna('NULL', inplace=True)
        df_mac_full['VRF'] = df_mac_full.apply(lambda row: vrf_check(row), axis=1)
        df_mac_full.drop(columns=['VRF_x', 'VRF_y'], inplace=True)
        df_mac_full.insert(loc=0, column='NE_NAME', value=ne_name)
        df_mac_full.insert(loc=7, column='DATE', value=now)

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'mac_table.csv'))
        df_mac_full.to_csv(current_csv_path, mode='a', header=False, index=False)

        # +=============================   NE_METRO_INT PROCESS   ==================================+

        # INTERFACES
        init_interfaces = [i for i, s in enumerate(lines_analysis) if 'show ip interface brief' in s][0] + 1
        lines_interfaces = lines_analysis[init_interfaces:]
        end_interfaces = [i for i, s in enumerate(lines_interfaces) if 'show' in s][0]
        lines_interfaces = lines_interfaces[:end_interfaces]
        indices = [i for i, s in enumerate(lines_interfaces) if 'nterface' in s]
        header = lines_interfaces[indices[0]]
        columns = header.split()
        quantity = len(columns)
        del lines_interfaces[0:(indices[0] + 1)]
        lines_interfaces = [line.replace('administratively down', 'administratively-down') for line in lines_interfaces]
        df_lines_interfaces = [segments.split()[:quantity] for segments in lines_interfaces]
        df_interfaces = pd.DataFrame(df_lines_interfaces, columns=columns)
        df_interfaces.dropna(subset=[columns[1]], inplace=True)
        interfaces = df_interfaces['Interface'].to_list()
        end_triggers = []
        for end_trigger in interfaces:
            if (not ('TenGig' in end_trigger)) and ('TE' in end_trigger[:2].upper()):
                end_triggers.append(replace('Te', 'TenGigabitEthernet', end_trigger) + ' is ')
            else:
                end_triggers.append(end_trigger + ' is ')
        if '-gwc-' in ne_name or '-hl4-' in ne_name:
            end_triggers.append('#\n')
        else:
            end_triggers.append('#show')
        end_triggers_qos = []
        interface_qos_check = []
        for end_trigger in interfaces:
            if (not ('TenGig' in end_trigger)) and ('TE' in end_trigger[:2].upper()):
                end_triggers_qos.append('interface ' + replace('Te', 'TenGigabitEthernet', end_trigger) + '\n')
            else:
                end_triggers_qos.append('interface ' + end_trigger + '\n')
        init_all_interfaces = [i for i, s in enumerate(lines_analysis) if 'show interfaces' in s][0] + 1
        lines_all_interfaces = lines_analysis[init_all_interfaces:]
        interfaces_csv_df = pd.DataFrame(
            columns=['NE_NAME', 'INTERFACE', 'IP', 'TUNNEL_ID', 'PEER_TUNNEL_IP', 'PHY_STATUS', 'LOG_STATUS',
                     'DESCRIPTION', 'RX_UTI', 'TX_UTI', 'RX_ERROR', 'TX_ERROR', 'RX_POWER', 'TX_POWER', 'RX_SENSE_HI',
                     'RX_SENSE_LO', 'TX_SENSE_HI', 'TX_SENSE_LO', 'QOS', 'MTU', 'MAC_A', 'TYPE', 'DUPLEX', 'SPEED',
                     'NEGOTIATION','VRF','NE_PTP_STATUS','INT_PTP_CONFIG', 'DATE'])
        interface_triggers = [i for i, s in enumerate(lines_all_interfaces) if any(end in s for end in end_triggers)]

        for idx, interface in enumerate(interfaces):
            ip_address = df_interfaces[df_interfaces['Interface'] == interface]['IP-Address'].iloc[0]
            if (not ('TenGig' in interface)) and ('TE' in interface[:2].upper()):
                interface_founder = replace('Te', 'TenGigabitEthernet', interface)
            else:
                interface_founder = interface
            # if [i for i, s in enumerate(lines_all_interfaces) if f'{interface_founder} is ' in s]:
            if idx < len(interfaces):
                # init_interface = [i for i, s in enumerate(lines_all_interfaces) if f'{interface_founder} is ' in s][0]
                # lines_interface = lines_all_interfaces[init_interface:]
                # end_interface = [i for i, s in enumerate(lines_interface) if any(end in s for end in end_triggers)][1]
                # lines_interface = lines_interface[:end_interface]
                lines_interface = lines_all_interfaces[interface_triggers[idx]:interface_triggers[idx + 1]]
                tx_power = df_tx[df_tx['Interface'] == interface]['Optical Power'].iloc[0] if not df_tx[
                    df_tx['Interface'] == interface].empty else 'NULL'
                tx_sense_high = df_tx[df_tx['Interface'] == interface]['High Warn'].iloc[0] if not df_tx[
                    df_tx['Interface'] == interface].empty else 'NULL'
                tx_sense_low = df_tx[df_tx['Interface'] == interface]['Low Warn'].iloc[0] if not df_tx[
                    df_tx['Interface'] == interface].empty else 'NULL'
                rx_power = df_rx[df_rx['Interface'] == interface]['Optical Power'].iloc[0] if not df_rx[
                    df_rx['Interface'] == interface].empty else 'NULL'
                rx_sense_high = df_rx[df_rx['Interface'] == interface]['High Warn'].iloc[0] if not df_rx[
                    df_rx['Interface'] == interface].empty else 'NULL'
                rx_sense_low = df_rx[df_rx['Interface'] == interface]['Low Warn'].iloc[0] if not df_rx[
                    df_rx['Interface'] == interface].empty else 'NULL'
                phy_status = df_interfaces[df_interfaces['Interface'] == interface]['Status'].iloc[0] if df_interfaces[df_interfaces['Interface'] == interface]['Status'].iloc[0] != 'admin' else f'{df_interfaces[df_interfaces["Interface"] == interface]["Status"].iloc[0]} {df_interfaces[df_interfaces["Interface"] == interface]["Protocol"].iloc[0]}'
                log_status = df_interfaces[df_interfaces['Interface'] == interface]['Protocol'].iloc[0]
                tx_sense_high = check_if_float(tx_sense_high)
                tx_sense_low = check_if_float(tx_sense_low)
                rx_sense_high = check_if_float(rx_sense_high)
                rx_sense_low = check_if_float(rx_sense_low)
                tx_power = check_if_float(tx_power)
                rx_power = check_if_float(rx_power)
                response = '\n'.join(lines_interface)
                rx_uti, tx_uti = get_int_utilization_cisco(response)
                rx_error = get_int_rx_error_cisco(response)
                rx_error = check_if_int(rx_error)
                tx_error = get_int_tx_error_cisco(response)
                tx_error = check_if_int(tx_error)
                mac_a = get_int_mac_cisco(response)
                mtu = get_int_mtu_cisco(response)
                description = get_int_desc_cisco(response)
                ne_name_frag = ne_name.split('-')[-3].upper()
                if any(x for x in [f'T{ne_name_frag}', f'M{ne_name_frag}', f'W{ne_name_frag}', 'M-BR', 'M_BR','I-BR','I_BR'] if
                       x in description.upper()):
                    interface_qos_check.append(interface)
                duplex = get_duplex_info(response)
                if any(x for x in ['VL', 'LO', '.', 'BD'] if x in interface.upper()):
                    ne_type = 'Logical'
                    negotiation = 'N/A'
                else:
                    ne_type, negotiation = get_int_type_cisco(response)
                    if cpu_type:
                        if 'GI' in interface[:2].upper():
                            ne_type = 'Optical'
                        elif 'TE' in interface[:2].upper():
                            ne_type = 'Optical'
                        elif 'FA' in interface[:2].upper():
                            ne_type = 'Electrical'
                    elif (ne_type == 'Unknown') and ('DOWN' in phy_status.upper()) and ('DOWN' in log_status.upper()):
                        ne_type = 'Empty'
                    if (ne_type == 'Empty') and (tx_power != 'NULL') and (rx_power != 'NULL'):
                        ne_type = 'Optical'
                if 'GI' in interface[:2].upper():
                    speed = '1Gb'
                elif 'FA' in interface[:2].upper():
                    speed = '100Mb'
                elif 'TE' in interface[:2].upper():
                    speed = '10Gb'
                else:
                    speed = 'N/A'
                interface_dict = {'NE_NAME': ne_name,
                                  'INTERFACE': interface,
                                  'IP': ip_address,
                                  'PHY_STATUS': phy_status,
                                  'LOG_STATUS': log_status,
                                  'DESCRIPTION': description,
                                  'RX_UTI': rx_uti,
                                  'TX_UTI': tx_uti,
                                  'RX_ERROR': rx_error,
                                  'TX_ERROR': tx_error,
                                  'RX_POWER': rx_power,
                                  'TX_POWER': tx_power,
                                  'RX_SENSE_HI': rx_sense_high,
                                  'RX_SENSE_LO': rx_sense_low,
                                  'TX_SENSE_HI': tx_sense_high,
                                  'TX_SENSE_LO': tx_sense_low,
                                  'MTU': mtu,
                                  'MAC_A': mac_a,
                                  'TYPE': ne_type,
                                  'DUPLEX': duplex,
                                  'SPEED': speed,
                                  'NEGOTIATION': negotiation,
                                  'VRF':'',
                                  'NE_PTP_STATUS': '',
                                  'INT_PTP_CONFIG': '',
                                  'DATE': now}

                interfaces_csv_df = pd.concat([interfaces_csv_df,pd.DataFrame(interface_dict,index=[0])],ignore_index=True)

        # QOS CHECK
        if '-gwc-' in ne_name or '-hl4-' in ne_name:
            init_all_qos = [i for i, s in enumerate(lines_analysis) if 'show running-config interface' in s][0] + 1
        else:
            init_all_qos = [i for i, s in enumerate(lines_analysis) if 'show running-config | section interface' in s][
                               0] + 1
        lines_all_qos = lines_analysis[init_all_qos:]
        end_all_qos = [i for i, s in enumerate(lines_all_qos) if '#show' in s][0] + 1
        lines_all_qos = lines_all_qos[:end_all_qos]
        qos_info = []

        #=========vrf
        vrf_dict = {}
        vrf_lines = [s for s in lines_all_qos if 'interface' in s or 'ip vrf forwarding' in s]
        for i, vrf in enumerate(vrf_lines):
            try:
                if 'ip vrf forwarding' in vrf_lines[i+1]:
                    vrf_dict[vrf_lines[i].strip().split()[1]] = vrf_lines[i+1].strip().split()[-1]
            except:
                pass
        for current_interface in interfaces:
            if current_interface in vrf_dict.keys():
                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'VRF'] = vrf_dict[current_interface]
        #=========end vrf

        for qos_check in end_triggers_qos:
            init_qos = [i for i, s in enumerate(lines_all_qos) if s == qos_check]
            if init_qos:
                init_qos = init_qos[0]
                lines_qos = lines_all_qos[init_qos:]
                end_qos = [i for i, s in enumerate(lines_qos) if
                           len(s.strip().split()) > 1 and s.strip().split()[0].upper() == 'INTERFACE']
                if len(end_qos) >= 2:
                    end_qos = end_qos[1]
                else:
                    end_qos = [i for i, s in enumerate(lines_qos) if '#show' in s][0]
                lines_qos = lines_qos[:end_qos]
                response_version = '\n'.join(lines_qos)
                qos_info.append(response_version)
        interface_qos_check = [y.split('.')[0] for y in interface_qos_check]
        interface_qos_check = list(set(interface_qos_check))
        for current_qos_check in interface_qos_check:
            if current_qos_check.upper()[:2] == 'VL':
                qos_finder = f'service instance {"".join(filter(str.isdigit, current_qos_check))}'
            else:
                if (not ('TenGig' in current_qos_check)) and ('TE' in current_qos_check[:2].upper()):
                    qos_finder = 'interface ' + replace('Te', 'TenGigabitEthernet', current_qos_check) + '\n'
                else:
                    qos_finder = 'interface ' + current_qos_check + '\n'
            qos_response = [s for s in qos_info if qos_finder in s]
            if qos_response:
                qos_response = qos_response[0]
                all_finds = [m.start() for m in re.finditer('SERVICE-POLICY', qos_response.upper())]
                if '-gwc-' in ne_name or '-hl4-' in ne_name:
                    if len(all_finds) >= 1:
                        qos_result = 'OK'
                    else:
                        qos_result = 'NOK'
                else:
                    if len(all_finds) >= 2:
                        qos_result = 'OK'
                    else:
                        qos_result = 'NOK'
                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_qos_check, 'QOS'] = qos_result

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'interface_table.csv'))
        interfaces_csv_df['TUNNEL_ID'] = 'NULL'
        interfaces_csv_df['PEER_TUNNEL_IP'] = 'NULL'
        interfaces_csv_df.to_csv(current_csv_path, mode='a', header=False, index=False)

        # +=============================   NE_METRO_FULL_ROUTING PROCESS   ==================================+

        protocol_dict = {'L': 'local',
                         'C': 'connected',
                         'S': 'static',
                         'R': 'RIP',
                         'M': 'mobile',
                         'B': 'BGP',
                         'D': 'EIGRP',
                         'EX': 'EIGRP external',
                         'O': 'OSPF',
                         'IA': 'OSPF inter area',
                         'N1': 'OSPF NSSA external type 1',
                         'N2': 'OSPF NSSA external type 2',
                         'E1': 'OSPF external type 1',
                         'E2': 'OSPF external type 2',
                         'i': 'IS-IS',
                         'su': 'IS-IS summary',
                         'L1': 'IS-IS level-1',
                         'L2': 'IS-IS level-2',
                         'ia': 'IS-IS inter area',
                         '*': 'candidate default',
                         'U': 'per-user static route',
                         'o': 'ODR',
                         'P': 'periodic downloaded static route',
                         'H': 'NHRP',
                         'l': 'LISP',
                         'a': 'application route',
                         '+': 'replicated route',
                         '%': 'next hop override',
                         'p': 'overrides from PfR'}

        init_vrf = [i for i, s in enumerate(lines_analysis) if 'show running-config | include vrf' in s][0] + 1
        lines_vrf = lines_analysis[init_vrf:]
        end_vrf = [i for i, s in enumerate(lines_vrf) if 'show' in s][0]
        lines_vrf = lines_vrf[:end_vrf]
        vrf_list = lines_vrf[:-1]
        vrf_list = [x.strip().split()[-1] for x in vrf_list]
        vrf_list = list(set(vrf_list))
        vrf_list.insert(0, 'Global')
        df_content = []

        for current_vrf in vrf_list:
            try:
                if current_vrf == 'Global':
                    command = 'show ip route\n'
                else:
                    command = f'show ip route vrf {current_vrf}'
                init_route_table = [i for i, s in enumerate(lines_analysis) if command in s][0] + 1
                lines_route_table = lines_analysis[init_route_table:]
                end_route_table = [i for i, s in enumerate(lines_route_table) if '#' in s][0]
                lines_route_table = lines_route_table[:end_route_table]
                init_lines_route_table = [i for i, s in enumerate(lines_route_table) if 'GATEWAY OF LAST' in s.upper()]
                if init_lines_route_table:
                    lines_route_table = lines_route_table[init_lines_route_table[0] + 1:]

                    first_line = True
                    new_lines_route_table = []
                    imported = False
                    last_non_space = ''

                    for current_line in lines_route_table:
                        if 'SUB' in current_line.upper():
                            continue
                        if current_line == '\n':
                            continue
                        if first_line:
                            first_line = False
                            last_non_space = current_line
                        elif current_line[0] != ' ':
                            if not imported:
                                new_lines_route_table.append(last_non_space)

                            last_non_space = current_line
                            imported = False
                        else:
                            new_line = f'{last_non_space[:-1]} {current_line}'
                            new_lines_route_table.append(new_line)
                            imported = True

                    if not imported:
                        if not current_line == '\n':
                            new_lines_route_table.append(current_line)

                    for line_route in new_lines_route_table:
                        protocol = line_route[:5].strip().split()
                        if len(protocol) >= 2:
                            protocol = protocol[-1]
                            protocol = protocol_dict[protocol]
                        else:
                            protocol = protocol[0]
                            if '*' in protocol:
                                protocol = protocol.split("*")
                                if protocol[-1] == '':
                                    protocol = protocol[0]
                                else:
                                    protocol = protocol[-1]

                                protocol = f'Default {protocol_dict[protocol]}'
                            else:
                                protocol = protocol_dict[protocol]

                        destination = line_route[5:]
                        if 'VIA' in destination.upper():
                            destination = destination.split('[')[0].strip()
                        elif 'DIRECT' in destination.upper():
                            destination = destination.upper().split('IS')[0].strip()

                        next_hop = line_route[5:]
                        if 'VIA' in next_hop.upper():
                            next_hop = next_hop.split(',')[0].strip()
                            next_hop = next_hop.upper().split('VIA')[1].strip()
                            if '(' in next_hop:
                                next_hop = next_hop.split('(')[0].strip()
                        elif 'DIRECT' in next_hop.upper():
                            next_hop = 'NULL'

                        routing_interface = line_route[5:]
                        if 'VIA' in routing_interface.upper():
                            routing_interface = routing_interface.split(',')
                            if len(routing_interface) >= 3:
                                routing_interface = routing_interface[-1].strip()
                            else:
                                routing_interface = 'NULL'
                        elif 'DIRECT' in routing_interface.upper():
                            routing_interface = routing_interface.split(',')[-1].strip()

                        pre_cost = line_route[5:]
                        if '[' in pre_cost:
                            pre_cost = pre_cost.split('[')[1].split(']')[0].strip()
                            pre = int(pre_cost.split('/')[0])
                            cost = int(pre_cost.split('/')[1])
                        else:
                            cost = pre = 0

                        df_content.append(
                            [ne_name, destination, protocol, pre, cost, 'NULL', next_hop, routing_interface,
                             current_vrf])
            except:
                pass

        columns = ['NE_NAME', 'Destination/Mask', 'Proto', 'Pre', 'Cost', 'Flags', 'NextHop', 'Interface',
                   'VPN_Instance']

        df_routing_table = pd.DataFrame(df_content, columns=columns)

        df_routing_table['DESTINATION'] = df_routing_table.apply(lambda row: get_only_destination(row), axis=1)
        df_routing_table['MASK'] = df_routing_table.apply(lambda row: get_only_mask(row), axis=1)
        df_routing_table = df_routing_table[
            ['NE_NAME', 'Interface', 'DESTINATION', 'MASK', 'NextHop', 'Proto', 'VPN_Instance', 'Pre', 'Cost', 'Flags']]

        df_routing_table.columns = ['NE_NAME', 'INTERFACE', 'DESTINATION', 'MASK', 'NEXT_HOP', 'PROTOCOL', 'VRF', 'PRE',
                                    'COST', 'FLAGS']

        df_routing_table['DATE'] = now
        current_csv_path = os.path.abspath(os.path.join(csv_path, 'full_routing_table_table.csv'))
        df_routing_table.to_csv(current_csv_path, mode='a', header=False, index=False)

        os.rename(file_path, save_path)
        final = datetime.now()
        delta = final - start
        delta = str(delta).split('.')[0]
        print(f'Done: {ne_name} in {delta}')

    except Exception as e:
        traceback.print_tb(e.__traceback__)
        print(e)
        logger = get_logger()
        logger.warning(f'Error: {ne_name}')
        logger.error(e, exc_info=True)
        print(f'Error: {ne_name}')


def file_read_cisco_xr(file_path, ne_name, now, save_path, csv_path):

    try:
        first_file = open(f'{file_path}', 'r')
        lines_analysis = first_file.readlines()
        first_file.close()
        start = datetime.now()

        # +=============================   NE_METRO_MAC PROCESS   ==================================+

        # TRANSCIEVER CHECK
        init_transciever = [i for i, s in enumerate(lines_analysis) if 'show interface transceiver detail' in s][0] + 1
        lines_transciever = lines_analysis[init_transciever:]
        end_transciever = [i for i, s in enumerate(lines_transciever) if 'show' in s][0]
        lines_transciever = lines_transciever[:end_transciever]
        columns = ['Interface', 'Optical Power', 'High Alarm', 'High Warn', 'Low Warn', 'Low Alarm']
        indices = [i for i, s in enumerate(lines_transciever) if '----' in s]
        indices = indices[-2:]
        if not indices:
            df_tx = pd.DataFrame(columns=columns)
            df_rx = pd.DataFrame(columns=columns)
        else:
            list_tx = lines_transciever[indices[0] + 1:indices[1]]
            empty_line = [i for i, s in enumerate(list_tx) if s == '\n'][0]
            list_tx = list_tx[:empty_line]
            df_content = []
            for segments in list_tx:
                segment_list = segments.split()
                if '-' in segment_list:
                    segment_list.remove('-')
                if '--' in segment_list:
                    segment_list.remove('--')
                if '---' in segment_list:
                    segment_list.remove('---')
                if '+' in segment_list:
                    segment_list.remove('+')
                if '++' in segment_list:
                    segment_list.remove('++')
                if '+++' in segment_list:
                    segment_list.remove('+++')
                df_content.append(segment_list)
            df_tx = pd.DataFrame(df_content, columns=columns)
            list_rx = lines_transciever[indices[1] + 1:]
            empty_line = [i for i, s in enumerate(list_rx) if s == '\n'][0]
            list_rx = list_rx[:empty_line]
            df_content = []
            for segments in list_rx:
                segment_list = segments.split()
                if '-' in segment_list:
                    segment_list.remove('-')
                if '--' in segment_list:
                    segment_list.remove('--')
                if '---' in segment_list:
                    segment_list.remove('---')
                if '+' in segment_list:
                    segment_list.remove('+')
                if '++' in segment_list:
                    segment_list.remove('++')
                if '+++' in segment_list:
                    segment_list.remove('+++')
                df_content.append(segment_list)
            df_rx = pd.DataFrame(df_content, columns=columns)
            df_rx['Interface'] = df_rx['Interface'].str.replace('Gi','GigabitEthernet')
            df_tx['Interface'] = df_tx['Interface'].str.replace('Gi','GigabitEthernet')

        # MAC
        init_vrf = [i for i, s in enumerate(lines_analysis) if 'show running-config | include vrf' in s][0] + 1
        lines_vrf = lines_analysis[init_vrf:]
        end_vrf = [i for i, s in enumerate(lines_vrf) if 'show' in s][0]
        lines_vrf = lines_vrf[:end_vrf]
        if not [i for i, s in enumerate(lines_vrf) if 'vrf' in s]:
            vrf_list = ['NULL']
        else:
            vrf_list = lines_vrf[:-1]
            vrf_list = [s for s in vrf_list if s.startswith('vrf') or s.startswith(' vrf')]
            vrf_list = [x.strip().split()[-1] for x in vrf_list]
            vrf_list = list(set(vrf_list))
            vrf_list.insert(0, 'NULL')
        df_content = []
        cpu_type = False
        for vrf in vrf_list:
            if vrf == 'NULL':
                command = 'show arp'
            else:
                command = f'show arp vrf {vrf}'
            init_mac = [i for i, s in enumerate(lines_analysis) if command in s]
            if not init_mac:
                continue
            init_mac = init_mac[0] + 1
            lines_mac = lines_analysis[init_mac:]
            if not (('show' in lines_mac[1]) or ('show' in lines_mac[2]) or ('show' in lines_mac[3])):
                end_mac = [i for i, s in enumerate(lines_mac) if '#' in s][0]
                lines_mac = lines_mac[:end_mac]
                if any([s for s in lines_mac if '--------' in s]):
                    cpu_type = True
                    index_mac = [i + 1 for i, s in enumerate(lines_mac) if 'Address' in s]
                    final_index = index_mac[-1]
                    mac_content = []
                    for current_index in index_mac:
                        lines_mac_current = lines_mac[current_index:]
                        if not current_index == final_index:
                            end_mac_current = [i for i, s in enumerate(lines_mac_current) if s == '\n'][0]
                            lines_mac_current = lines_mac_current[:end_mac_current]
                        mac_content = mac_content + lines_mac_current
                    lines_mac = mac_content
                else:
                    init_mac = [i for i, s in enumerate(lines_mac) if 'Address' in s][0] + 1
                    lines_mac = lines_mac[init_mac:]
                content_initial = [segments.split()[:6] for segments in lines_mac]
                [segment.append(vrf) for segment in content_initial]
                df_content = df_content + content_initial
        if cpu_type:
            columns = ['IP', 'A_OR_B', 'MAC', 'STATE', 'TYPE', 'INTERFACE', 'VRF']
        else:
            columns = ['IP', 'A_OR_B', 'MAC', 'STATE', 'TYPE', 'INTERFACE', 'VRF']

        df_mac = pd.DataFrame(df_content, columns=columns)
        df_mac_a = df_mac[df_mac['A_OR_B'] == '-'].copy()
        df_mac_a = df_mac_a[['INTERFACE', 'IP', 'MAC', 'VRF']]
        df_mac_a.columns = ['INTERFACE', 'IP_A', 'MAC_A', 'VRF']
        df_mac_b = df_mac[df_mac['A_OR_B'] != '-'].copy()
        df_mac_b.drop(columns=['A_OR_B'], inplace=True)
        df_mac_b = df_mac_b[['INTERFACE', 'IP', 'MAC', 'VRF']]
        df_mac_b.columns = ['INTERFACE', 'IP_B', 'MAC_B', 'VRF']
        df_mac_full = df_mac_a.merge(df_mac_b, on='INTERFACE', how='outer')
        df_mac_full.fillna('NULL', inplace=True)
        df_mac_full['VRF'] = df_mac_full.apply(lambda row: vrf_check(row), axis=1)
        df_mac_full.drop(columns=['VRF_x', 'VRF_y'], inplace=True)
        df_mac_full.insert(loc=0, column='NE_NAME', value=ne_name)
        df_mac_full.insert(loc=7, column='DATE', value=now)

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'mac_table.csv'))
        df_mac_full.to_csv(current_csv_path, mode='a', header=False, index=False)

        # +=============================   NE_METRO_INT PROCESS   ==================================+

        # INTERFACES
        init_interfaces = [i for i, s in enumerate(lines_analysis) if 'show ip interface brief' in s][0] + 1
        lines_interfaces = lines_analysis[init_interfaces:]
        end_interfaces = [i for i, s in enumerate(lines_interfaces) if 'show' in s][0]
        lines_interfaces = lines_interfaces[:end_interfaces]
        indices = [i for i, s in enumerate(lines_interfaces) if 'nterface' in s]
        header = lines_interfaces[indices[0]]
        columns = header.split()
        quantity = len(columns)
        del lines_interfaces[0:(indices[0] + 1)]
        lines_interfaces = [line.replace('administratively down', 'administratively-down') for line in lines_interfaces]
        df_lines_interfaces = [segments.split()[:quantity] for segments in lines_interfaces]
        df_interfaces = pd.DataFrame(df_lines_interfaces, columns=columns)
        df_interfaces.dropna(subset=[columns[1]], inplace=True)
        interfaces = df_interfaces['Interface'].to_list()
        end_triggers = []
        for end_trigger in interfaces:
            if (not ('TenGig' in end_trigger)) and ('TE' in end_trigger[:2].upper()):
                end_triggers.append(replace('Te', 'TenGigabitEthernet', end_trigger) + ' is ')
            else:
                end_triggers.append(end_trigger + ' is ')
        if '-gwc-' in ne_name or '-hl4-' in ne_name:
            end_triggers.append('#\n')
        else:
            end_triggers.append('show')
        end_triggers_qos = []
        interface_qos_check = []
        for end_trigger in interfaces:
            if (not ('TenGig' in end_trigger)) and ('TE' in end_trigger[:2].upper()):
                end_triggers_qos.append('interface ' + replace('Te', 'TenGigabitEthernet', end_trigger) + '\n')
            else:
                end_triggers_qos.append('interface ' + end_trigger + '\n')
        init_all_interfaces = [i for i, s in enumerate(lines_analysis) if 'show interfaces' in s][0] + 1
        lines_all_interfaces = lines_analysis[init_all_interfaces:]
        interfaces_csv_df = pd.DataFrame(
            columns=['NE_NAME', 'INTERFACE', 'IP', 'TUNNEL_ID', 'PEER_TUNNEL_IP', 'PHY_STATUS', 'LOG_STATUS',
                     'DESCRIPTION', 'RX_UTI', 'TX_UTI', 'RX_ERROR', 'TX_ERROR', 'RX_POWER', 'TX_POWER', 'RX_SENSE_HI',
                     'RX_SENSE_LO', 'TX_SENSE_HI', 'TX_SENSE_LO', 'QOS', 'MTU', 'MAC_A', 'TYPE', 'DUPLEX', 'SPEED',
                     'NEGOTIATION','VRF','NE_PTP_STATUS','INT_PTP_CONFIG', 'DATE'])
        interface_triggers = [i for i, s in enumerate(lines_all_interfaces) if any(end in s for end in end_triggers)]

        for idx, interface in enumerate(interfaces):
            ip_address = df_interfaces[df_interfaces['Interface'] == interface]['IP-Address'].iloc[0]
            if (not ('TenGig' in interface)) and ('TE' in interface[:2].upper()):
                interface_founder = replace('Te', 'TenGigabitEthernet', interface)
            else:
                interface_founder = interface
            # if [i for i, s in enumerate(lines_all_interfaces) if f'{interface_founder} is ' in s]:
            if idx < len(interfaces):
                # init_interface = [i for i, s in enumerate(lines_all_interfaces) if f'{interface_founder} is ' in s][0]
                # lines_interface = lines_all_interfaces[init_interface:]
                # end_interface = [i for i, s in enumerate(lines_interface) if any(end in s for end in end_triggers)][1]
                # lines_interface = lines_interface[:end_interface]
                lines_interface = lines_all_interfaces[interface_triggers[idx]:interface_triggers[idx + 1]]

                tx_power = df_tx[df_tx['Interface'] == interface]['Optical Power'].iloc[0] if not df_tx[
                    df_tx['Interface'] == interface].empty else 'NULL'
                tx_sense_high = df_tx[df_tx['Interface'] == interface]['High Warn'].iloc[0] if not df_tx[
                    df_tx['Interface'] == interface].empty else 'NULL'
                tx_sense_low = df_tx[df_tx['Interface'] == interface]['Low Warn'].iloc[0] if not df_tx[
                    df_tx['Interface'] == interface].empty else 'NULL'
                rx_power = df_rx[df_rx['Interface'] == interface]['Optical Power'].iloc[0] if not df_rx[
                    df_rx['Interface'] == interface].empty else 'NULL'
                rx_sense_high = df_rx[df_rx['Interface'] == interface]['High Warn'].iloc[0] if not df_rx[
                    df_rx['Interface'] == interface].empty else 'NULL'
                rx_sense_low = df_rx[df_rx['Interface'] == interface]['Low Warn'].iloc[0] if not df_rx[
                    df_rx['Interface'] == interface].empty else 'NULL'
                phy_status = df_interfaces[df_interfaces['Interface'] == interface]['Status'].iloc[0] if df_interfaces[df_interfaces['Interface'] == interface]['Status'].iloc[0] != 'admin' else f'{df_interfaces[df_interfaces["Interface"] == interface]["Status"].iloc[0]} {df_interfaces[df_interfaces["Interface"] == interface]["Protocol"].iloc[0]}'
                log_status = df_interfaces[df_interfaces['Interface'] == interface]['Protocol'].iloc[0]
                tx_sense_high = check_if_float(tx_sense_high)
                tx_sense_low = check_if_float(tx_sense_low)
                rx_sense_high = check_if_float(rx_sense_high)
                rx_sense_low = check_if_float(rx_sense_low)
                tx_power = check_if_float(tx_power)
                rx_power = check_if_float(rx_power)
                response = '\n'.join(lines_interface)
                rx_uti, tx_uti = get_int_utilization_cisco(response)
                rx_error = get_int_rx_error_cisco(response)
                rx_error = check_if_int(rx_error)
                tx_error = get_int_tx_error_cisco(response)
                tx_error = check_if_int(tx_error)
                mac_a = get_int_mac_cisco(response)
                mtu = get_int_mtu_cisco(response)
                description = get_int_desc_cisco(response)
                ne_name_frag = ne_name.split('-')[-3].upper()
                if any(x for x in [f'T{ne_name_frag}', f'M{ne_name_frag}', f'W{ne_name_frag}', 'M-BR', 'M_BR','I-BR','I_BR'] if
                       x in description.upper()):
                    interface_qos_check.append(interface)
                duplex = get_duplex_info(response)
                if any(x for x in ['VL', 'LO', '.', 'BD'] if x in interface.upper()):
                    ne_type = 'Logical'
                    negotiation = 'N/A'
                else:
                    ne_type, negotiation = get_int_type_cisco(response)
                    if cpu_type:
                        if 'GI' in interface[:2].upper():
                            ne_type = 'Optical'
                        elif 'TE' in interface[:2].upper():
                            ne_type = 'Optical'
                        elif 'FA' in interface[:2].upper():
                            ne_type = 'Electrical'
                    elif (ne_type == 'Unknown') and ('DOWN' in phy_status.upper()) and ('DOWN' in log_status.upper()):
                        ne_type = 'Empty'
                    if (ne_type == 'Empty') and (tx_power != 'NULL') and (rx_power != 'NULL'):
                        ne_type = 'Optical'
                if 'GI' in interface[:2].upper():
                    speed = '1Gb'
                elif 'FA' in interface[:2].upper():
                    speed = '100Mb'
                elif 'TE' in interface[:2].upper():
                    speed = '10Gb'
                else:
                    speed = 'N/A'
                interface_dict = {'NE_NAME': ne_name,
                                  'INTERFACE': interface,
                                  'IP': ip_address,
                                  'PHY_STATUS': phy_status,
                                  'LOG_STATUS': log_status,
                                  'DESCRIPTION': description,
                                  'RX_UTI': rx_uti,
                                  'TX_UTI': tx_uti,
                                  'RX_ERROR': rx_error,
                                  'TX_ERROR': tx_error,
                                  'RX_POWER': rx_power,
                                  'TX_POWER': tx_power,
                                  'RX_SENSE_HI': rx_sense_high,
                                  'RX_SENSE_LO': rx_sense_low,
                                  'TX_SENSE_HI': tx_sense_high,
                                  'TX_SENSE_LO': tx_sense_low,
                                  'MTU': mtu,
                                  'MAC_A': mac_a,
                                  'TYPE': ne_type,
                                  'DUPLEX': duplex,
                                  'SPEED': speed,
                                  'NEGOTIATION': negotiation,
                                  'VRF':'',
                                  'NE_PTP_STATUS': '',
                                  'INT_PTP_CONFIG': '',
                                  'DATE': now}

                interfaces_csv_df = pd.concat([interfaces_csv_df,pd.DataFrame(interface_dict,index=[0])],ignore_index=True)

        # QOS CHECK
        init_all_qos = [i for i, s in enumerate(lines_analysis) if 'show running-config interface' in s][0] + 1
        lines_all_qos = lines_analysis[init_all_qos:]
        end_all_qos = [i for i, s in enumerate(lines_all_qos) if f'show ip' in s][0] + 1
        lines_all_qos = lines_all_qos[:end_all_qos]
        qos_info = []

        #=========vrf
        vrf_dict = {}
        vrf_lines = [s for s in lines_all_qos if 'interface' in s or 'vrf' in s]
        for i, vrf in enumerate(vrf_lines):
            try:
                if 'vrf' in vrf_lines[i+1]:
                    vrf_dict[vrf_lines[i].strip().split()[1]] = vrf_lines[i+1].strip().split()[-1]
            except:
                pass
        for current_interface in interfaces:
            if current_interface in vrf_dict.keys():
                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_interface, 'VRF'] = vrf_dict[current_interface]
        #=========end vrf

        for qos_check in end_triggers_qos:
            init_qos = [i for i, s in enumerate(lines_all_qos) if s == qos_check]
            if init_qos:
                init_qos = init_qos[0]
                lines_qos = lines_all_qos[init_qos:]
                end_qos = [i for i, s in enumerate(lines_qos) if
                           len(s.strip().split()) > 1 and s.strip().split()[0].upper() == 'INTERFACE']
                if len(end_qos) >= 2:
                    end_qos = end_qos[1]
                else:
                    end_qos = [i for i, s in enumerate(lines_qos) if 'show' in s][0]
                lines_qos = lines_qos[:end_qos]
                response_version = '\n'.join(lines_qos)
                qos_info.append(response_version)
        interface_qos_check = [y.split('.')[0] for y in interface_qos_check]
        interface_qos_check = list(set(interface_qos_check))
        for current_qos_check in interface_qos_check:
            if current_qos_check.upper()[:2] == 'VL':
                qos_finder = f'service instance {"".join(filter(str.isdigit, current_qos_check))}'
            else:
                if (not ('TenGig' in current_qos_check)) and ('TE' in current_qos_check[:2].upper()):
                    qos_finder = 'interface ' + replace('Te', 'TenGigabitEthernet', current_qos_check) + '\n'
                else:
                    qos_finder = 'interface ' + current_qos_check + '\n'
            qos_response = [s for s in qos_info if qos_finder in s]
            if qos_response:
                qos_response = qos_response[0]
                all_finds = [m.start() for m in re.finditer('SERVICE-POLICY', qos_response.upper())]
                if '-gwc-' in ne_name or '-hl4-' in ne_name:
                    if len(all_finds) >= 1:
                        qos_result = 'OK'
                    else:
                        qos_result = 'NOK'
                else:
                    if len(all_finds) >= 2:
                        qos_result = 'OK'
                    else:
                        qos_result = 'NOK'
                interfaces_csv_df.loc[interfaces_csv_df['INTERFACE'] == current_qos_check, 'QOS'] = qos_result

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'interface_table.csv'))
        interfaces_csv_df['TUNNEL_ID'] = 'NULL'
        interfaces_csv_df['PEER_TUNNEL_IP'] = 'NULL'
        interfaces_csv_df.to_csv(current_csv_path, mode='a', header=False, index=False)

        # +=============================   NE_METRO_FULL_ROUTING PROCESS   ==================================+

        protocol_dict = {'L': 'local',
                         'C': 'connected',
                         'S': 'static',
                         'R': 'RIP',
                         'M': 'mobile',
                         'B': 'BGP',
                         'D': 'EIGRP',
                         'EX': 'EIGRP external',
                         'O': 'OSPF',
                         'IA': 'OSPF inter area',
                         'N1': 'OSPF NSSA external type 1',
                         'N2': 'OSPF NSSA external type 2',
                         'E1': 'OSPF external type 1',
                         'E2': 'OSPF external type 2',
                         'i': 'IS-IS',
                         'su': 'IS-IS summary',
                         'L1': 'IS-IS level-1',
                         'L2': 'IS-IS level-2',
                         'ia': 'IS-IS inter area',
                         '*': 'candidate default',
                         'U': 'per-user static route',
                         'o': 'ODR',
                         'P': 'periodic downloaded static route',
                         'H': 'NHRP',
                         'l': 'LISP',
                         'a': 'application route',
                         '+': 'replicated route',
                         '%': 'next hop override',
                         'p': 'overrides from PfR'}

        init_vrf = [i for i, s in enumerate(lines_analysis) if 'show running-config | include vrf' in s][0] + 1
        lines_vrf = lines_analysis[init_vrf:]
        end_vrf = [i for i, s in enumerate(lines_vrf) if 'show' in s][0]
        lines_vrf = lines_vrf[:end_vrf]
        vrf_list = lines_vrf[:-1]
        vrf_list = [s for s in vrf_list if s.startswith('vrf') or s.startswith(' vrf')]
        vrf_list = [x.strip().split()[-1] for x in vrf_list]
        vrf_list = list(set(vrf_list))
        vrf_list.insert(0, 'Global')
        df_content = []

        for current_vrf in vrf_list:
            try:
                if current_vrf == 'Global':
                    command = 'show ip route\n'
                else:
                    command = f'show ip route vrf {current_vrf}'
                init_route_table = [i for i, s in enumerate(lines_analysis) if command in s][0] + 1
                lines_route_table = lines_analysis[init_route_table:]
                end_route_table = [i for i, s in enumerate(lines_route_table) if '#' in s][0]
                lines_route_table = lines_route_table[:end_route_table]
                init_lines_route_table = [i for i, s in enumerate(lines_route_table) if 'GATEWAY OF LAST' in s.upper()]
                if init_lines_route_table:
                    lines_route_table = lines_route_table[init_lines_route_table[0] + 1:]

                    first_line = True
                    new_lines_route_table = []
                    imported = False
                    last_non_space = ''

                    for current_line in lines_route_table:
                        if 'SUB' in current_line.upper():
                            continue
                        if current_line == '\n':
                            continue
                        if first_line:
                            first_line = False
                            last_non_space = current_line
                        elif current_line[0] != ' ':
                            if not imported:
                                new_lines_route_table.append(last_non_space)

                            last_non_space = current_line
                            imported = False
                        else:
                            new_line = f'{last_non_space[:-1]} {current_line}'
                            new_lines_route_table.append(new_line)
                            imported = True

                    if not imported:
                        if not current_line == '\n':
                            new_lines_route_table.append(current_line)

                    for line_route in new_lines_route_table:
                        protocol = line_route[:5].strip().split()
                        if len(protocol) >= 2:
                            protocol = protocol[-1]
                            protocol = protocol_dict[protocol]
                        else:
                            protocol = protocol[0]
                            if '*' in protocol:
                                protocol = protocol.split("*")
                                if protocol[-1] == '':
                                    protocol = protocol[0]
                                else:
                                    protocol = protocol[-1]

                                protocol = f'Default {protocol_dict[protocol]}'
                            else:
                                protocol = protocol_dict[protocol]

                        destination = line_route[5:]
                        if 'VIA' in destination.upper():
                            destination = destination.split('[')[0].strip()
                        elif 'DIRECT' in destination.upper():
                            destination = destination.upper().split('IS')[0].strip()

                        next_hop = line_route[5:]
                        if 'VIA' in next_hop.upper():
                            next_hop = next_hop.split(',')[0].strip()
                            next_hop = next_hop.upper().split('VIA')[1].strip()
                            if '(' in next_hop:
                                next_hop = next_hop.split('(')[0].strip()
                        elif 'DIRECT' in next_hop.upper():
                            next_hop = 'NULL'

                        routing_interface = line_route[5:]
                        if 'VIA' in routing_interface.upper():
                            routing_interface = routing_interface.split(',')
                            if len(routing_interface) >= 3:
                                routing_interface = routing_interface[-1].strip()
                            else:
                                routing_interface = 'NULL'
                        elif 'DIRECT' in routing_interface.upper():
                            routing_interface = routing_interface.split(',')[-1].strip()

                        pre_cost = line_route[5:]
                        if '[' in pre_cost:
                            pre_cost = pre_cost.split('[')[1].split(']')[0].strip()
                            pre = int(pre_cost.split('/')[0])
                            cost = int(pre_cost.split('/')[1])
                        else:
                            cost = pre = 0

                        df_content.append(
                            [ne_name, destination, protocol, pre, cost, 'NULL', next_hop, routing_interface,
                             current_vrf])
            except:
                pass

        columns = ['NE_NAME', 'Destination/Mask', 'Proto', 'Pre', 'Cost', 'Flags', 'NextHop', 'Interface',
                   'VPN_Instance']

        df_routing_table = pd.DataFrame(df_content, columns=columns)

        df_routing_table['DESTINATION'] = df_routing_table.apply(lambda row: get_only_destination(row), axis=1)
        df_routing_table['MASK'] = df_routing_table.apply(lambda row: get_only_mask(row), axis=1)
        df_routing_table = df_routing_table[
            ['NE_NAME', 'Interface', 'DESTINATION', 'MASK', 'NextHop', 'Proto', 'VPN_Instance', 'Pre', 'Cost', 'Flags']]

        df_routing_table.columns = ['NE_NAME', 'INTERFACE', 'DESTINATION', 'MASK', 'NEXT_HOP', 'PROTOCOL', 'VRF', 'PRE',
                                    'COST', 'FLAGS']

        df_routing_table['DATE'] = now
        current_csv_path = os.path.abspath(os.path.join(csv_path, 'full_routing_table_table.csv'))
        df_routing_table.to_csv(current_csv_path, mode='a', header=False, index=False)

        os.rename(file_path, save_path)
        final = datetime.now()
        delta = final - start
        delta = str(delta).split('.')[0]
        print(f'Done: {ne_name} in {delta}')

    except Exception as e:
        traceback.print_tb(e.__traceback__)
        print(e)
        logger = get_logger()
        logger.warning(f'Error: {ne_name}')
        logger.error(e, exc_info=True)
        print(f'Error: {ne_name}')


def file_read_cisco_routing_table(file_path, source_df, ne_name, csv_path, vendor):

    try:
        first_file = open(f'{file_path}', 'r')
        lines_analysis = first_file.readlines()
        first_file.close()
        start = datetime.now()

        # ROUTING-TABLE
        init_peers = [i for i, s in enumerate(lines_analysis) if 'show ip bgp vpnv4 all summary' in s][0] + 1
        lines_peers = lines_analysis[init_peers:]
        end_peers = [i for i, s in enumerate(lines_peers) if '#' in s or '>' in s][0]
        lines_peers = lines_peers[:end_peers]
        response = '\n'.join(lines_peers)
        peers = get_peers_cisco(response)
        lowest_cost = 9999999
        routing_table_df = pd.DataFrame(
            columns=['NE_NAME', 'INTERFACE', 'IP_LOCAL', 'MAC_LOCAL', 'IP_RECEIVED', 'MAC_RECEIVED', 'NE_NAME_NEXT_HOP',
                     'INTERFACE_NEXT_HOP', 'IP_NEXT_HOP', 'MAC_NEXT_HOP', 'COST', 'PEER', 'PREFERENTIAL', 'VRF',
                     'DATE'])

        if vendor == 'TELLABS':
            if 'Total' in peers:
                peers.remove('Total')

        for current_peer in peers:
            init_current_peer = [i for i, s in enumerate(lines_analysis) if f'show ip route {current_peer}' in s][0] + 1
            lines_current_peer = lines_analysis[init_current_peer:]
            end_current_peer = [i for i, s in enumerate(lines_current_peer) if '#' in s or '>' in s][0]
            lines_current_peer = lines_current_peer[:end_current_peer]
            response = '\n'.join(lines_current_peer)
            interface, cost = get_int_and_cost_cisco(response, vendor)
            int_df = source_df[(source_df['INTERFACE'] == interface) & (source_df['NE_NAME'] == ne_name)].copy()

            int_df['COST'] = cost
            int_df['PEER'] = current_peer
            int_df['PREFERENTIAL'] = 'False'
            #routing_table_df = routing_table_df.append(int_df, ignore_index=True)
            routing_table_df = pd.concat([routing_table_df,int_df],ignore_index=True)
            if int(cost) < lowest_cost:
                lowest_cost = int(cost)

        routing_table_df['COST'] = routing_table_df['COST'].astype(int)
        routing_table_df.loc[routing_table_df['COST'] == lowest_cost, 'PREFERENTIAL'] = 'True'

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'routing_table_table.csv'))
        routing_table_df.to_csv(current_csv_path, mode='a', header=False, index=False)
        final = datetime.now()
        delta = final - start
        delta = str(delta).split('.')[0]
        print(f'Done routing-table: {ne_name} in {delta}')

    except Exception as e:
        logger = get_logger()
        logger.warning(f'Error: {ne_name}')
        logger.error(e, exc_info=True)
        print(f'Error on routing-table: {ne_name}')


def file_read_tellabs(file_path, ne_name, now, save_path, csv_path):
    try:
        first_file = open(f'{file_path}', 'r')
        lines_analysis = first_file.readlines()
        first_file.close()
        start = datetime.now()

        # GET VRFs
        # init_vrfs = [i for i, s in enumerate(lines_analysis) if 'show ip interface brief' in s][0] + 1
        # lines_vrfs = lines_analysis[init_vrfs:]
        # end_vrfs = [i for i, s in enumerate(lines_vrfs) if '#' in s][0]
        # lines_vrfs = lines_vrfs[:end_vrfs]
        # lines_vrfs = [x for x in lines_vrfs if 'VRF' in x.upper()]
        # vrfs = [x.split()[-1] for x in lines_vrfs]
        # vrfs = list(set(vrfs))

        # INTERFACES
        init_interfaces = [i for i, s in enumerate(lines_analysis) if 'show ip interface brief' in s][0] + 1
        lines_interfaces = lines_analysis[init_interfaces:]
        end_interfaces = [i for i, s in enumerate(lines_interfaces) if 'show' in s][0]
        lines_interfaces = lines_interfaces[:end_interfaces]
        indices = [i for i, s in enumerate(lines_interfaces) if 'nterface' in s]
        columns = ['interface', 'ip', 'phy_status', 'log_status', 'vrf','NE_PTP_STATUS','INT_PTP_CONFIG']
        del lines_interfaces[0:(indices[0] + 1)]
        interface_lines = []
        end_triggers = []
        for cur_line in lines_interfaces:
            if len(cur_line.split()) <= 5:
                add_traces = 5 - len(cur_line.split())
                traces_str = ' -' * add_traces
                interface_lines.append((cur_line + traces_str).split())
            else:
                cur_line_split = cur_line.split()
                if cur_line_split[4].upper() == 'VRF':
                    cur_line_split = [cur_line_split[0], cur_line_split[1], cur_line_split[2], cur_line_split[3],
                                      cur_line_split[-1]]
                else:
                    cur_line_split = [cur_line_split[0], cur_line_split[1], cur_line_split[2], cur_line_split[3], '-']
                interface_lines.append(cur_line_split)

            end_triggers.append(f'Interface {interface_lines[0]}\n')

        df_interface = pd.DataFrame(interface_lines, columns=columns)

        end_triggers = []

        interfaces = df_interface['interface'].unique().tolist()

        for cur_int in interfaces:
            end_triggers.append(f'Interface {cur_int}\n')

        end_triggers.append('show')

        df_interface = df_interface[~df_interface['interface'].str.contains('SO', case=False)]
        df_interface = df_interface[~df_interface['interface'].str.contains('NU', case=False)]
        # df_interface = df_interface[~df_interface['interface'].str.contains('MF', case=False)]

        interfaces = df_interface['interface'].unique().tolist()

        # DESCRIPTION
        init_description = [i for i, s in enumerate(lines_analysis) if 'show ip interface description' in s][0] + 1
        lines_description = lines_analysis[init_description:]
        end_description = [i for i, s in enumerate(lines_description) if 'show' in s][0]
        lines_description = lines_description[:end_description]
        df_interface['description'] = '-'

        for cur_line in lines_description:
            interface = cur_line.split()[0]
            if not interface in interfaces:
                continue
            description = cur_line.split()
            if [i for i, s in enumerate(description) if 'DESCRIPTION' in s.upper()]:
                description_idx = [i for i, s in enumerate(description) if 'DESCRIPTION' in s.upper()][0] + 1
                description = ' '.join(description[description_idx:])
                row_index = df_interface.index[df_interface['interface'] == interface].tolist()[0]
                df_interface.loc[row_index, 'description'] = description

        df_interface['rx_uti'] = 0
        df_interface['tx_uti'] = 0
        df_interface['rx_error'] = 0
        df_interface['tx_error'] = 0
        df_interface['rx_power'] = 0
        df_interface['tx_power'] = 0
        df_interface['rx_sense_hi'] = 0
        df_interface['rx_sense_lo'] = 0
        df_interface['tx_sense_hi'] = 0
        df_interface['tx_sense_lo'] = 0
        df_interface['duplex'] = '-'
        df_interface['tunnel_id'] = '-'
        df_interface['peer_tunnel_ip'] = '-'
        df_interface['date'] = now
        df_interface['qos'] = '-'
        df_interface['mtu'] = 0
        df_interface['mac_a'] = '-'
        df_interface['type'] = '-'
        df_interface['speed'] = '-'
        df_interface['negotiation'] = '-'
        df_interface['ne_name'] = ne_name

        # INTERFACES DETAILS
        init_details = [i for i, s in enumerate(lines_analysis) if 'show interface' in s][0] + 1
        lines_details = lines_analysis[init_details:]
        end_details = [i for i, s in enumerate(lines_details) if 'show' in s][0] + 1
        lines_details = lines_details[:end_details]

        init_qos = [i for i, s in enumerate(lines_analysis) if 'show running-config | begin Interfaces' in s][0] + 1
        lines_qos = lines_analysis[init_qos:]

        #=========vrf
        vrf_dict = {}
        #vrf_lines = [s for s in lines_qos if 'interface' in s or 'bridging bridging-instance' in s]
        vrf_lines = [s for s in lines_qos if 'interface' in s or 'vrf' in s or 'bridging bridging-instance' in s]
        for i, vrf in enumerate(vrf_lines):
            try:
                if 'vrf' in vrf_lines[i+1]:
                    vrf_dict[vrf_lines[i].strip().split()[1]] = vrf_lines[i+1].strip().split()[-1]
                if 'bridging bridging-instance' in vrf_lines[i+1]:
                    vrf_dict[vrf_lines[i].strip().split()[1]] = vrf_lines[i+1].strip().split()[2]
            except:
                pass
        for current_interface in interfaces:
            if current_interface in vrf_dict.keys():
                df_interface.loc[df_interface['interface'] == current_interface, 'vrf'] = vrf_dict[current_interface]
        #=========end vrf

        for cur_int in interfaces:

            cur_end_trigger = [x for x in end_triggers if f'{cur_int}\n' in x]

            if cur_end_trigger:
                cur_end_trigger = cur_end_trigger[0]
                end_triggers.remove(cur_end_trigger)

            init_interface = [i for i, s in enumerate(lines_details) if cur_end_trigger in s]
            if init_interface:
                init_interface = [i for i, s in enumerate(lines_details) if cur_end_trigger in s][0]
                lines_interface = lines_details[init_interface:]
                end_interface = [i for i, s in enumerate(lines_interface) if any(end in s for end in end_triggers)][0]
                lines_interface = lines_interface[:end_interface]

                mtu = get_int_mtu_tellabs(lines_interface)

                mac = get_int_mac_tellabs(lines_interface)

                if not ('.' or 'LAG' or 'LO') in cur_int.upper():
                    int_type = get_int_type_tellabs(lines_interface)
                else:
                    int_type = 'Logical'

                speed = get_int_speed_tellabs(lines_interface)

                negotiation = get_int_negotiation_tellabs(lines_interface)

                # QOS
                init_qos_int = [i for i, s in enumerate(lines_qos) if f'INTERFACE {cur_int.upper()}\n' in s.upper()][0]
                qos_int = lines_qos[init_qos_int:]
                end_qos_int = [i for i, s in enumerate(qos_int) if '!' in s][0]
                qos_int = qos_int[:end_qos_int]

                qos_int_line = [x for x in qos_int if 'QOS' in x.upper()]

                if qos_int_line:
                    qos_int_line = qos_int_line[0]
                    if 'ENABLE' in qos_int_line:
                        qos = 'OK'
                    else:
                        qos = 'NOK'
                else:
                    qos = 'NOK'
            else:
                int_type = 'Logical'
                qos = mac = speed = negotiation = '-'
                mtu = 0

            row_index = df_interface.index[df_interface['interface'] == cur_int].tolist()[0]
            df_interface.loc[row_index, 'qos'] = qos
            df_interface.loc[row_index, 'mtu'] = mtu
            df_interface.loc[row_index, 'mac_a'] = mac
            df_interface.loc[row_index, 'type'] = int_type
            df_interface.loc[row_index, 'speed'] = speed
            df_interface.loc[row_index, 'negotiation'] = negotiation

        df_mac = df_interface[['ne_name', 'interface', 'ip', 'mac_a', 'vrf', 'date']]

        df_interface = df_interface[
            ['ne_name', 'interface', 'ip', 'tunnel_id', 'peer_tunnel_ip', 'phy_status', 'log_status', 'description',
             'rx_uti', 'tx_uti', 'rx_error', 'tx_error', 'rx_power', 'tx_power', 'rx_sense_hi', 'rx_sense_lo',
             'tx_sense_hi', 'tx_sense_lo', 'qos', 'mtu', 'mac_a', 'type', 'duplex', 'speed', 'negotiation','vrf','NE_PTP_STATUS','INT_PTP_CONFIG', 'date']]

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'interface_table.csv'))
        df_interface.to_csv(current_csv_path, mode='a', header=False, index=False)

        # NE_METRO_MAC
        # vrfs.insert(0, 'Global')
        df_mac['mac_b'] = '-'
        df_mac['ip_b'] = '-'
        df_mac['ip_b'] = '-'

        #vrfs = ['Global']
        df_interface.loc[df_interface['vrf'] == '-', 'vrf'] = 'Global'
        vrfs = df_interface['vrf'].unique().tolist()
        for vrf in vrfs:
            if vrf == 'Global':
                command = 'show arp'
            else:
                command = f'show arp vrf {vrf}'
            init_mac = [i for i, s in enumerate(lines_analysis) if command in s]
            if not init_mac:
                continue
            init_mac = init_mac[0] + 1
            lines_mac = lines_analysis[init_mac:]
            if 'ADDRESS' in lines_mac[0].upper() or 'ADDRESS' in lines_mac[1].upper():
                end_mac = [i for i, s in enumerate(lines_mac) if 'show' in s][0]
                lines_mac = lines_mac[:end_mac]
                lines_mac = [x for x in lines_mac if not 'ADDRESS' in x.upper()]
                for cur_line_mac in lines_mac:
                    ip_b = cur_line_mac.strip().split()[0].strip()
                    mac_b = cur_line_mac.strip().split()[-2].strip()
                    mac_b = mac_b.replace(':', '')
                    mac_b = f'{mac_b[0:4]}-{mac_b[4:8]}-{mac_b[8:12]}'
                    mac_int = cur_line_mac.strip().split()[-1].strip()
                    if not df_mac[df_mac['interface'] == mac_int].empty:
                        row_index = df_mac.index[df_mac['interface'] == mac_int].tolist()[0]
                        df_mac.loc[row_index, 'ip_b'] = ip_b
                        df_mac.loc[row_index, 'mac_b'] = mac_b
                        df_mac.loc[row_index, 'vrf'] = vrf
                    else:
                        continue

        df_mac = df_mac[['ne_name', 'interface', 'ip', 'mac_a', 'ip_b', 'mac_b', 'vrf', 'date']]
        df_mac = df_mac[df_mac['vrf'] == 'Global']

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'mac_table.csv'))
        df_mac.to_csv(current_csv_path, mode='a', header=False, index=False)

        os.rename(file_path, save_path)

        final = datetime.now()

        delta = final - start

        delta = str(delta).split('.')[0]

        print(f'Done: {ne_name} in {delta}')

    except Exception as e:
        print(e)
        logger = get_logger()
        logger.warning(f'Error: {ne_name}')
        logger.error(e, exc_info=True)
        print(f'Error: {ne_name}')


def run_general_for_loop(ne_files, df_metro, directory_path, done_path, now, csv_path):
    total = len(ne_files)
    for index, file in enumerate(ne_files):

        porcentagem = ((index + 1) * 100) / total
        if porcentagem % 5 == 0:
            print(f'General Parser status : {porcentagem}%')
            telegram_send_message(f'General Parser status : {porcentagem}%')
        try:
            ne_name = file.replace('.txt', '')
            vendor = df_metro[df_metro['NE_NAME'] == ne_name]['REAL_VENDOR'].iloc[0]
            sfw_version = df_metro[df_metro['NE_NAME'] == ne_name]['SFW_VERSION'].iloc[0]
            file_path = os.path.abspath(os.path.join(directory_path, file))
            save_path = os.path.abspath(os.path.join(done_path, file))
            print(f'Doing {file} =====> {index + 1}\\{total}  [{vendor}]')
            if vendor == 'HUAWEI':
                file_read_huawei(file_path, ne_name, now, save_path, csv_path)

            elif vendor == 'CISCO':
                if 'XR' in sfw_version:
                    file_read_cisco_xr(file_path, ne_name, now, save_path, csv_path)
                else:
                    file_read_cisco(file_path, ne_name, now, save_path, csv_path)

            elif vendor == 'TELLABS':
                file_read_tellabs(file_path, ne_name, now, save_path, csv_path)
            else:
                print(f'Skip {file} : REAL_VENDOR not found on NE_METRO!')
        except Exception as err:
            print(f'Cause:  {err}')
            traceback.print_tb(err.__traceback__)


def run_routing_table_for_loop(df_metro, done_path, source_df, csv_path):
    ne_files = [f for f in os.listdir(done_path) if os.path.isfile(os.path.join(done_path, f))]
    if 'desktop.ini' in ne_files:
        ne_files.remove('desktop.ini')
    total = len(ne_files)
    for index, file in enumerate(ne_files):

        ne_name = file.replace('.txt', '')
        vendor = df_metro[df_metro['NE_NAME'] == ne_name]['REAL_VENDOR'].iloc[0]
        file_path = os.path.abspath(os.path.join(done_path, file))

        if not any(s for s in ['-gwc-','i-'] if s in ne_name):
            print(f'Doing {file} routing-table =====> {index + 1}\\{total}')
            if vendor == 'HUAWEI':
                file_read_huawei_routing_table(file_path, source_df, ne_name, csv_path)

            elif vendor == 'CISCO' or vendor == 'TELLABS':
                file_read_cisco_routing_table(file_path, source_df, ne_name, csv_path, vendor)


def clean_done_folder(done_path):
    previous_path = os.path.join(done_path, 'previous')
    done_files = [f for f in os.listdir(done_path) if os.path.isfile(os.path.join(done_path, f))]

    previous_files = [f for f in os.listdir(previous_path) if os.path.isfile(os.path.join(previous_path, f))]

    if 'desktop.ini' in previous_files:
        previous_files.remove('desktop.ini')

    if previous_files:
        for current_file in previous_files:
            os.remove(os.path.join(previous_path, current_file))

    for current_file in done_files:
        os.rename(os.path.join(done_path, current_file), os.path.join(previous_path, current_file))


def run_csv_generation(passwords):
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    ne_files = [f for f in os.listdir(directory_path) if os.path.isfile(os.path.join(directory_path, f))]
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/' \
                        f'{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)
    df_metro = pd.read_sql('SELECT * FROM NE_METRO', con=db_connection)
    df_metro.sort_values('DATE', ascending=False, inplace=True)
    done_path = os.path.abspath(os.path.join(directory_path, 'done'))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))

    now = datetime.now().date().strftime("%Y-%m-%d")
    if 'desktop.ini' in ne_files:
        ne_files.remove('desktop.ini')

    print('General Parser Started')
    telegram_send_message('Netmiko General Parser Started')
    clean_done_folder(done_path)
    run_general_for_loop(ne_files, df_metro, directory_path, done_path, now, csv_path)

    print('Routing-table Parser Started')
    telegram_send_message('Routing-table Parser Started')
    mac_file = os.path.abspath(os.path.join(csv_path, 'mac_table.csv'))
    source_df = get_full_mac_table_from_csv(mac_file)
    run_routing_table_for_loop(df_metro, done_path, source_df, csv_path)

