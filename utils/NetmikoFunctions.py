import os
import time

import pandas as pd
from datetime import datetime
import mysql.connector as mysql
from sqlalchemy import create_engine
from netmiko import ConnectHandler
from netmiko import redispatch
import platform

from utils.NetmikoGeneral import *
from utils.NetmikoProcess import first_file_analysis_huawei, first_file_analysis_cisco, first_file_analysis_tellabs, \
    first_file_analysis_nokia
from utils.NetmikoConnections import *
from utils.telegram_bot import *
import traceback


def get_progress(passwords):
    """
            Função responsável pela verificação do progesso da coleta, listando quantos sites possuimos na coleta atual
            quantos foram coletados e quantos falharam.

            :param passwords: dict => dicionário de usuário e senhas providos no arquivo passwords.txt;


    """
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/' \
                        f'{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)

    df_todo = pd.read_sql(f"SELECT * FROM AUTOMATION_PROGRESS", con=db_connection)
    total = len(df_todo)

    df_todo = pd.read_sql(f"SELECT * FROM AUTOMATION_PROGRESS WHERE COLLECTED = 'True' ", con=db_connection)
    collected = len(df_todo)

    df_todo = pd.read_sql(f"SELECT * FROM AUTOMATION_PROGRESS WHERE CONNECTED_VIA IS NULl and COL_ATTEMPT != 0",
                          con=db_connection)
    failed = len(df_todo)

    return total, collected, failed


def collect_ne(passwords, restart_auto_process):
    """
            Função responsável para verificar se possuimos caixas no banco de dados

            :param passwords: dict => dicionário de usuário e senhas providos no arquivo passwords.txt
            :param restart_auto_process: boolean => determina se iremos apagar a tabela AUTOMATION_PROGRESS

    """

    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/' \
                        f'{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)
    if restart_auto_process:
        db = mysql.connect(
            host="10.26.82.160",
            user=passwords['sql_user'],
            passwd=passwords['sql_password'],
            database=passwords['sql_schema'],
            allow_local_infile=True
        )
        # DEFINE PASTA BACKUP PARA ULTIMO AUTOMATION
        df = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)
        now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        basepath = os.path.dirname(__file__)
        media_path = os.path.abspath(os.path.join(basepath, "../media"))
        csv_path = os.path.abspath(os.path.join(media_path, "csv"))
        backup_path = os.path.abspath(os.path.join(csv_path, 'backup', f'AUTOMATION_PROGRESS_{now}.csv'))
        df.to_csv(backup_path, mode='w', header=True, index=False)

        # limpa tabela automation_progess
        cursor = db.cursor()
        exclude = 'truncate AUTOMATION_PROGRESS'
        cursor.execute(exclude)

    df = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)

    return df


def get_grep(ufs, vendors, passwords, reset_process=False, only_fusion=False):
    """
            Função responsável pela listegem dos equipamentos no jumpserver!

            :param ufs: list => relação de estados que desejamos processar;
            :param vendors: list => relação dos vendors que desejamos processar;
            :param passwords: dict => dicionário de usuário e senhas providos no arquivo passwords.txt;
            :param reset_process: default False => determina se iremos apagar a tabela AUTOMATION_PROGRESS;
            :param only_fusion: defalt False => determina se iremos processar somente rede Fusion;

    """
    # verifica se há sites na tabela AUTOMATION_PROGRESS e se a flag de reset estiver True reseta a tabela
    df_sites = collect_ne(passwords, reset_process)
    todo = df_sites.empty

    # roda a coleta de elementos apenas se a tabela AUTOMATION_PROGRESS estiver vazia
    if todo:
        # conectando no jump
        # definindo parametros do jumpserver para grep de elementos
        jump_server = {
            'device_type': 'linux',
            'ip': passwords['gesa1_host'],
            'username': passwords['gesa1_user'],
            'password': passwords['gesa1_password'],
            'port': 22
        }
        # conectando ao jump server
        jump_server_connection = ConnectHandler(**jump_server)

        try:
            print('### GREP COLLECT START! ###')
            telegram_send_message(f'Start Grep region: {ufs}')
            start = datetime.now()
            grep_response = ''
            for uf in ufs:
                grep_text = f'cat /etc/hosts |egrep "i-br-{uf}-|m-br-{uf}-" '
                grep_response += jump_server_connection.send_command_timing(grep_text, delay_factor=10)

            # filragens para deixar apenas os elementos desejados
            grep_response = grep_response.split('\n')
            filtered_lines = [x for x in grep_response if x.strip().__contains__('-br-')]
            filtered_lines = [segments.split()[:3] for segments in filtered_lines]
            df = pd.DataFrame(filtered_lines, columns=['IP', 'Name', 'Vendor'])
            if only_fusion:
                accepted_patterns = ['hl4', 'hl5', 'hl5g', 'hl5s', 'hl5d']
            else:
                accepted_patterns = ['gwc', 'gwd', 'gws', 'hl4', 'hl5', 'hl5g', 'hl5s', 'hl5d']
            pattern = '|'.join(accepted_patterns)
            df = df[df['Name'].str.contains(pattern, na=True)]
            # >>>> filtra equipamentos pelo VENDOR
            df['Vendor'] = df['Vendor'].str.upper()
            df = df[df['Vendor'].isin(vendors)]
            df.dropna(subset=['Vendor'], inplace=True)
            # >>>> filtra equipamentos pelo VENDOR
            # >>>> INSERE DATA NO DATAFRAME
            now = datetime.now().date().strftime("%Y-%m-%d")
            df['DATE'] = now
            df.drop_duplicates(inplace=True)
            # >>>> GERANDO QUERY PARA INSERIR ELEMENTOS COLETADOS PELO GREP NA TABELA AUTOMATION_PROGRESS
            query = f'INSERT INTO AUTOMATION_PROGRESS (IP, NE_NAME, VENDOR, STARTED) VALUES'
            for index, row in df.iterrows():
                query = query + f'("{row["IP"]}","{row["Name"]}", "{row["Vendor"]}", "{now}"), '

            query = query[:-2]
            db = mysql.connect(
                host="10.26.82.160",
                user=passwords['sql_user'],
                passwd=passwords['sql_password'],
                database=passwords['sql_schema'],
                allow_local_infile=True
            )
            cursor = db.cursor()
            # >>>> INSERINDO NOVOS ELEMENTOS NA TABELA
            cursor.execute(query)
            db.commit()
            # desconecta do jump-server
            jump_server_connection.disconnect()
            # verifica quanto tempo demorou para rodar
            final = datetime.now()
            delta = final - start
            delta = str(delta).split('.')[0]
            print('### GREP COLLECT FINISH! ###')
            print(f'Done: in {delta}')
            telegram_send_message(f'Done: in {delta}')

        except Exception as e:
            print('### GREP COLLECT FAIL! ###')
            traceback.print_exc(e.__traceback__)
            jump_server_connection.disconnect()


def df_todo_retry_fn(passwords):
    '''
    função para tentativa extra em sites com status Ativado no SMTX!
    Não será possível usa-la até que tenhamos mapeados no banco o status do SMTX
    :param passwords dict => dicionário com senhas e schema para processar
    '''

    # conexão com bd para rodar query de update
    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=passwords['sql_schema'],
        allow_local_infile=True
    )
    cursor = db.cursor()

    # conexão com bd para buscar lista de sites com status False no colected e Ativado no SMTX
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/' \
                        f'{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)
    df_to_fix = pd.read_sql("SELECT auto.ip, auto.NE_NAME, auto.vendor, auto.COLLECTED, param.Status_SMTX FROM "
                            "AUTOMATION_PROGRESS as auto left join tx_ne_param.NE_SMTX as param on "
                            "param.NE_NAME = auto.NE_NAME where auto.COLLECTED = 'False' "
                            "and param.Status_SMTX = 'Ativado' group by 1;", con=db_connection)
    df_empty = df_to_fix.empty
    if not df_empty:
        # se houver sites neste estado um loop para mudança do status é rodado
        for index, row in df_to_fix.iterrows():
            ne_name = row['NE_NAME']
            query = f"UPDATE AUTOMATION_PROGRESS SET COLLECTED = NULL, OM_LINK = NULL  where NE_NAME = '{ne_name}';"
            cursor.execute(query)
            db.commit()
    # caso contrário ele tentara mais uma vez em todos os sites que deram falha
    else:
        query = f"UPDATE AUTOMATION_PROGRESS SET COLLECTED = NULL, OM_LINK = NULL where COLLECTED = 'False';"
        cursor.execute(query)
        db.commit()


def get_df_todo(gesa, passwords, vendors, teste, retry=False):
    """
            Função responsável pela listegem dos para realização da coleta

            :param teste: dicionário com lista de caixas e flag para teste
            :param gesa: int => id da tentativa, assim como id do jumpserver;
            :param vendors: list => relação dos vendors que desejamos processar;
            :param passwords: dict => dicionário de usuário e senhas e schema providos no arquivo passwords.txt;
            :param retry: default False => determina se iremos tentar coletar os elementos que falharam
            mas estão com status ativo no SMTX;

    """
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/' \
                        f'{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)

    # retry é utilizado para que após a implementação do SMTX possamos realizar uma nova tentativa
    # de coleta nos sites que estão com status OK no SMTX
    if not retry:
        df_todo = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)
    else:
        df_todo_retry_fn(passwords)
        df_todo = pd.read_sql('SELECT * FROM AUTOMATION_PROGRESS', con=db_connection)

    if not teste['teste']:

        # filtra elementos por vendor
        df_todo = df_todo[df_todo['VENDOR'].isin(vendors)]

        # filtra elementos que não foram coletados ainda
        df_todo = df_todo[df_todo['COLLECTED'].isna()]

        # filtra elementos em que a tentativa é menor ou igual a atual
        df_todo['COL_ATTEMPT'] = df_todo['COL_ATTEMPT'].astype(int)
        df_todo = df_todo[df_todo['COL_ATTEMPT'] <= int(gesa)]


    else:
        # testes elementos definidos na main
        df_todo = df_todo[df_todo['NE_NAME'].isin(teste['test_list'])]

    return df_todo


def write_read_command(command, jump_server_connection, pattern, time_out=10):
    """
            Função RASCUNHO para implementação de coleta de logs em elementos TELLABS que conectam
            por TELNET, pois a lib Paramiko não possui classe coriant_telnet.

            :param command: str => string com o comando para executar no equipamento
            :param jump_server_connection: obj => objeto da conexão com o jumpserver, criado com o ConnectHandler.
            :param pattern: str => string com o patrão do terminal a ser buscado no final do comando
            :param time_out: int => padrão 10, determina quantos segundos iremos aguardar uma resposta.

    """
    # jump_server_connection.write_channel(command + '\r\n')
    page = jump_server_connection.send_command_timing(command)
    # page = jump_server_connection.send_command(command)
    output = ''
    while time_out >= 0:
        try:
            output += str(page + '\n').replace(chr(8), '')
            if 'More' in page:
                jump_server_connection.write_channel(' ')
                page = jump_server_connection.read_until_pattern(f'More|{pattern}')
            elif pattern in page:
                return output

        except:
            time.sleep(1)
            time_out -= 1
            continue

    return None


# rodar processo por tentativa/gesa
def run_attempt(passwords, gesa, vendors, test, definitive=False, retry=False):
    """
            Função responsável pela coleta! É nela que executamos as tentativas de conexão por SSH
            e Telnet nos equipamentos, e em caso de sucesso coletamos os logs!

            :param test: dicionário com lista de caixas e flag para teste
            :param passwords: dict => dicionário com senhas e schema para processar
            :param gesa: int => id do jump_server / tentativa
            :param vendors: list => quais os vendors que serão coletados
            :param definitive: boolean => se True define False na coleta após a tentativa, não sendo mais possível
            realisar novas tentativas
            :param retry: boolean => Em fase de desenvolvimento, será possível implementar após termos acesso ao SMTX

    """

    # dict para redispatch
    redispatch_dict = {'HUAWEI_SSH': 'huawei',
                       'TELLABS_SSH': 'coriant',
                       'NOKIA_SSH': 'nokia_sros',
                       'CISCO_SSH': 'cisco_ios',
                       'HUAWEI_TELNET': 'huawei_telnet',
                       'TELLABS_TELNET': 'huawei_telnet',
                       'NOKIA_TELNET': 'nokia_sros_telnet',
                       'CISCO_TELNET': 'cisco_ios_telnet',
                       'HUAWEI': 'huawei',
                       'TELLABS': 'huawei',
                       'NOKIA': 'nokia_sros',
                       'CISCO': 'cisco_ios',
                       }
    now = datetime.now().date().strftime("%Y-%m-%d")

    # define diretórios que serão utilizados para trabalhar
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    ne_metro_file_path = os.path.abspath(os.path.join(csv_path, 'ne_metro_table.csv'))

    # conectando no BACOs
    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=passwords['sql_schema'],
        allow_local_infile=True
    )

    db.autocommit = True
    cursor = db.cursor()

    # define sites para coleta
    site_list = get_df_todo(gesa, passwords, vendors, test, retry)
    site_list.reset_index(inplace=True, drop=True)

    # conectando no gesa
    jump_server = {
        'device_type': 'linux',
        'ip': passwords[f'gesa{gesa}_host'],
        'username': passwords[f'gesa{gesa}_user'],
        'password': passwords[f'gesa{gesa}_password'],
        'default_enter': '\r\n',
        'port': 22
    }

    # conectando ao jump server
    try:
        jump_server_connection = ConnectHandler(**jump_server)
        print(f"Jump server = GESA {gesa} | Retry {retry}: {jump_server_connection.find_prompt()}")
        telegram_send_message(f"Jump server = GESA {gesa} Connected")
        connected = True
    except Exception as e:
        raise e

    # percorrendo Df com elementos para ser coletado
    total = len(site_list['NE_NAME'])
    last_por = -1

    for index, row in site_list.iterrows():
        ip = row['IP']
        ne_name = row['NE_NAME']
        ne_init = ne_name.split('-')[4].upper()
        vendor = row['VENDOR']
        uf = ne_name.split('-')[2].upper()
        start = datetime.now()

        # keep alive progress
        porcentagem = int(((index + 1) * 100) / total)
        totalicus, collected, failed = get_progress(passwords)
        if porcentagem != last_por and porcentagem % 2 == 0:
            last_por = porcentagem
            telegram_send_message(f'Keep alive report:\n Gesa: {gesa} \n Progress: {porcentagem}%  \n '
                                  f'Total site number: {totalicus} \n Attempt site number: {total} \n '
                                  f'Total collected: {collected} \n Total failed: {failed}')

        # criando dataframe para ne_metro
        df_version = pd.DataFrame(columns=['NE_INIT', 'UF', 'NE_NAME', 'REAL_NE_NAME', 'IP', 'VENDOR', 'REAL_VENDOR',
                                           'HW_VERSION', 'SFW_VERSION', 'LICENSE', 'OM_LINK', 'CONNECTED_VIA', 'GESA',
                                           'DATE'])

        # define query de falha
        if definitive:
            fail_query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa}," \
                         f"OM_LINK = 'False', COLLECTED = 'False' WHERE NE_NAME = '{ne_name}'"
        else:
            fail_query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa} " \
                         f"WHERE NE_NAME = '{ne_name}'"

        # se estiver desconectado conecta novamente
        if not connected:
            jump_server_connection = ConnectHandler(**jump_server)
            connected = True

        try:
            # ================= realizando tentativa de conexão ================= Telnet ainda não funciona!
            print(f"Trying to connect:({vendor}) {ne_name} ({index + 1}/{total})=>", end=' ')
            # try ssh (PRINCIPAL)
            tacacs = ''
            print(f"ssh...", end=' ')
            connection, real_vendor = netmiko_ssh(gesa, jump_server_connection, passwords, ip, vendor)
            # try ssh sem tacacs
            if not connection and (vendor == 'HUAWEI' or vendor == 'CISCO'):
                tacacs = '(sem tacacs)'
                print(f"ssh{tacacs}...", end=' ')
                connection, real_vendor = netmiko_ssh(gesa, jump_server_connection, passwords, ip, vendor,
                                                      tacacs=False)
            # try TELNET
            if not connection:
                tacacs = ''
                print(f"telnet{tacacs}...", end=' ')
                connection, real_vendor = netmiko_telnet(gesa, jump_server_connection, passwords, ip, ne_name, vendor)
            # try TELNET sem tacacs
            if not connection and (vendor == 'HUAWEI' or vendor == 'CISCO'):
                tacacs = '(sem tacacs)'
                print(f"telnet{tacacs}...", end=' ')
                connection, real_vendor = netmiko_telnet(gesa, jump_server_connection, passwords, ip, ne_name, vendor,
                                                         tacacs=False)

            # ================= só continua se houver conexão ok =================
            if connection:
                print(f"\nConnected: {ne_name} by {connection}{tacacs}")
                pass
            else:
                print(f'\nFail to connect:  {ne_name}')
                cursor.execute(fail_query)
                continue

            # ================= define query sucesso:
            success_query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
                            f" CONNECTED_VIA = '{connection}{tacacs}', OM_LINK = 'True', COLLECTED = 'True' WHERE NE_NAME = '{ne_name}'"

            # coletando logs
            if real_vendor == 'HUAWEI':
                ne_metro_updated = False
                try:
                    redispatch(jump_server_connection, device_type=redispatch_dict[f'{real_vendor}_{connection}'])

                    # =================== Efetua a coleta de logs via paramiko START ===========================
                    log = jump_server_connection.send_command_timing('display version', strip_command=False)
                    log += jump_server_connection.send_command_timing('display arp all', strip_command=False)
                    log += jump_server_connection.send_command_timing('display interface brief', strip_command=False)
                    log += jump_server_connection.send_command_timing('display ip vpn-instance', strip_command=False)
                    log += jump_server_connection.send_command_timing('display bgp vpnv4 all peer', strip_command=False)
                    # coletando peers, vpn instances e atualizando NE_metro
                    try:
                        peers, vpn_instances = first_file_analysis_huawei(ne_name, uf, vendor, ne_init, ip, now,
                                                                          log,
                                                                          connection, ne_metro_file_path, df_version,
                                                                          real_vendor, gesa)
                        ne_metro_updated = True
                    except Exception as e:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                        print(f'Error on process Huawei log!')
                        raise e

                    log += jump_server_connection.send_command_timing('display interface', strip_command=False)
                    if not '-gwc-' in ne_name and not '-hl4-' in ne_name:
                        for peer in peers:
                            log += jump_server_connection.send_command_timing(f'display ip routing-table {peer}',
                                                                              strip_command=False)
                    log += jump_server_connection.send_command_timing('display current interface', strip_command=False)
                    log += jump_server_connection.send_command_timing('display ip routing-table', strip_command=False)
                    for vpn_instance in vpn_instances:
                        log += jump_server_connection.send_command_timing(
                            f'display ip routing-table vpn-instance {vpn_instance}', strip_command=False)
                    # CHECANDO PTP somente rede FUSION
                    if 'i-br-' in ne_name:
                        log += jump_server_connection.send_command_timing('display ptp all', strip_command=False)

                    # =================== Efetua a coleta de logs via paramiko END ===========================

                    # salvando logs no txt
                    file_name = f'{ne_name}.txt'
                    file_path = os.path.abspath(os.path.join(directory_path, file_name))
                    with open(file_path, "w") as text_file:
                        text_file.write(log)

                    # saindo do equipamento
                    jump_server_connection.send_command('quit', expect_string=r'(:|\$)')
                    # print(logout)
                    redispatch(jump_server_connection, device_type='linux')
                    # atualiza db
                    cursor.execute(success_query)
                except Exception as e:
                    if ne_metro_updated:
                        ne_metro_log_update(ne_name, ne_metro_file_path)
                    else:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                    raise e

            elif real_vendor == 'CISCO':
                ne_metro_updated = False
                try:
                    # configura conexão para o roteador
                    redispatch(jump_server_connection, device_type=redispatch_dict[f'{real_vendor}_{connection}'])

                    # =================== Efetua a coleta de logs via paramiko START ===========================
                    log = jump_server_connection.send_command_timing('show version', strip_command=False)
                    log += jump_server_connection.send_command_timing('show interface transceiver detail',
                                                                      strip_command=False)
                    log += jump_server_connection.send_command_timing('show ip interface brief', strip_command=False)
                    log += jump_server_connection.send_command_timing('show running-config | include vrf',
                                                                      strip_command=False)
                    log += jump_server_connection.send_command_timing('show ip bgp vpnv4 all summary',
                                                                      strip_command=False)
                    # coletando vrf_list, peers e atualizando NE_metro
                    try:
                        vrf_list, peers = first_file_analysis_cisco(ne_name, uf, vendor, ne_init, ip, now,
                                                                    log,
                                                                    connection, ne_metro_file_path, df_version,
                                                                    real_vendor, gesa)
                        ne_metro_updated = True
                    except Exception as e:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                        print(f'Error on process CISCO log!')
                        raise e

                    log += jump_server_connection.send_command_timing('show arp', strip_command=False)

                    for vrf in vrf_list:
                        log += jump_server_connection.send_command_timing(f'show arp vrf {vrf}', strip_command=False)

                    log += jump_server_connection.send_command_timing('show interfaces', strip_command=False)
                    if not '-gwc-' in ne_name and not 'i-br-' in ne_name:
                        for peer in peers:
                            log += jump_server_connection.send_command_timing(f'show ip route {peer}',
                                                                              strip_command=False)

                    if '-gwc-' in ne_name or 'i-br-' in ne_name:
                        log += jump_server_connection.send_command_timing('show running-config interface',
                                                                          strip_command=False)
                    else:
                        log += jump_server_connection.send_command_timing('show running-config | section interface',
                                                                          strip_command=False)

                    log += jump_server_connection.send_command_timing('show ip route', strip_command=False)
                    for current_vrf in vrf_list:
                        log += jump_server_connection.send_command_timing(f'show ip route vrf {current_vrf}',
                                                                          strip_command=False)
                    # CHECANDO PTP somente rede FUSION
                    if 'i-br-' in ne_name:
                        log += jump_server_connection.send_command_timing('Show ptp platform servo',
                                                                          strip_command=False)

                    # =================== Efetua a coleta de logs via paramiko END ===========================

                    # salvando logs no txt
                    file_name = f'{ne_name}.txt'
                    file_path = os.path.abspath(os.path.join(directory_path, file_name))
                    with open(file_path, "w") as text_file:
                        text_file.write(log)

                    # saindo do equipamento
                    jump_server_connection.send_command('quit', expect_string=r'(:|\$)')
                    # print(logout)
                    redispatch(jump_server_connection, device_type='linux')
                    # atualiza bd
                    cursor.execute(success_query)

                except Exception as e:
                    if ne_metro_updated:
                        ne_metro_log_update(ne_name, ne_metro_file_path)
                    else:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                    raise e


            # TELLABS NÃO FUNCIONA!!!!!!!!!
            elif real_vendor == 'TELLABS':
                ne_metro_updated = False
                '''
                Netmiko apresenta diversas falhas ao processar dados do sistema Coriant, não 
                foi possível realizar coleta!
                rascunho abaixo:
                try:
                    redispatch(jump_server_connection, device_type=redispatch_dict[f'{real_vendor}_{connection}'])

                    # =================== Efetua a coleta de logs via paramiko START ===========================
                    jump_server_connection.send_command_timing('en', strip_prompt=False,
                                                               strip_command=True)

                    log = 'show ip interface brief ' + \
                          jump_server_connection.send_command_timing('show ip interface brief', strip_prompt=False,
                                                                     strip_command=True)
                    log += 'show ip bgp vpnv4 all summary ' + \
                           jump_server_connection.send_command_timing('show ip bgp vpnv4 all summary',
                                                                      strip_prompt=False, strip_command=True)
                    # coletando vrf_list, peers e atualizando NE_metro
                    try:
                        vrf_list, peers = first_file_analysis_tellabs(ne_name, uf, vendor, ne_init, ip, now, log,
                                                                      connection, ne_metro_file_path, df_version,
                                                                      real_vendor, gesa)
                        ne_metro_updated = True
                    except Exception as e:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                        print(f'Error on process TELLABS log!')
                        raise e
                    log += 'show ip interface description ' + jump_server_connection.send_command_timing(
                        'show ip interface description', strip_prompt=False, strip_command=True)
                    log += 'show arp ' + jump_server_connection.send_command_timing('show arp', strip_prompt=False,
                                                                                    strip_command=True)
                    for cur_vrf in vrf_list:
                        log += f'show arp vrf {cur_vrf} ' + jump_server_connection.send_command_timing(
                            f'show arp vrf {cur_vrf}', strip_prompt=False, strip_command=True)
                    if not '-gwc-' in ne_name or not '-hl4-' in ne_name:
                        for cur_peer in peers:
                            log += f'show ip route {cur_peer} ' + jump_server_connection.send_command_timing(
                                f'show ip route {cur_peer}', strip_prompt=False, strip_command=False)
                    log += 'show interface ' + jump_server_connection.send_command_timing('show interface',
                                                                                          strip_prompt=False,
                                                                                          strip_command=True)
                    log += 'show running-config | begin Interfaces ' + jump_server_connection.send_command_timing(
                        'show running-config | begin Interfaces', strip_prompt=False, strip_command=True)

                    # =================== Efetua a coleta de logs via paramiko END ===========================

                    # salvando logs no txt
                    file_name = f'{ne_name}.txt'
                    file_path = os.path.abspath(os.path.join(directory_path, file_name))
                    with open(file_path, "w") as text_file:
                        text_file.write(log)

                    # saindo do equipamento
                    jump_server_connection.send_command('quit', expect_string=r'(:|\$)')
                    redispatch(jump_server_connection, device_type='linux')
                    # atualiza bd
                    cursor.execute(success_query)
                except Exception as e:
                    if ne_metro_updated:
                        ne_metro_log_update(ne_name, ne_metro_file_path)
                    else:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                    raise e
                '''

                jump_server_connection.send_command('quit', expect_string=r'\)\?|:|\$|#|\>')
                # atualiza BD para não coletar novamente
                query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
                        f" CONNECTED_VIA = '{connection}', OM_LINK = 'True', COLLECTED = 'Not_working' " \
                        f"WHERE NE_NAME = '{ne_name}'"
                cursor.execute(query)
                print(f"Finished: {ne_name}")


            elif real_vendor == 'NOKIA' or real_vendor == 'ALCATEL':
                ne_metro_updated = False
                try:
                    # =================== Efetua a coleta de logs via paramiko START ===========================

                    redispatch(jump_server_connection, device_type=redispatch_dict[f'{real_vendor}_{connection}'])
                    log = jump_server_connection.send_command_timing('show version', strip_command=False,
                                                                     strip_prompt=False)
                    log += jump_server_connection.send_command_timing('show port', strip_command=False,
                                                                      strip_prompt=False)
                    log += jump_server_connection.send_command_timing('admin display-config', strip_command=False,
                                                                      strip_prompt=False)
                    try:
                        vrf_list, peers = first_file_analysis_nokia(ne_name, uf, vendor, ne_init, ip, now, log,
                                                                    connection, ne_metro_file_path, df_version,
                                                                    real_vendor, gesa)
                        ne_metro_updated = True
                    except Exception as e:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                        print(f'Error on process TELLABS log!')
                        raise e

                    log += jump_server_connection.send_command_timing('admin display-config', strip_command=False,
                                                                      strip_prompt=False)

                    # salvando logs no txt
                    file_name = f'{ne_name}.txt'
                    file_path = os.path.abspath(os.path.join(directory_path, file_name))
                    with open(file_path, "w") as text_file:
                        text_file.write(log)

                    # saindo do equipamento
                    jump_server_connection.send_command('logout', expect_string=r'(:|\$)')
                    # print(logout)
                    redispatch(jump_server_connection, device_type='linux')
                    # atualiza bd
                    cursor.execute(success_query)

                    # =================== Efetua a coleta de logs via paramiko END ===========================

                    jump_server_connection.send_command('quit', expect_string=r'\)\?|:|\$|#|\>')
                    # atualiza BD para não coletar novamente
                    query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
                            f" CONNECTED_VIA = '{connection}', OM_LINK = 'True', COLLECTED = 'Not_working' " \
                            f"WHERE NE_NAME = '{ne_name}'"
                    cursor.execute(query)
                    print(f"Finished: {ne_name}")

                except Exception as e:
                    if ne_metro_updated:
                        ne_metro_log_update(ne_name, ne_metro_file_path)
                    else:
                        ne_metro_log(ne_name, uf, vendor, ne_init, ip, now, connection, ne_metro_file_path, df_version,
                                     real_vendor, gesa)
                    raise e

            # caso for cisco Fusion!
            # elif real_vendor == 'CISCO' and 'i-br-' in ne_name:
            #     jump_server_connection.send_command('quit', expect_string=r'\)\?|:|\$|#|\>')
            #     # atualiza BD para não coletar novamente
            #     query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
            #             f" CONNECTED_VIA = '{connection}', OM_LINK = 'True', COLLECTED = 'Not_mapped' " \
            #             f"WHERE NE_NAME = '{ne_name}'"
            #     cursor.execute(query)
            #     print(f"Finished: {ne_name}")
            #     continue

            elif real_vendor == 'TELLABS' and connection == 'TELNET':
                jump_server_connection.send_command('quit', expect_string=r'\)\?|:|\$|#|\>')
                # atualiza BD para não coletar novamente
                query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
                        f" CONNECTED_VIA = '{connection}', OM_LINK = 'True', COLLECTED = 'Not_working' " \
                        f"WHERE NE_NAME = '{ne_name}'"
                cursor.execute(query)
                print(f"Finished: {ne_name}")
                continue

            else:
                jump_server_connection.send_command('quit', expect_string=r'\)\?|:|\$|#|\>')
                # atualiza BD para não coletar novamente
                query = f"UPDATE AUTOMATION_PROGRESS SET COL_ATTEMPT = {gesa},  GESA = 'GESA_{gesa}'," \
                        f" CONNECTED_VIA = '{connection}', OM_LINK = 'True', COLLECTED = 'False' WHERE NE_NAME = '{ne_name}'"
                cursor.execute(query)
                print(f"Finished: {ne_name} not mapped vendor!")
                continue

            # finaliza coleta do elemento e mostra quanto tempo demorou!
            final = datetime.now()
            delta = final - start
            delta = str(delta).split('.')[0]
            print(f"Finished: {ne_name} in {delta}")

        except Exception as e:
            print(f'\nFail processing:  {ne_name}\nCause: {e}')
            # desconectando para resetar o prompt
            jump_server_connection.disconnect()
            connected = False
            cursor.execute(fail_query)
            traceback.print_tb(e.__traceback__)

    # finalizando tentativa
    jump_server_connection.disconnect()


def get_command_dict(ne_name, vendor):
    """
            :param ne_name: str =>
            :param vendor: obj =>
    """
    # comandos para rede fusion
    if 'i-br' in ne_name:
        if vendor == 'CISCO':
            command_dict = {
                'show version': 'show version',
                'show interface transceiver detail': 'show interface transceiver detail',
                'show ip interface brief': 'show ip interface brief',
                'show running-config | include vrf': '',
                'show ip bgp vpnv4 all summary': 'show ip bgp vpnv4 all summary',
                'show arp': 'show arp',
                'show arp vrf': 'show arp vrf',
                'show ip route peer': 'show ip route',
                'show running-config interface': 'show running-config interface',
                'show ip route': 'show ip route',
                'show ip route vrf': 'show ip route vrf',
                'Show ptp platform servo': 'Show ptp platform servo'
            }
            return command_dict
    # comandos para rede legada
    else:
        if vendor == 'CISCO':
            command_dict = {
                'show version': 'show version',
                'show interface transceiver detail': 'show interface transceiver detail',
                'show ip interface brief': 'show ip interface brief',
                'show running-config | include vrf': '',
                'show ip bgp vpnv4 all summary': 'show ip bgp vpnv4 all summary',
                'show arp': 'show arp',
                'show arp vrf': 'show arp vrf',
                'show ip route peer': 'show ip route',
                'show running-config interface': 'show running-config interface',
                'show ip route': 'show ip route',
                'show ip route vrf': 'show ip route vrf',
                'Show ptp platform servo': 'Show ptp platform servo'
            }
            return command_dict
