import pandas as pd
import os
from datetime import datetime
from ParserAux import *
def license_parse_init(log_output,ne_name):

    # define pasta do csv
    basepath = os.path.dirname(__file__)
    media_path = os.path.abspath(os.path.join(basepath, "../media"))
    csv_path = os.path.abspath(os.path.join(media_path, "csv"))

    lines = log_output.split('\n')

    init = [i for i, s in enumerate(lines) if 'Item name ' in s][0] + 3
    lines = lines[init:-2]

    filtered_lines = [segments.split()[:4] for segments in lines]
    license_table_df = pd.DataFrame(filtered_lines, columns=['Item_name', 'Item_type', 'Value', 'Description'])
    # >>>> INSERE DATA NO DATAFRAME
    now = datetime.now().date().strftime("%Y-%m-%d")
    license_table_df.insert(0, 'NE_NAME', ne_name)
    license_table_df = license_table_df.assign(DATE=now)

    # 'NE_NAME', 'Item_name', 'Item_type', 'Item_type', 'Description', 'DATE'

    current_csv_path = os.path.abspath(os.path.join(csv_path, 'license_table.csv'))
    license_table_df.to_csv(current_csv_path, mode='a', header=False, index=False)


def interface_parse(log_output,ne_name):

    # define pasta do csv
    basepath = os.path.dirname(__file__)
    media_path = os.path.abspath(os.path.join(basepath, "../media"))
    csv_path = os.path.abspath(os.path.join(media_path, "csv"))

    try:
        lines = log_output.split('\n')
        #encontra linha de inicio
        line_init = [i for i, s in enumerate(lines) if 'Protocol' in s][0]+1
        lines = lines[line_init:-1]

        # empty lists
        df_list = []

        for i, item in enumerate(lines):
            inner_line = item.split()
            #checando se a descrição foi splitada
            if len(inner_line)>4:
                description = ''
                for i, part in enumerate(inner_line):
                    if i > 2:
                        description += part + ' '
                del inner_line[3:]
                inner_line.append(description.strip())
            if len(inner_line) < 4:
                inner_line.append(None)

            if '(' in inner_line[0]:
                port_speed = inner_line[0].split('(')[-1]
                port_speed = port_speed.split(')')[0]
                inner_line.insert(1,port_speed)
            else:
                inner_line.insert(1, None)


            df_list.append(inner_line)


        license_table_df = pd.DataFrame(df_list, columns=['Interface','Port_speed','PHY', 'Protocol', 'Description'])

        # >>>> INSERE DATA NO DATAFRAME
        now = datetime.now().date().strftime("%Y-%m-%d")
        license_table_df.insert(0, 'NE_NAME', ne_name)
        license_table_df = license_table_df.assign(DATE=now)

        current_csv_path = os.path.abspath(os.path.join(csv_path, 'interface_speed_table.csv'))
        license_table_df.to_csv(current_csv_path, mode='a', header=False, index=False)
    except:
        raise Exception("Error Handling license Parse!")




def display_interface_parse(log_output,ne_name):

    # define pasta do csv
    basepath = os.path.dirname(__file__)
    media_path = os.path.abspath(os.path.join(basepath, "../media"))
    csv_path = os.path.abspath(os.path.join(media_path, "csv"))
    lines_analysis = log_output.split('\n')
    now = datetime.now().date().strftime("%Y-%m-%d")

    try:

        # +=============================   NE_METRO_INT PROCESS   ==================================+

        init_interfaces = [i for i, s in enumerate(lines_analysis) if 'display interface brief' in s][0] + 1
        lines_interfaces = lines_analysis[init_interfaces:]
        end_interfaces = [i for i, s in enumerate(lines_interfaces) if 'display' in s][0]
        lines_interfaces = lines_interfaces[:end_interfaces]
        indices = [i for i, s in enumerate(lines_interfaces) if 'nterface' in s]
        header = lines_interfaces[indices[0]]
        columns = header.split()
        quantity = len(columns)
        del lines_interfaces[0:(indices[0] + 1)]
        df_lines_interfaces = [segments.split()[:quantity] for segments in lines_interfaces]
        df_interfaces = pd.DataFrame(df_lines_interfaces, columns=columns)
        df_interfaces.dropna(subset=[columns[1]], inplace=True)
        interfaces = df_interfaces['Interface'].to_list()
        end_triggers = [s.split('(')[0] + ' current state' for s in interfaces]
        # if '-gwc-' in ne_name:
        #     end_triggers.append('>\n')
        # else:
        end_triggers.append('>display')
        interface_qos_check = []
        end_triggers_qos = ['interface ' + s.split('(')[0] + '\n' for s in interfaces]
        init_all_interfaces = [i for i, s in enumerate(lines_analysis) if 'display interface' in s][1] + 1
        lines_all_interfaces = lines_analysis[init_all_interfaces:]
        end_all_interfaces = [i for i, s in enumerate(lines_all_interfaces) if 'display' in s][0] + 1
        lines_all_interfaces = lines_all_interfaces[:end_all_interfaces]

        interface_triggers = [i for i, s in enumerate(lines_all_interfaces) if any(end in s for end in end_triggers)]

        interfaces_csv_df = pd.DataFrame(
            columns=['Interface','Port_speed','PHY', 'Protocol', 'Description'])

        for i, interface in enumerate(interfaces):
            if i == 1505:
                pause = 10
            if i < len(interfaces):
                phy_status = df_interfaces[df_interfaces['Interface'] == interface]['PHY'].iloc[0]
                log_status = df_interfaces[df_interfaces['Interface'] == interface]['Protocol'].iloc[0]
                if '(' in interface:
                    interface = interface.split('(')[0]
                # init_interface = [i for i, s in enumerate(lines_all_interfaces) if f'{interface} current state' in s][0]
                lines_interface = lines_all_interfaces[interface_triggers[i]:interface_triggers[i + 1]]
                # end_interface = [i for i, s in enumerate(lines_interface) if any(end in s for end in end_triggers)][1]
                # lines_interface = lines_interface[:interface_triggers[i+1]]
                response = '\n'.join(lines_interface)

                description = get_int_desc_huawei(response)
                ne_name_frag = ne_name.split('-')[-3].upper()
                if any(x for x in
                       [f'T{ne_name_frag}', f'M{ne_name_frag}', f'W{ne_name_frag}', 'M-BR', 'M_BR', 'I-BR', 'I_BR'] if
                       x in description.upper()):
                    interface_qos_check.append(interface)
                mac_a = get_int_mac_huawei(response)
                mtu = get_int_mtu_huawei(response)
                duplex = get_duplex_info(response)
                ip_address = get_int_ip_add(response)
                if 'NUL' in interface.upper():
                    ne_type = 'N/A'
                else:
                    ne_type = get_int_type_huawei(response)
                    if (ne_type == 'Unknown') and (rx_power != 'NULL'):
                        ne_type = 'Optical'
                    elif (ne_type == 'Unknown') and (rx_power == 'NULL') and (tx_power == 'NULL'):
                        ne_type = 'Empty'
                if any(x for x in ['N/A', 'Logical'] if ne_type == x):
                    speed = 'N/A'
                else:
                    speed = get_int_speed_huawei(response)
                if any(x for x in ['N/A', 'Logical'] if ne_type == x):
                    negotiation = 'N/A'
                else:
                    negotiation = get_int_negotiation_huawei(response)
                    if not negotiation:
                        if speed != 'N/A':
                            try:
                                negotiation_speed = speed.upper().replace('G', '000')
                                negotiation = f'Speed {negotiation_speed}'
                            except:
                                pass

                interface_dict = {'NE_NAME': ne_name,
                                  'INTERFACE': interface,
                                  'IP': ip_address,
                                  'PHY_STATUS': phy_status,
                                  'LOG_STATUS': log_status,
                                  'DESCRIPTION': description,
                                  'MTU': mtu,
                                  'MAC_A': mac_a,
                                  'TYPE': ne_type,
                                  'DUPLEX': duplex,
                                  'SPEED': speed,
                                  'NEGOTIATION': negotiation,
                                  'DATE': now}

                interfaces_csv_df = pd.concat([interfaces_csv_df, pd.DataFrame(interface_dict, index=[0])],
                                              ignore_index=True)

    except:
        raise Exception("Error Handling license Parse!")