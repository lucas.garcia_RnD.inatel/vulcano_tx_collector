import os
import shutil

if __name__ == '__main__':

    #define diretorios de trabalho
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "../media"))
    filter_path = os.path.abspath(os.path.join(basepath, "../filter"))

    #define arquivo com lista de sites para copiar
    filter_file = os.path.abspath(os.path.join(filter_path, 'sites_filter.txt'))
    first_file = open(f'{filter_file}', 'r')
    sites_to_filter = first_file.readlines()
    first_file.close()

    #retira o /n das linhas
    sites_to_filter = [s.strip() for s in sites_to_filter]

    #loop para copiar todos os sites listados no arquivo
    for site in sites_to_filter:
        try:
            src = os.path.abspath(os.path.join(directory_path, f'{site}.txt'))
            dst = os.path.abspath(os.path.join(filter_path, f'{site}.txt'))
            shutil.copy(src, dst)
        except:
            pass
