import mysql.connector as mysql
import os
from datetime import datetime
import pandas as pd
from sqlalchemy import create_engine

from utils.csv_parser import get_last_destination_ip
from utils.general import get_last_dated_df, get_site_name, remove_parentheses_string, remove_strokes_string, \
    validate_link, get_media_tx
from utils.telegram_bot import telegram_send_message

def restore_auto_progess(passwords,csv_name):
    print('============>IMPORTING NE_METRO STARTED<============')

    now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv', 'backup'))
    bkp_file = os.path.abspath(os.path.join(csv_path, csv_name))

    if os.path.exists(bkp_file):
        upload_bkp_file = bkp_file.replace('\\', '/')
        db = mysql.connect(
            host="10.26.82.160",
            user=passwords['sql_user'],
            passwd=passwords['sql_password'],
            database=passwords['sql_schema'],
            allow_local_infile=True
        )

        cursor = db.cursor()

        sql = f'LOAD DATA LOCAL INFILE "{upload_bkp_file}" REPLACE INTO TABLE NE_METRO\n' \
            f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
            f'LINES TERMINATED BY \'\r\n\';'

        cursor.execute(sql)
        db.commit()


def ne_metro_csv_upload(passwords):
    print('============>IMPORTING NE_METRO STARTED<============')

    now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    ne_metro_file = os.path.abspath(os.path.join(csv_path, 'ne_metro_table.csv'))
    if os.path.exists(ne_metro_file):
        ne_metro_done_file = os.path.abspath(os.path.join(csv_path, 'done', f'ne_metro_table_{now}.csv'))
        upload_ne_metro_file = ne_metro_file.replace('\\', '/')

        db = mysql.connect(
            host="10.26.82.160",
            user=passwords['sql_user'],
            passwd=passwords['sql_password'],
            database=passwords['sql_schema'],
            allow_local_infile=True
        )

        cursor = db.cursor()

        sql = f'LOAD DATA LOCAL INFILE "{upload_ne_metro_file}" REPLACE INTO TABLE NE_METRO\n' \
            f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
            f'LINES TERMINATED BY \'\r\n\';'

        cursor.execute(sql)
        db.commit()

        sql = f'DELETE FROM NE_METRO WHERE DATE < DATE_SUB(NOW(), INTERVAL 3 MONTH);'
        cursor.execute(sql)
        db.commit()

        os.rename(ne_metro_file, ne_metro_done_file)

    print('============>IMPORTING NE_METRO FINISHED<============')


def interface_csv_upload(passwords):
    print('============>IMPORTING NE_METRO_INT STARTED<============')
    telegram_send_message('============>IMPORTING NE_METRO_INT STARTED<============')

    now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    ne_interface_file = os.path.abspath(os.path.join(csv_path, 'interface_table.csv'))
    backup_path = os.path.abspath(os.path.join(csv_path, 'backup', 'interface_table.csv'))

    if os.path.exists(ne_interface_file):
        ne_interface_done_file = os.path.abspath(os.path.join(csv_path, 'done', f'interface_table_{now}.csv'))
        upload_ne_interface_file = ne_interface_file.replace('\\', '/')

        df = pd.read_csv(ne_interface_file, header=None)

        try:
            os.rename(ne_interface_file, backup_path)

            columns = ['NE_NAME', 'INTERFACE', 'IP', 'TUNNEL_ID', 'PEER_TUNNEL_IP', 'PHY_STATUS', 'LOG_STATUS',
                       'DESCRIPTION', 'RX_UTI', 'TX_UTI', 'RX_ERROR', 'TX_ERROR', 'RX_POWER', 'TX_POWER', 'RX_SENSE_HI',
                       'RX_SENSE_LO', 'TX_SENSE_HI', 'TX_SENSE_LO', 'QOS', 'MTU', 'MAC_A', 'TYPE', 'DUPLEX', 'SPEED',
                       'NEGOTIATION', 'VRF','NE_PTP_STATUS','INT_PTP_CONFIG', 'DATE']

            df.columns = columns

            df.drop_duplicates(inplace=True)

            df['NE_NAME'].fillna('NULL-X', inplace=True)
            df['INTERFACE'].fillna('NULL-X', inplace=True)
            df['DATE'].fillna(now, inplace=True)
            df['RX_ERROR'] = df['RX_ERROR'].fillna(0)
            df['RX_ERROR'] = df['RX_ERROR'].astype('int')
            df['TX_ERROR'] = df['TX_ERROR'].fillna(0)
            df['TX_ERROR'] = df['TX_ERROR'].astype('int')
            df['MTU'] = df['MTU'].fillna(0)
            df['MTU'] = df['MTU'].astype('int')
            df['RX_POWER'] = df['RX_POWER'].astype('float')
            df['RX_POWER'] = df['RX_POWER'].fillna('NULL')
            df['TX_POWER'] = df['TX_POWER'].astype('float')
            df['TX_POWER'] = df['TX_POWER'].fillna('NULL')
            df['RX_SENSE_HI'] = df['RX_SENSE_HI'].astype('float')
            df['RX_SENSE_HI'] = df['RX_SENSE_HI'].fillna('NULL')
            df['RX_SENSE_LO'] = df['RX_SENSE_LO'].astype('float')
            df['RX_SENSE_LO'] = df['RX_SENSE_LO'].fillna('NULL')
            df['TX_SENSE_HI'] = df['TX_SENSE_HI'].astype('float')
            df['TX_SENSE_HI'] = df['TX_SENSE_HI'].fillna('NULL')
            df['TX_SENSE_LO'] = df['TX_SENSE_LO'].astype('float')
            df['TX_SENSE_LO'] = df['TX_SENSE_LO'].fillna('NULL')

            df.to_csv(ne_interface_file, mode='a', header=False, index=False, float_format='%g')

            db = mysql.connect(
                host="10.26.82.160",
                user=passwords['sql_user'],
                passwd=passwords['sql_password'],
                database=passwords['sql_schema'],
                allow_local_infile=True
            )

            cursor = db.cursor()

            sql = f'LOAD DATA LOCAL INFILE "{upload_ne_interface_file}" REPLACE INTO TABLE NE_METRO_INT\n' \
                f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                f'LINES TERMINATED BY \'\r\n\';'

            cursor.execute(sql)
            db.commit()

            sql = f'DELETE FROM NE_METRO_INT WHERE DATE < DATE_SUB(NOW(), INTERVAL 3 MONTH);'
            cursor.execute(sql)
            db.commit()

            os.rename(ne_interface_file, ne_interface_done_file)
            os.remove(backup_path)
        except:
            telegram_send_message('Error while importing NE_METRO_INT file! Please check interface.csv on backup folder')
            print('Error while importing NE_METRO_INT file! Please check interface.csv on backup folder')

    print('============>IMPORTING NE_METRO_INT FINISHED<============')
    telegram_send_message('============>IMPORTING NE_METRO_INT FINISHED<============')


def mac_csv_upload(passwords):
    print('============>IMPORTING NE_METRO_MAC STARTED<============')
    telegram_send_message('============>IMPORTING NE_METRO_MAC STARTED<============')

    now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    ne_mac_file = os.path.abspath(os.path.join(csv_path, 'mac_table.csv'))
    backup_path = os.path.abspath(os.path.join(csv_path, 'backup', 'mac_table.csv'))

    if os.path.exists(ne_mac_file):
        ne_mac_done_file = os.path.abspath(os.path.join(csv_path, 'done', f'mac_table_{now}.csv'))
        upload_ne_mac_file = ne_mac_file.replace('\\', '/')

        df = pd.read_csv(ne_mac_file, header=None)

        try:
            os.rename(ne_mac_file, backup_path)

            columns = ['NE_NAME', 'INTERFACE', 'IP_A', 'MAC_A', 'IP_B', 'MAC_B', 'VRF', 'DATE']

            df.columns = columns

            df.drop_duplicates(inplace=True)

            df['NE_NAME'].fillna('NULL-X', inplace=True)
            df['INTERFACE'].fillna('NULL-X', inplace=True)
            df['DATE'].fillna(now, inplace=True)

            df.to_csv(ne_mac_file, mode='a', header=False, index=False)

            db = mysql.connect(
                host="10.26.82.160",
                user=passwords['sql_user'],
                passwd=passwords['sql_password'],
                database=passwords['sql_schema'],
                allow_local_infile=True
            )

            cursor = db.cursor()

            sql = f'LOAD DATA LOCAL INFILE "{upload_ne_mac_file}" REPLACE INTO TABLE NE_METRO_MAC\n' \
                f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                f'LINES TERMINATED BY \'\r\n\';'

            cursor.execute(sql)
            db.commit()

            sql = f'DELETE FROM NE_METRO_MAC WHERE DATE < DATE_SUB(NOW(), INTERVAL 3 MONTH);'
            cursor.execute(sql)
            db.commit()

            os.rename(ne_mac_file, ne_mac_done_file)
            os.remove(backup_path)
        except:
            print('Error while importing NE_METRO_MAC file! Please check mac_table.csv on backup folder')

    print('============>IMPORTING NE_METRO_MAC FINISHED<============')
    telegram_send_message('============>IMPORTING NE_METRO_MAC FINISHED<============')


def routing_table_csv_upload(passwords):
    print('============>IMPORTING NE_METRO_ROUTING STARTED<============')
    telegram_send_message('============>IMPORTING NE_METRO_ROUTING STARTED<============')

    now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    ne_routing_table_file = os.path.abspath(os.path.join(csv_path, 'routing_table_table.csv'))
    backup_path = os.path.abspath(os.path.join(csv_path, 'backup', 'routing_table_table.csv'))

    if os.path.exists(ne_routing_table_file):
        ne_routing_table_done_file = os.path.abspath(os.path.join(csv_path, 'done', f'routing_table_table_{now}.csv'))
        upload_ne_routing_table_file = ne_routing_table_file.replace('\\', '/')

        df = pd.read_csv(ne_routing_table_file, header=None)

        try:
            os.rename(ne_routing_table_file, backup_path)

            columns = ['NE_NAME', 'INTERFACE', 'IP_LOCAL', 'MAC_LOCAL', 'IP_RECEIVED', 'MAC_RECEIVED',
                       'NE_NAME_NEXT_HOP', 'INTERFACE_NEXT_HOP', 'IP_NEXT_HOP', 'MAC_NEXT_HOP', 'COST', 'PEER',
                       'PREFERENTIAL', 'VRF', 'DATE']

            df.columns = columns

            df.drop_duplicates(inplace=True)

            df['NE_NAME'].fillna('NULL-X', inplace=True)
            df['INTERFACE'].fillna('NULL-X', inplace=True)
            df['PEER'].fillna('NULL-X', inplace=True)
            df['DATE'].fillna(now, inplace=True)

            df.to_csv(ne_routing_table_file, mode='a', header=False, index=False)

            db = mysql.connect(
                host="10.26.82.160",
                user=passwords['sql_user'],
                passwd=passwords['sql_password'],
                database=passwords['sql_schema'],
                allow_local_infile=True
            )

            cursor = db.cursor()

            sql = f'LOAD DATA LOCAL INFILE "{upload_ne_routing_table_file}" REPLACE INTO TABLE NE_METRO_ROUTING\n' \
                f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                f'LINES TERMINATED BY \'\r\n\';'

            cursor.execute(sql)
            db.commit()

            sql = f'DELETE FROM NE_METRO_ROUTING WHERE DATE < DATE_SUB(NOW(), INTERVAL 3 MONTH);'
            cursor.execute(sql)
            db.commit()

            os.rename(ne_routing_table_file, ne_routing_table_done_file)
            os.remove(backup_path)
        except:
            print('Error while importing NE_METRO_ROUTING file! Please check routing_table_table.csv on backup folder')

    print('============>IMPORTING NE_METRO_ROUTING FINISHED<============')
    telegram_send_message('============>IMPORTING NE_METRO_ROUTING FINISHED<============')


def full_routing_table_csv_upload(passwords):
    print('============>IMPORTING NE_METRO_FULL_ROUTING STARTED<============')
    telegram_send_message('============>IMPORTING NE_METRO_FULL_ROUTING STARTED<============')

    now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    ne_routing_table_file = os.path.abspath(os.path.join(csv_path, 'full_routing_table_table.csv'))
    backup_path = os.path.abspath(os.path.join(csv_path, 'backup', 'full_routing_table_table.csv'))

    if os.path.exists(ne_routing_table_file):
        ne_routing_table_done_file = os.path.abspath(os.path.join(csv_path, 'done', f'full_routing_table_table_{now}.csv'))
        upload_ne_routing_table_file = ne_routing_table_file.replace('\\', '/')

        df = pd.read_csv(ne_routing_table_file, header=None)

        try:
            os.rename(ne_routing_table_file, backup_path)

            columns = ['NE_NAME', 'INTERFACE', 'DESTINATION', 'MASK', 'NEXT_HOP', 'PROTOCOL', 'VRF', 'PRE', 'COST',
                       'FLAGS', 'DATE']

            df.columns = columns

            df.drop_duplicates(inplace=True)

            df['COST'] = df['COST'].fillna(0)
            df['COST'] = df['COST'].astype('int')
            df['PRE'] = df['PRE'].fillna(0)
            df['PRE'] = df['PRE'].astype('int')

            df.to_csv(ne_routing_table_file, mode='a', header=False, index=False)

            db = mysql.connect(
                host="10.26.82.160",
                user=passwords['sql_user'],
                passwd=passwords['sql_password'],
                database=passwords['sql_schema'],
                allow_local_infile=True
            )

            cursor = db.cursor()

            exclude = 'truncate NE_METRO_FULL_ROUTING'
            cursor.execute(exclude)

            sql = f'LOAD DATA LOCAL INFILE "{upload_ne_routing_table_file}" REPLACE INTO TABLE NE_METRO_FULL_ROUTING\n' \
                f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                f'LINES TERMINATED BY \'\r\n\';'

            cursor.execute(sql)
            db.commit()

            os.rename(ne_routing_table_file, ne_routing_table_done_file)
            os.remove(backup_path)
        except:
            telegram_send_message()
            print('Error while importing NE_METRO_FULL_ROUTING file! Please check full_routing_table_table.csv on'
                  ' backup folder')

    print('============>IMPORTING NE_METRO_FULL_ROUTING FINISHED<============')
    telegram_send_message('============>IMPORTING NE_METRO_FULL_ROUTING FINISHED<============')


def simple_chart_table_csv_upload(passwords):
    print('============>IMPORTING SIMPLE_ROUTING_CHART STARTED<============')
    telegram_send_message('============>IMPORTING SIMPLE_ROUTING_CHART STARTED<============')

    start = datetime.now()

    # GET BASES
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/' \
                        f'{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)
    df_routing = pd.read_sql('SELECT * FROM NE_METRO_ROUTING', con=db_connection)

    # DF STRUCTURING
    df_routing.sort_values(by='DATE', ascending=False, inplace=True)
    df_routing.drop_duplicates(
        subset=['NE_NAME', 'INTERFACE', 'NE_NAME_NEXT_HOP', 'INTERFACE_NEXT_HOP', 'PEER', 'PREFERENTIAL'], inplace=True)
    df_routing = df_routing[['NE_NAME', 'INTERFACE', 'NE_NAME_NEXT_HOP', 'INTERFACE_NEXT_HOP', 'PEER', 'PREFERENTIAL',
                             'DATE']]

    # INSERT TABLE ON DB

    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    simple_routing_table_file = os.path.abspath(os.path.join(csv_path, 'simple_routing_import.csv'))

    try:
        os.remove(simple_routing_table_file)
    except:
        pass

    df_routing.to_csv(simple_routing_table_file, header=False, index=False)

    csv_path = os.path.abspath(simple_routing_table_file)
    csv_path = csv_path.replace('\\', '/')

    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=passwords['sql_schema'],
        allow_local_infile=True
    )

    cursor = db.cursor()

    exclude = 'truncate SIMPLE_ROUTING_CHART'
    cursor.execute(exclude)

    sql = f'LOAD DATA LOCAL INFILE "{csv_path}" INTO TABLE SIMPLE_ROUTING_CHART\n' \
        f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
        f'LINES TERMINATED BY \'\r\n\';'

    cursor.execute(sql)

    db.commit()

    try:
        os.remove(simple_routing_table_file)
    except:
        pass

    end = datetime.now()
    delta = end - start

    print(f'>>>>TIME FOR IMPORTING = {str(delta).split(".")[0]}<<<<')

    print('============>IMPORTING SIMPLE_ROUTING_CHART FINISHED<============')
    telegram_send_message('============>IMPORTING SIMPLE_ROUTING_CHART FINISHED<============')


def full_chart_table_csv_upload(passwords):
    print('============>IMPORTING FULL_ROUTING_CHART STARTED<============')
    telegram_send_message('============>IMPORTING FULL_ROUTING_CHART STARTED<============')

    start = datetime.now()

    # GET BASES
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)
    df_routing = pd.read_sql('SELECT * FROM NE_METRO_FULL_ROUTING', con=db_connection)
    df_int = pd.read_sql('SELECT * FROM NE_METRO_INT', con=db_connection)

    # DF STRUCTURING
    df_int.sort_values(by='DATE', ascending=False, inplace=True)
    df_int.drop_duplicates(subset=['NE_NAME', 'INTERFACE'], inplace=True)
    df_int.dropna(subset=['IP'], inplace=True)
    df_int = df_int[df_int['IP'] != 'unassigned']
    df_int = df_int[['NE_NAME', 'IP', 'INTERFACE']]
    df_int.columns = ['NE_NAME_NEXT_HOP', 'NEXT_HOP', 'INTERFACE_NEXT_HOP']
    df_int.drop_duplicates(subset='NEXT_HOP', inplace=True)
    df_routing = df_routing.merge(df_int, on='NEXT_HOP')
    df_routing.dropna(subset=['DESTINATION'], inplace=True)
    df_routing = df_routing[df_routing['DESTINATION'].str.contains('.')]
    df_routing.drop_duplicates(subset=['NE_NAME', 'DESTINATION', 'NE_NAME_NEXT_HOP'], inplace=True)
    df_routing = df_routing[
        ['NE_NAME', 'DESTINATION', 'MASK', 'INTERFACE', 'PROTOCOL', 'VRF', 'NE_NAME_NEXT_HOP', 'INTERFACE_NEXT_HOP']]
    df_routing['MASK'].replace({0: 32}, inplace=True)
    df_routing.fillna({'MASK': 32, 'INTERFACE': 'NO_INTERFACE'}, inplace=True)
    df_routing["MASK"] = pd.to_numeric(df_routing["MASK"])
    df_routing.fillna({'MASK': 32, 'INTERFACE': 'NO_INTERFACE'}, inplace=True)
    df_routing['LAST_IP_DESTINATION'] = df_routing.apply(lambda row: get_last_destination_ip(row), axis=1)
    df_routing.reset_index(inplace=True)
    df_routing = df_routing[
        ['NE_NAME', 'INTERFACE', 'NE_NAME_NEXT_HOP', 'INTERFACE_NEXT_HOP', 'DESTINATION', 'MASK', 'LAST_IP_DESTINATION',
         'PROTOCOL', 'VRF']]

    # INSERT TABLE ON DB

    df_routing['MASK'] = df_routing['MASK'].astype(int)

    try:
        os.remove('media\\csv\\routing_full_import.csv')
    except:
        pass

    df_routing.to_csv('media\\csv\\routing_full_import.csv', header=False, index=False)

    csv_path = os.path.abspath('media\\csv\\routing_full_import.csv')
    csv_path = csv_path.replace('\\', '/')

    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=passwords['sql_schema'],
        allow_local_infile=True
    )

    cursor = db.cursor()

    exclude = 'truncate FULL_ROUTING_CHART'
    cursor.execute(exclude)

    sql = f'LOAD DATA LOCAL INFILE "{csv_path}" INTO TABLE FULL_ROUTING_CHART\n' \
        f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
        f'LINES TERMINATED BY \'\r\n\';'

    cursor.execute(sql)

    sql = f"UPDATE FULL_ROUTING_CHART SET INTERFACE = 'NO_INTERFACE' WHERE INTERFACE = ''"
    cursor.execute(sql)

    db.commit()

    try:
        os.remove('media\\csv\\routing_full_import.csv')
    except:
        pass

    end = datetime.now()
    delta = end - start

    print(f'>>>>TIME FOR IMPORTING = {str(delta).split(".")[0]}<<<<')

    print('============>IMPORTING FULL_ROUTING_CHART FINISHED<============')
    telegram_send_message('============>IMPORTING FULL_ROUTING_CHART FINISHED<============')


def metro_steps_table_csv_upload(passwords):
    print('============>IMPORTING METRO_STEPS STARTED<============')
    telegram_send_message('============>IMPORTING METRO_STEPS STARTED<============')

    start = datetime.now()

    # GET BASES
    query = f'SELECT * FROM {passwords["sql_schema"]}.SIMPLE_ROUTING_CHART'
    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)
    df_routing = pd.read_sql(query, con=db_connection)

    ne_filter = list(set(df_routing['NE_NAME'].to_list()))

    df_routing.dropna(subset=['NE_NAME_NEXT_HOP'], inplace=True)
    df_routing = df_routing[df_routing['NE_NAME_NEXT_HOP'] != '']
    df_routing.drop_duplicates(subset=['NE_NAME', 'NE_NAME_NEXT_HOP', 'INTERFACE', 'INTERFACE_NEXT_HOP', 'PEER',
                                       'PREFERENTIAL'], inplace=True)

    df_routing = get_last_dated_df(df_routing)

    df_list = []
    for cur_source in ne_filter:
        starting_site = cur_source
        starting_site_literal = f'{starting_site.split("-")[-3]}_{starting_site.split("-")[2]}'.upper()
        starting_site = starting_site.split("-")[-3].upper()
        find_ne_names = [cur_source]
        actual_step = 1
        passed_by = []
        steps_info = []
        while find_ne_names:
            next_ne = []
            full_next_ne = []
            cur_find_ne_name_list = []
            for cur_find_ne_name in find_ne_names:
                passed_by.append(cur_find_ne_name)
                response_row = df_routing[df_routing['NE_NAME'] == cur_find_ne_name].copy()
                if not response_row.empty:
                    next_ne = list(set(response_row['NE_NAME_NEXT_HOP'].to_list()))
                    response_row = response_row[response_row['NE_NAME_NEXT_HOP'].isin(next_ne)]
                    for index, row in response_row.iterrows():
                        source_ne = row['NE_NAME']
                        source_interface = row['INTERFACE']
                        target_ne = row['NE_NAME_NEXT_HOP']
                        target_interface = row['INTERFACE_NEXT_HOP']
                        peer = row['PEER']
                        preferential = row['PREFERENTIAL']
                        cur_find_ne_name_list.append({'hop_idx': actual_step,
                                                      'link_site': starting_site,
                                                      'link_site_literal': starting_site_literal,
                                                      'start_source': cur_source,
                                                      'source_ne': source_ne,
                                                      'source_interface': source_interface,
                                                      'target_ne': target_ne,
                                                      'target_interface': target_interface,
                                                      'peer': peer,
                                                      'preferential': preferential})

                    next_ne = [x for x in next_ne if not x in passed_by]
                    full_next_ne = next_ne + full_next_ne
                else:
                    continue

            find_ne_names = full_next_ne

            actual_step += 1

            df_list = df_list + cur_find_ne_name_list

    df_hops = pd.DataFrame(df_list)

    df_hops['link_site_literal_idx'] = ''

    all_sites_literal = list(set(df_hops[df_hops['hop_idx'].astype(str) == '1']['link_site_literal'].to_list()))

    for cur_site_literal in all_sites_literal:
        df_aux = df_hops[df_hops['link_site_literal'] == cur_site_literal]
        literal_sites = list(set(df_aux[df_aux['hop_idx'].astype(str) == '1']['source_ne'].to_list()))

        if len(literal_sites) > 1:
            for index, row in df_aux.iterrows():
                df_hops.loc[index, 'link_site_literal_idx'] = f"{row['link_site_literal']}_{row['start_source'].split('-')[-1]}"
        else:
            for index, row in df_aux.iterrows():
                df_hops.loc[index, 'link_site_literal_idx'] = row['link_site_literal']

    df_hops.reset_index(inplace=True, drop=True)

    df_hops = df_hops[
        ['hop_idx', 'link_site', 'link_site_literal', 'link_site_literal_idx', 'start_source', 'source_ne',
         'source_interface', 'target_ne', 'target_interface', 'peer', 'preferential']]

    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=passwords['sql_schema'],
        allow_local_infile=True
    )

    cursor = db.cursor()

    exclude = 'truncate METRO_LINKS'
    cursor.execute(exclude)

    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    metro_steps_file = os.path.abspath(os.path.join(csv_path, 'metro_steps.csv'))

    df_hops.to_csv(metro_steps_file, mode='a', header=False, index=False)
    upload_steps_file = metro_steps_file.replace('\\', '/')

    sql = f'LOAD DATA LOCAL INFILE "{upload_steps_file}" REPLACE INTO TABLE METRO_LINKS\n' \
        f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
        f'LINES TERMINATED BY \'\r\n\';'

    cursor.execute(sql)
    db.commit()

    os.remove(metro_steps_file)

    end = datetime.now()
    delta = end - start

    print(f'>>>>TIME FOR IMPORTING = {str(delta).split(".")[0]}<<<<')

    print('============>IMPORTING METRO_STEPS FINISHED<============')
    telegram_send_message('============>IMPORTING METRO_STEPS FINISHED<============')


def radio_steps_table_csv_upload(passwords):
    print('============>IMPORTING RADIO_STEPS STARTED<============')
    telegram_send_message('============>IMPORTING RADIO_STEPS STARTED<============')

    start = datetime.now()

    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    general_path = os.path.abspath(os.path.join(directory_path, 'general'))
    file = os.path.abspath(os.path.join(general_path, 'SMTX_RJO.txt'))

    file = open(file, 'r')
    lines = file.readlines()
    lines = [[s.strip() for s in x.split(';')[:10]] for x in lines]
    df = pd.DataFrame(lines[1:], columns=lines[0])
    df = df[df['Status'].isin(['Ativada', 'Ativado'])]
    df = df[df['Tecnologia'] == 'LTE']

    df['site_name'] = df.apply(lambda row: get_site_name(row), axis=1)
    df['site_name_literal'] = df.apply(lambda row: f'{row["site_name"]}_{row["regional"]}', axis=1)

    df_hops_list = []

    all_sites = list(set(df['site_name_literal'].to_list()))

    for cur_site in all_sites:
        df_cur_site = df[df['site_name_literal'] == cur_site].copy()
        for idx, row in df_cur_site.iterrows():
            key_metro = []
            hops = row['EqptoAprox'].split(' / ')
            last_element = ''
            gradual = True if row['site_name'] in hops[0].upper() else False
            if gradual:
                source_idx = 0
                target_idx = -1
            else:
                source_idx = -1
                target_idx = 0

            total_steps = 0
            cur_hops_list = []

            for cur_hop in hops:
                source = cur_hop.split(' - ')[source_idx].strip()
                target = cur_hop.split(' - ')[target_idx].strip()

                source = remove_parentheses_string(source)
                target = remove_parentheses_string(target)

                source = remove_strokes_string(source)
                target = remove_strokes_string(target)

                if last_element:
                    if gradual:
                        if validate_link(last_element, source):
                            if '-GW' in last_element.upper():
                                key_metro.append(last_element)
                            if '-GW' in source.upper():
                                key_metro.append(source)

                            total_steps += 1
                            cur_hops_list.append(
                                {'hop_idx': 0, 'site_name': row['site_name'],
                                 'site_name_literal': row['site_name_literal'],
                                 'key_metro': '-', 'source': last_element, 'target': source})
                    else:
                        if validate_link(last_element, target):
                            if '-GW' in last_element.upper():
                                key_metro.append(last_element)
                            if '-GW' in target.upper():
                                key_metro.append(target)

                            total_steps += 1
                            cur_hops_list.append(
                                {'hop_idx': 0, 'site_name': row['site_name'],
                                 'site_name_literal': row['site_name_literal'],
                                 'key_metro': '-', 'source': target, 'target': last_element})

                if validate_link(source, target):
                    if '-GW' in source.upper():
                        key_metro.append(source)
                    if '-GW' in target.upper():
                        key_metro.append(target)

                    total_steps += 1
                    cur_hops_list.append(
                        {'hop_idx': 0, 'site_name': row['site_name'], 'site_name_literal': row['site_name_literal'],
                         'key_metro': '-', 'source': source, 'target': target})

                last_element = target if gradual else source

            if key_metro:
                key_metro = list(set(key_metro))
                key_metro = ', '.join(key_metro)
            else:
                key_metro = '-'

            if gradual:
                gradual_step = 0
                for step_info in cur_hops_list:
                    step_info['key_metro'] = key_metro
                    gradual_step += 1
                    step_info['hop_idx'] = gradual_step
            else:
                for step_info in cur_hops_list:
                    step_info['key_metro'] = key_metro
                    step_info['hop_idx'] = total_steps
                    total_steps -= 1

            df_hops_list = df_hops_list + cur_hops_list

    df_hops = pd.DataFrame(df_hops_list)

    df_hops.sort_values(['site_name_literal', 'hop_idx'], inplace=True)

    df_hops.dropna(subset=['site_name_literal', 'source', 'target'], inplace=True)

    df_hops.drop_duplicates(inplace=True)

    df_hops.reset_index(inplace=True, drop=True)

    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=passwords['sql_schema'],
        allow_local_infile=True
    )

    cursor = db.cursor()

    exclude = 'truncate RADIOS_LINKS'
    cursor.execute(exclude)

    radio_steps_file = os.path.abspath(os.path.join(csv_path, 'radio_steps.csv'))

    df_hops.to_csv(radio_steps_file, mode='a', header=False, index=False)
    upload_steps_file = radio_steps_file.replace('\\', '/')

    sql = f'LOAD DATA LOCAL INFILE "{upload_steps_file}" REPLACE INTO TABLE RADIOS_LINKS\n' \
        f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
        f'LINES TERMINATED BY \'\r\n\';'

    cursor.execute(sql)
    db.commit()

    os.remove(radio_steps_file)

    end = datetime.now()
    delta = end - start

    print(f'>>>>TIME FOR IMPORTING = {str(delta).split(".")[0]}<<<<')

    print('============>IMPORTING RADIO_STEPS FINISHED<============')
    telegram_send_message('============>IMPORTING RADIO_STEPS FINISHED<============')


def full_steps_table_csv_upload(passwords):
    print('============>IMPORTING FULL_STEPS STARTED<============')
    telegram_send_message('============>IMPORTING FULL_STEPS STARTED<============')

    start = datetime.now()

    query_radios = f'SELECT * FROM {passwords["sql_schema"]}.RADIOS_LINKS'
    query_metro = f'SELECT * FROM {passwords["sql_schema"]}.METRO_LINKS'

    db_connection_str = f'mysql+pymysql://{passwords["sql_user"]}:{passwords["sql_password"]}@10.26.82.160/' \
                        f'{passwords["sql_schema"]}'
    db_connection = create_engine(db_connection_str)

    df_links_radio = pd.read_sql(query_radios, con=db_connection)
    df_links_metro = pd.read_sql(query_metro, con=db_connection)

    df_links_metro.rename(columns={'SOURCE_NE': 'SOURCE_ELEMENT', 'TARGET_NE': 'TARGET_ELEMENT'}, inplace=True)

    df_links_radio['HOP_INDEX'] = 'RADIO_' + df_links_radio['HOP_INDEX'].astype(str)
    df_links_radio['SOURCE_INT'] = '-'
    df_links_radio['TARGET_INT'] = '-'
    df_links_radio['PEER'] = '-'
    df_links_radio['PREFERENTIAL'] = '-'
    df_links_radio['NAME_LITERAL_IDX'] = df_links_radio['SITE_NAME_LITERAL']
    df_links_radio['START_SOURCE'] = '-'

    site_names_metro_df = list(set(df_links_metro['NAME_LITERAL_IDX'].to_list()))
    site_names_radio_df = list(set(df_links_radio['NAME_LITERAL_IDX'].to_list()))

    radios_to_df = list(set(site_names_radio_df) - set(site_names_metro_df))

    last_site = ''

    db_columns = list(df_links_metro.columns)

    df_full = pd.DataFrame()

    for cur_radio_add in radios_to_df:

        df_add = df_links_radio[df_links_radio['SITE_NAME_LITERAL'] == cur_radio_add][db_columns].copy()

        add_metro = list(set(df_add[df_add['TARGET_ELEMENT'].str.contains('-gw')]['TARGET_ELEMENT'].to_list()))

        add_metro += list(set(df_add[df_add['SOURCE_ELEMENT'].str.contains('-gw')]['SOURCE_ELEMENT'].to_list()))

        df_add = df_add.append(df_links_metro[df_links_metro['START_SOURCE'].isin(add_metro)][db_columns].copy())

        df_add['SITE_NAME'] = df_add['SITE_NAME'].iloc[0]
        df_add['SITE_NAME_LITERAL'] = df_add['SITE_NAME_LITERAL'].iloc[0]
        df_add['NAME_LITERAL_IDX'] = df_add['SITE_NAME_LITERAL'].iloc[0]
        df_add['LINK_TYPE'] = 'Main'

        df_full = df_full.append(df_add, ignore_index=True)

    for cur_metro_add in site_names_metro_df:
        df_add = df_links_metro[df_links_metro['NAME_LITERAL_IDX'] == cur_metro_add][db_columns].copy()

        if not df_add.empty:
            find_site = df_add.iloc[0]['SITE_NAME_LITERAL']
            df_add['LINK_TYPE'] = 'Main'
        else:
            continue

        df_radio_main = df_links_radio[df_links_radio['SITE_NAME_LITERAL'] == find_site].copy()

        df_radio_main['LINK_TYPE'] = 'Main'

        df_add = df_add.append(df_radio_main, ignore_index=True)

        ne_names_find = list(set(df_add['SOURCE_ELEMENT'].to_list())) + list(set(df_add['TARGET_ELEMENT'].to_list()))

        ne_names_find = list(set(ne_names_find))

        df_radio_adj = pd.DataFrame()

        for cur_ne_find in ne_names_find:
            df_radio_adj = df_radio_adj.append(
                df_links_radio[df_links_radio['KEY_METRO'].str.contains(cur_ne_find)].copy(), ignore_index=True)

        df_radio_adj['LINK_TYPE'] = 'Adjacent'

        df_add = df_add.append(df_radio_adj, ignore_index=True)

        df_add['SITE_NAME'] = df_add.iloc[0]['SITE_NAME']
        df_add['SITE_NAME_LITERAL'] = df_add.iloc[0]['SITE_NAME_LITERAL']
        df_add['NAME_LITERAL_IDX'] = df_add.iloc[0]['NAME_LITERAL_IDX']

        df_add.drop_duplicates(inplace=True)

        df_full = df_full.append(df_add, ignore_index=True)

    df_full['HOP_INDEX'] = df_full['HOP_INDEX'].astype(str)
    df_full['HOP_INDEX'] = df_full['HOP_INDEX'].str.split('.', 0)
    df_full['HOP_INDEX'] = df_full['HOP_INDEX'].str[0]
    df_full.drop_duplicates(inplace=True)

    df_full['MEDIA_TX'] = df_full.apply(lambda row: get_media_tx(row), axis=1)

    df_full.drop(columns=['SITE_NAME_LITERAL', 'START_SOURCE', 'KEY_METRO'], inplace=True)

    db = mysql.connect(
        host="10.26.82.160",
        user=passwords['sql_user'],
        passwd=passwords['sql_password'],
        database=passwords['sql_schema'],
        allow_local_infile=True
    )
    cursor = db.cursor()

    exclude = 'truncate FULL_LINKS'
    cursor.execute(exclude)

    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))
    full_steps_file = os.path.abspath(os.path.join(csv_path, 'full_steps.csv'))

    df_full.to_csv(full_steps_file, mode='a', header=False, index=False)

    upload_steps_file = full_steps_file.replace('\\', '/')

    sql = f'LOAD DATA LOCAL INFILE "{upload_steps_file}" REPLACE INTO TABLE FULL_LINKS\n' \
        f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
        f'LINES TERMINATED BY \'\r\n\';'
    cursor.execute(sql)

    db.commit()
    os.remove(full_steps_file)

    end = datetime.now()
    delta = end - start

    print(f'>>>>TIME FOR IMPORTING = {str(delta).split(".")[0]}<<<<')

    print('============>IMPORTING FULL_STEPS FINISHED<============')
    telegram_send_message('============>IMPORTING FULL_STEPS FINISHED<============')
    telegram_send_message('Processo Finalizado Com sucesso!')

