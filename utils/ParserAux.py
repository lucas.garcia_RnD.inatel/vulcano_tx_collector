from utils.general import replace
import pandas as pd
from statistics import mean

def check_if_float(value):
    if type(value) == float:
        return value
    value = value.replace(',', '.')
    try:
        value = float(value)
        return str(value)
    except:
        return 'NULL'


def check_if_int(value):
    try:
        value = int(value)
        if value > 2147483647:
            value = 2147483647
        return str(value)
    except:
        return 'NULL'


def get_interfaces_from_df(content):
    del content[0]
    indices = [i for i, s in enumerate(content) if 'nterface' in s]
    header = content[indices[0]]
    columns = header.split()
    quantity = len(columns)
    del content[0:(indices[0] + 1)]
    df_content = [segments.split()[:quantity] for segments in content]
    df = pd.DataFrame(df_content, columns=columns)
    df.dropna(subset=[columns[1]], inplace=True)
    interfaces = df['Interface'].to_list()

    return df, interfaces


def get_huawei_transciever_infos(response):
    if any(x for x in ['WORKING RANGE', 'WARNING RANGE', 'NORMAL RANGE'] if x in response.upper()):
        response = replace('WORKING RANGE', 'WARNING RANGE', response)
        response = replace('NORMAL RANGE', 'WARNING RANGE', response)
    response = response.split('\n')
    response_warning = [x for x in response if 'WARNING RANGE' in x.upper()]
    response_power = [x for x in response if 'RX POWER' in x.upper()]

    #processando para GWC/GWD/HL5... onde há warning range
    if len(response_warning) == 2:
        response_rx = response_warning[0]
        response_rx = response_rx.split(',', 1)

        rx_power = response_rx[0]
        rx_power = rx_power.split(':')[1].strip().upper().replace('DBM', '')
        rx_sense = response_rx[1]
        rx_sense = rx_sense.split('[')[1].replace(']', '').split(',')
        rx_sense_low = rx_sense[0].upper().replace('DBM', '')
        rx_sense_high = rx_sense[1].upper().replace('DBM', '')

        response_tx = response_warning[1]
        response_tx = response_tx.split(',', 1)

        tx_power = response_tx[0]
        tx_power = tx_power.split(':')[1].strip().upper().replace('DBM', '')
        tx_sense = response_tx[1]
        tx_sense = tx_sense.split('[')[1].replace(']', '').split(',')
        tx_sense_low = tx_sense[0].upper().replace('DBM', '')
        tx_sense_high = tx_sense[1].upper().replace('DBM', '')
        tx_power = check_if_float(tx_power)
        tx_sense_high = check_if_float(tx_sense_high)
        tx_sense_low = check_if_float(tx_sense_low)
        rx_power = check_if_float(rx_power)
        rx_sense_high = check_if_float(rx_sense_high)
        rx_sense_low = check_if_float(rx_sense_low)

        rx_sense_low = 0 if 'inf' in rx_sense_low else rx_sense_low
        rx_power = 0 if 'inf' in rx_power else rx_power
        rx_sense_high = 0 if 'inf' in rx_sense_high else rx_sense_high
        tx_sense_low = 0 if 'inf' in tx_sense_low else tx_sense_low
        tx_sense_high = 0 if 'inf' in tx_sense_high else tx_sense_high
        tx_power = 0 if 'inf' in tx_power else tx_power

    #PROCESSANDO PARA HL4
    elif len(response_warning) == 1:
        #pick Rx power
        rx_list = []
        tx_list = []
        if response_power:
            for tx_rx in response_power:
                rx_list.append(float(tx_rx.split()[2].strip().upper().replace('DBM,','')))
                tx_list.append(float(tx_rx.split()[5].strip().upper().replace('DBM','')))

            rx_power = round(mean(rx_list),2)
            tx_power = round(mean(tx_list),2)
        else:
            rx_power = 'NULL'
            tx_power = 'NULL'

        response_rx = response_warning[0].upper().replace('DBM,', 'DBM@')
        response_rx = response_rx.split('@')

        #define rx senses
        rx_sense = response_rx[0].split('[')[1].replace(']', '').split(',')
        rx_sense_low = rx_sense[0].upper().replace('DBM', '').strip()
        rx_sense_high = rx_sense[1].upper().replace('DBM', '').strip()

        #define tx senses
        tx_sense = response_rx[1].split('[')[1].replace(']', '').split(',')
        tx_sense_low = tx_sense[0].upper().replace('DBM', '').strip()
        tx_sense_high = tx_sense[1].upper().replace('DBM', '').strip()

        #check float numbers
        tx_power = check_if_float(tx_power)
        tx_sense_high = check_if_float(tx_sense_high)
        tx_sense_low = check_if_float(tx_sense_low)
        rx_power = check_if_float(rx_power)
        rx_sense_high = check_if_float(rx_sense_high)
        rx_sense_low = check_if_float(rx_sense_low)

        rx_sense_low = 0 if 'inf' in rx_sense_low else rx_sense_low
        rx_sense_high = 0 if 'inf' in rx_sense_high else rx_sense_high
        tx_sense_low = 0 if 'inf' in tx_sense_low else tx_sense_low
        tx_sense_high = 0 if 'inf' in tx_sense_high else tx_sense_high


    elif [x for x in response if 'RX POWER' in x.upper()]:
        response_rx = [x for x in response if 'RX POWER' in x.upper()][0]
        response_rx = response_rx.split(',', 1)
        rx_power = response_rx[0]
        rx_power = rx_power.split(':')[1].strip().upper().replace('DBM', '')
        tx_power = response_rx[1]
        tx_power = tx_power.split(':')[1].strip().upper().replace('DBM', '')

        tx_sense_high = tx_sense_low = rx_sense_high = rx_sense_low = 'NULL'
    elif [x for x in response if 'RX OPTICAL POWER' in x.upper()]:
        response_rx = [x for x in response if 'RX OPTICAL POWER' in x.upper()][0]
        response_rx = response_rx.split(',', 1)
        rx_power = response_rx[0]
        rx_power = rx_power.split(':')[1].strip().upper().replace('DBM', '')
        tx_power = response_rx[1]
        tx_power = tx_power.split(':')[1].strip().upper().replace('DBM', '')

        tx_sense_high = tx_sense_low = rx_sense_high = rx_sense_low = 'NULL'
    else:
        tx_power = tx_sense_high = tx_sense_low = rx_power = rx_sense_high = rx_sense_low = 'NULL'

    return tx_power, tx_sense_high, tx_sense_low, rx_power, rx_sense_high, rx_sense_low


def get_int_type_huawei(response):
    response = replace('Optical transceiver is off line', 'Optical transceiver is offline', response)
    response = replace('Optical transceiver is off-line', 'Optical transceiver is offline', response)
    if 'TWISTED' in response.upper():
        return 'Electrical'
    elif 'SINGLEMODE' in response.upper():
        return 'Optical'
    elif 'SINGLE MODE' in response.upper():
        return 'Optical'
    elif 'SINGLE-MODE' in response.upper():
        return 'Optical'
    elif 'MULTIMODE' in response.upper():
        return 'Optical'
    elif 'MULTI MODE' in response.upper():
        return 'Optical'
    elif 'MULTI-MODE' in response.upper():
        return 'Optical'
    elif 'MULTMODE' in response.upper():
        return 'Optical'
    elif 'MULT MODE' in response.upper():
        return 'Optical'
    elif 'MULT-MODE' in response.upper():
        return 'Optical'
    elif 'Optical transceiver is offline' in response:
        return 'Empty'
    elif 'UNKNOWN' in response.upper():
        return 'Unknown'
    else:
        return 'Logical'


def get_int_desc_huawei(response):
    response = response.split('\n')
    response = [x for x in response if 'escription' in x]
    if len(response) >= 1:
        response = response[0]
    else:
        return 'N/A'
    response = response.split(':')
    if len(response) >= 2 and response[1].strip():
        return response[1].strip().replace("\'", '')
    else:
        return 'N/A'


def get_int_mac_huawei(response):
    response = response.split('\n')
    response = [x for x in response if 'Hardware address is ' in x]
    if len(response) >= 1:
        response = response[0]
    else:
        return 'N/A'
    response = response.split('Hardware address is ')
    if len(response) >= 2:
        return response[1].strip()
    else:
        return 'N/A'


def get_int_speed_huawei(response):
    if 'CURRENT BW' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'CURRENT BW' in x.upper()]

    else:
        response = response.split('\n')
        response = [x for x in response if 'BW' in x.upper()]

    if len(response) >= 1:
        response = response[0]
    else:
        return 'N/A'

    if 'CURRENT' in response.upper():
        response = response.split(',')
        response = [x for x in response if 'CURRENT' in x.upper()][0]
        return response.split(':')[1].strip()

    elif 'PORT BW' in response.upper():
        response = response.split(',')
        response = [x for x in response if 'PORT BW' in x.upper()][0]
        return response.split(':')[1].strip()


def get_int_negotiation_huawei(response):
    if any(x for x in ['EGOTIATION:', 'EGOTIATION :'] if x in response.upper()):
        response = response.split('\n')
        response = [x for x in response if 'EGOTIATION' in x.upper()][0]
        response = response.split(',')
        response = [x for x in response if 'EGOTIATION' in x.upper()][0]
        return response.split(':')[1].strip()
    elif any(x for x in ['LINK TYPE', 'LINK-TYPE'] if x in response.upper()):
        response = replace('LINK-TYPE', 'LINK TYPE', response)
        response = response.split('\n')
        response = [x for x in response if 'LINK TYPE' in x.upper()][0]
        response = response.split(',')
        response = [x for x in response if 'LINK TYPE' in x.upper()][0]
        return response.split(':')[1].strip()
    else:
        return None


def get_int_mtu_huawei(response):
    if 'MAXIMUM TRANSMIT UNIT' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'MAXIMUM TRANSMIT UNIT' in x.upper()][0]
        response = response.split(',')
        response = [x for x in response if 'MAXIMUM TRANSMIT UNIT' in x.upper()][0]
        if response.strip().split(' ')[-1].upper() == 'BYTES':
            response = response.strip().split(' ')[-2]
        else:
            response = response.strip().split(' ')[-1]
        response = check_if_int(response)
        return str(response)
    else:
        return 'NULL'


def get_int_ip_add(response):
    if 'INTERNET ADDRESS IS' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'INTERNET ADDRESS IS' in x.upper()][0]
        response = response.strip().split(' ')[-1]
        if '(' in response:
            response = response.split('(')[1]
        return response.split('/')[0]
    else:
        return 'unassigned'


def get_cisco_transciever_df(child):
    child.sendline('show interface transceiver detail')
    child.expect('#', timeout=900)
    content = child.before.split('\n')
    columns = ['Interface', 'Optical Power', 'High Alarm', 'High Warn', 'Low Warn', 'Low Alarm']
    indices = [i for i, s in enumerate(content) if '----' in s]
    if not len(indices) > 0:
        df_content = []
        df_rx = pd.DataFrame(df_content, columns=columns)
        df_tx = pd.DataFrame(df_content, columns=columns)

        return df_tx, df_rx

    indices = indices[-2:]
    list_tx = content[indices[0]+1:indices[1]]
    empty_line = [i for i, s in enumerate(list_tx) if s == '\r'][0]
    list_tx = list_tx[:empty_line]
    df_content = [segments.split()[:6] for segments in list_tx]
    df_tx = pd.DataFrame(df_content, columns=columns)
    list_rx = content[indices[1]+1:]
    empty_line = [i for i, s in enumerate(list_rx) if s == '\r'][0]
    list_rx = list_rx[:empty_line]
    df_content = [segments.split()[:6] for segments in list_rx]
    df_rx = pd.DataFrame(df_content, columns=columns)

    return df_tx, df_rx


def get_int_type_cisco(response):
    response = response.split('\n')
    response_ne = [x for x in response if 'MEDIA TYPE IS' in x.upper()]
    if response_ne:
        negotiation = response_ne[0].split(',')[-2]
        negotiation = replace('link type is', '', negotiation)
        negotiation = negotiation.strip()
        response_ne = response_ne[0]
        ne_type_analysis = response_ne.split(',')[-1].strip()
        if 'UNKNOWN' in ne_type_analysis.upper():
            ne_type = 'Unknown'
        else:
            ne_type = response_ne.strip().split(' ')[-1].strip()

            if ne_type.upper() == 'IS':
                ne_type = 'Empty'
            elif 'LX' in ne_type.upper():
                ne_type = 'Optical'
            elif 'ZX' in ne_type.upper():
                ne_type = 'Optical'
            elif 'EX' in ne_type.upper():
                ne_type = 'Optical'
            elif 'SX' in ne_type.upper():
                ne_type = 'Optical'
            elif '-SR' in ne_type.upper():
                ne_type = 'Optical'
            elif '-ER' in ne_type.upper():
                ne_type = 'Optical'
            elif '-LR' in ne_type.upper():
                ne_type = 'Optical'
            elif '-ZR' in ne_type.upper():
                ne_type = 'Optical'
            elif 'SFP-' in ne_type.upper():
                ne_type = 'Optical'
            else:
                ne_type = 'Electrical'

    elif [x for x in response if 'LINK TYPE IS' in x.upper()]:
        response_ne = [x for x in response if 'LINK TYPE IS' in x.upper()]
        negotiation = response_ne[0].split(',')
        negotiation = [x for x in negotiation if 'LINK TYPE IS' in x.upper()][0]
        negotiation = replace('link type is', '', negotiation)
        negotiation = negotiation.strip()

        ne_type = 'N/A'

    else:
        ne_type = 'N/A'
        negotiation = 'N/A'

    return ne_type, negotiation


def get_int_mac_cisco(response):
    response = response.split('\n')
    response = [x for x in response if ', address is ' in x]
    if len(response) >= 1:
        response = response[0]
        response = response.split(', address is')[1]
        response = response.strip().split(' ')[0]
        return response.strip()
    else:
        return 'N/A'


def get_int_utilization_cisco(response):
    if any(x for x in ['RXLOAD', 'RX LOAD'] if x in response.upper()):
        response = replace('RX LOAD', 'RXLOAD', response)
        response = replace('TX LOAD', 'TXLOAD', response)
        response = response.split('\n')
        response = [x for x in response if 'RXLOAD' in x.upper()][0]
        response = response.split(',')
        response_rx = [x for x in response if 'RXLOAD' in x.upper()][0]
        rx_uti = response_rx.split(' ')[-1].strip()
        response_tx = [x for x in response if 'TXLOAD' in x.upper()][0]
        tx_uti = response_tx.split(' ')[-1].strip()
        return rx_uti, tx_uti
    else:
        return None, None


def get_int_rx_error_cisco(response):
    if 'INPUT ERROR' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'INPUT ERROR' in x.upper()][0]
        response = response.split(',')
        response = [x for x in response if 'INPUT ERROR' in x.upper()][0]
        response = response.strip().split(' ')[0]
        response = check_if_int(response)
        return str(response)
    else:
        return 'NULL'


def get_int_tx_error_cisco(response):
    if 'OUTPUT ERROR' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'OUTPUT ERROR' in x.upper()][0]
        response = response.split(',')
        response = [x for x in response if 'OUTPUT ERROR' in x.upper()][0]
        response = response.strip().split(' ')[0]
        response = check_if_int(response)
        return str(response)
    else:
        return 'NULL'


def get_int_mtu_cisco(response):
    if 'MTU' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'MTU' in x.upper()][0]
        response = response.split(',')
        response = [x for x in response if 'MTU' in x.upper()][0]
        if response.strip().split(' ')[-1].upper() == 'BYTES':
            response = response.strip().split(' ')[-2]
        else:
            response = response.strip().split(' ')[-1]
        response = check_if_int(response)
        return str(response)
    else:
        return 'NULL'


def get_int_desc_cisco(response):
    if 'DESCRIPTION' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'DESCRIPTION' in x.upper()][0]
        response = replace('DESCRIPTION', 'DESCRIPTION', response)
        response = response.strip().split('DESCRIPTION:')[-1].strip()

        return response
    else:
        return 'NULL'


def get_duplex_info(response):
    response = replace('FULL DUPLEX', 'FULL-DUPLEX', response)
    response = replace('HALF DUPLEX', 'HALF-DUPLEX', response)
    response = replace('FULL-DUPLEX', 'FULL-DUPLEX', response)
    response = replace('HALF-DUPLEX', 'HALF-DUPLEX', response)

    if 'FULL-DUPLEX' in response:
        return 'Full-duplex'
    elif 'HALF-DUPLEX' in response:
        return 'Half-duplex'
    else:
        return 'N/A'


def get_peers_cisco(response):
    response = response.split('\n')
    response = response[:-1]
    header_index = [i for i, x in enumerate(response) if 'NEIG' in x.upper()]
    if header_index:
        header_index = header_index[0]
        response_lines = response[header_index + 1:]
        response_lines = [x for x in response_lines if len(x.split()) >= 2]
        peers = [x.split()[0] for x in response_lines]
    else:
        peers = []

    return peers


def get_int_and_cost_cisco(response, vendor):
    response = response.split('\n')
    interface = [x for x in response if ', VIA' in x.upper()]
    if interface:
        interface = interface[0]
        interface = interface.split(', via')
        interface = interface[-1].split(',')[0].strip()
    else:
        interface = None

    cost = [x for x in response if 'METRIC' in x.upper()]
    if cost:
        cost = cost[0]

        if vendor == 'TELLABS':
            cost = cost.split(',')
            cost = [x for x in cost if 'METRIC' in x.upper()][0]

        cost = cost.strip().split()[-1]

    else:
        cost = 'NULL'

    cost = check_if_int(cost)

    return interface, cost


def get_peers_huawei(response):
    response = response.split('\n')
    response = response[:-1]
    header_index = [i for i, x in enumerate(response) if 'PREFRCV' in x.upper()]
    if header_index:
        header_index = header_index[0]
        response_lines = response[header_index + 1:]
        response_lines = [x for x in response_lines if len(x.split()) >= 2]
        peers = [x.split()[0] for x in response_lines]
    else:
        peers = []

    return peers


def get_int_and_cost_huawei(response):
    response = response.split('\n')
    response = response[:-1]
    header_index = [i for i, x in enumerate(response) if 'ESTINATIO' in x.upper()]
    if header_index:
        header_index = header_index[0]
        response_lines = response[header_index + 2:]
        response_lines = [x for x in response_lines if len(x.split()) >= 2]
        interface = [x.strip().split()[-1] for x in response_lines][0]
        cost = [x.strip().split()[3] for x in response_lines][0]
    else:
        interface = cost = 'NULL'

    cost = check_if_int(cost)

    return interface, cost


def get_mpls_ip_and_id(response):
    if 'MPLS L2VC' in response.upper():
        response = response.split('\n')
        response = [x for x in response if 'MPLS L2VC' in x.upper()][0]
        response = response.strip().split()
        if '.' in response[-2]:
            return str(response[-2]), str(response[-1])
        else:
            return 'NULL', 'NULL'
    else:
        return 'NULL', 'NULL'


def get_int_mtu_tellabs(response):
    mtu = [x for x in response if 'MTU' in x.upper()]

    if mtu:
        mtu = mtu[0]
        mtu = mtu.split('MTU')[-1]
        mtu = ''.join([x for x in mtu if x.isdigit()])
    else:
        mtu = 0

    return mtu


def get_int_mac_tellabs(response):
    mac = [x for x in response if 'MAC' in x.upper()]

    if mac:
        mac = mac[0]
        mac = mac.split(',')[0]
        mac = mac.split('(')[0]
        mac = mac.split()[-1].strip()
    else:
        mac = '-'

    return mac


def get_int_type_tellabs(response):
    int_type = [x for x in response if 'CONNECTOR' in x.upper()]

    if int_type:
        int_type = int_type[0]
        if 'RJ45' in int_type.upper():
            int_type = 'Electrical'
        elif 'DUPLEX' in int_type.upper():
            int_type = 'Optical'
        elif 'UNKNOWN' in int_type.upper():
            int_type = '-'
    else:
        int_type = '-'

    return int_type


def get_int_speed_tellabs(response):
    speed = [x for x in response if 'BANDWIDTH:' in x.upper()]

    if speed:
        speed = speed[0]
        speed = speed.split()[-1].strip()
        if speed == '1000M':
            speed = '1G'
    else:
        speed = '-'

    return speed


def get_int_negotiation_tellabs(response):
    negotiation = [x for x in response if 'AUTO NEG:' in x.upper()]

    if negotiation:
        negotiation = negotiation[0]
        negotiation = negotiation.split(',')[0].strip()
        negotiation = negotiation.split(':')[-1].strip()
        if negotiation.upper() == 'ENABLE':
            negotiation = 'auto'
        else:
            negotiation = 'disable'
    else:
        negotiation = '-'

    return negotiation
