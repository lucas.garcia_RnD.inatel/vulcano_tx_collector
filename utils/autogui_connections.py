import pyautogui
import time
from utils.NetmikoGeneral import autogui_wait, autogui_error, autogui_right_click, autogui_left_click
from utils.telegram_bot import telegram_send_message



def autogui_vendor_check(gesa_id):
    # ALERT SCREEN CHECK
    if pyautogui.locateOnScreen(f'prints\\general\\login_alert_screen.PNG', confidence=0.9):
        print(f'Alert Screen!')
        pyautogui.hotkey('y')
        time.sleep(0.5)
        pyautogui.hotkey('enter')
        time.sleep(1)
    # VENDOR CHECK
    if pyautogui.locateOnScreen('prints\\HUAWEI\\able_terminal.PNG', confidence=0.9):
        if pyautogui.locateOnScreen('prints\\general\\coriant.PNG', confidence=0.9):
            return 'TELLABS'
        elif pyautogui.locateOnScreen('prints\\general\\tellabs.PNG', confidence=0.9):
            return 'TELLABS'
        else:
            return 'HUAWEI'
    elif pyautogui.locateOnScreen('prints\\CISCO\\able_terminal.PNG', confidence=0.9):
        return 'CISCO'
    elif pyautogui.locateOnScreen('prints\\NOKIA_ALCATEL\\able_terminal.PNG', confidence=0.9):
        return 'NOKIA_ALCATEL'


    else:
        autogui_error(gesa_id, "", "Fail on real vendor check!")
        return None

def direct_connect_check():
    # ALERT SCREEN CHECK
    if pyautogui.locateOnScreen(f'prints\\general\\login_alert_screen.PNG', confidence=0.9):
        print(f'Alert Screen!')
        pyautogui.hotkey('y')
        time.sleep(0.5)
        pyautogui.hotkey('enter')
        time.sleep(2)
    # VENDOR CHECK
    if pyautogui.locateOnScreen('prints\\HUAWEI\\able_terminal.PNG', confidence=0.9):
        if pyautogui.locateOnScreen('prints\\general\\coriant.PNG', confidence=0.9):
            return True
        elif pyautogui.locateOnScreen('prints\\general\\tellabs.PNG', confidence=0.9):
            return True
        else:
            return True
    elif pyautogui.locateOnScreen('prints\\CISCO\\able_terminal.PNG', confidence=0.9):
        return True
    elif pyautogui.locateOnScreen('prints\\NOKIA_ALCATEL\\able_terminal.PNG', confidence=0.9):
        return True

    else:
        return False

def ssh_connect(master_user,master_password,ip,gesa_id,local_user):

    # TENTATIVA POR SSH
    pyautogui.typewrite(f'ssh {master_user}@{ip}')
    pyautogui.hotkey('enter')

    #checa tela yes/no
    if autogui_wait(f'general\\yes_no.PNG', 2):
        print(f'Yes/No Screen!')
        pyautogui.hotkey('yes')
        time.sleep(0.5)
        pyautogui.hotkey('enter')
        time.sleep(2)

    # SE ENCONTRA PASSWORD NA TELA PROSSEGUE
    if autogui_wait(f'general\\password.PNG', 2):
        time.sleep(0.5)
        pyautogui.typewrite(master_password)
        pyautogui.hotkey('enter')
        time.sleep(5)
        # VENDOR CHECK
        print(f'Start Real vendor check...')
        # Retorna o vendor de acordo com o able_terminal, caso não tiver na lista retorna None
        real_vendor = autogui_vendor_check(gesa_id)
        if not real_vendor:
            print(f"Password Error!")
            autogui_clear_screen(gesa_id)
            return None, None
        else:
            print(f'Real vendor = {real_vendor}')
            if local_user:
                #se usou user/pass direto muda a variavel
                return real_vendor, 'SSH [Local User]'
            else:
                return real_vendor, "SSH"
    else:
        autogui_error(gesa_id, "", "Fail to connect!")
        autogui_clear_screen(gesa_id)
        return None, None

def telnet_connect(master_user,master_password,ip,gesa_id,local_user):

    # TENTATIVA POR TELNET
    pyautogui.typewrite(f'telnet {ip}')
    pyautogui.hotkey('enter')
    time.sleep(0.5)

    #checa tela yes/no
    if autogui_wait(f'general\\yes_no.PNG', 2):
        print(f'Yes/No Screen!')
        pyautogui.hotkey('yes')
        time.sleep(0.5)
        pyautogui.hotkey('enter')
        time.sleep(2)

    # checando se a tela de login se aparecer password primeiro
    if autogui_wait('general\\password.PNG', 2):
        time.sleep(0.5)
        pyautogui.typewrite(master_password)
        pyautogui.hotkey('enter')
        time.sleep(5)
        # VENDOR CHECK
        print(f'Start Real vendor check...')
        # Retorna o vendor de acordo com o able_terminal, caso não tiver na lista retorna None
        real_vendor = autogui_vendor_check(gesa_id)
        if not real_vendor:
            print(f"Password Error!")
            autogui_clear_screen(gesa_id)
            return None, None
        else:
            print(f'Real vendor = {real_vendor}')
            if local_user:
                #se usou user/pass direto muda a variavel
                return real_vendor, 'Telnet [Local User]'
            else:
                return real_vendor, "Telnet"
    # checando se a tela de login se aparecer username ou Login primeiro
    if autogui_wait('general\\Login.PNG', 2):
        pass
    else:
        if autogui_wait('general\\username.PNG', 2):
            pass
        else:
            # checar se conectou automaticamente
            connect_direct = direct_connect_check()
            if connect_direct:
                print('Connected directly!')
                print(f'Start Real vendor check...')
                time.sleep(5)
                # Retorna o vendor de acordo com o able_terminal, caso não tiver na lista retorna None
                real_vendor = autogui_vendor_check(gesa_id)
                if not real_vendor:
                    autogui_clear_screen(gesa_id)
                    return None, None
                else:
                    print(f'Real vendor = {real_vendor}')
                    if local_user:
                        # se usou user/pass direto muda a variavel
                        return real_vendor, 'Telnet [Local User]'
                    else:
                        return real_vendor, "Telnet"
            else:
                print(f'Fail to connect!')
                autogui_error(gesa_id, "", "Telnet NOK")
                autogui_clear_screen(gesa_id)
                return None, None

    #somente para user/pass local
    if local_user:
        #insere user
        pyautogui.typewrite(master_user)
        pyautogui.hotkey('enter')

        if autogui_wait('general\\password.PNG', 2):
            time.sleep(0.5)
            pyautogui.typewrite(master_password)
            pyautogui.hotkey('enter')
            time.sleep(5)
            # VENDOR CHECK
            print(f'Start Real vendor check...')
            # Retorna o vendor de acordo com o able_terminal, caso não tiver na lista retorna None
            real_vendor = autogui_vendor_check(gesa_id)
            if not real_vendor:
                print(f"Password Error!")
                autogui_clear_screen(gesa_id)
                return None, None
            else:
                print(f'Real vendor = {real_vendor}')
                if local_user:
                    # se usou user/pass direto
                    return real_vendor, 'Telnet [Local User]'
                else:
                    return real_vendor, "Telnet"
        else:
            autogui_error(gesa_id, "", "Fail to connect!")
            autogui_clear_screen(gesa_id)
            return None, None
    else:
        return None, None

def autogui_clear_screen(gesa_id):
    time.sleep(0.5)
    pyautogui.keyDown('ctrl')
    pyautogui.press('l')
    pyautogui.keyUp('ctrl')
    time.sleep(0.5)
    if autogui_right_click(f'general\\putty_bar_{gesa_id}.PNG', 10):
        pass
    else:
        if autogui_left_click('general\\clear_screen.PNG', 10):
            pass
        else:
            print('FATAL ERROR: putty not found! Program stopped')
            telegram_send_message(f'FATAL ERROR: putty not found! Program stopped')
            exit()
    time.sleep(1.5)
    if autogui_left_click('general\\clear_screen.PNG', 10):
        pass