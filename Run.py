import traceback

from utils.NetmikoFunctions import get_grep
from utils.NetmikoFunctions import run_attempt
from utils.General_Functions import get_passwords
from utils.csv_parser import run_csv_generation
from utils.csv_upload import ne_metro_csv_upload, interface_csv_upload, mac_csv_upload, routing_table_csv_upload, \
    full_routing_table_csv_upload, simple_chart_table_csv_upload, metro_steps_table_csv_upload, \
    radio_steps_table_csv_upload, full_steps_table_csv_upload, restore_auto_progess

from utils.telegram_bot import *

# limpando log
os.remove('logger/Netmiko.log')
# gerando log
logging.basicConfig(filename='logger/Netmiko.log', level=logging.DEBUG)
logger = logging.getLogger("netmiko")

''' 

Aplicação com foco em coletar a parametrização da rede metro, processar e carregar no banco de dados.
A aplicação é dividida em algumas partes sendo elas:


Grep: Responsável por listar os elementos no jump_server filtra-los e guardalos na tabela AUTOMATION_PROGRESS,
esta que é responsável pelo gerenciamento de cada coleta. Sendo ela onde guardamos o progresso da coleta atual
informando se o canal de OMCH está ok, qual tipo de conexão e se a coleta foi realizada.


Collect: a função da coleta é utilizar a lib Netmiko para conexão com o jump-server e a partir daí conectar caixa
a caixa para que possamos coletar os devidos logs.
A principal função é a Run_attempt que acessa caixa a caixa coletango os logs e salvando no txt e atualização do
csv NE_METRO que salva as informações mais detalhadas de cada caixa.

csv_generate: responsável por analisar todos os logs e gerar as tabelas primarias que serão carregados no banco.


'''

# define flags para rodar
restore_automation_progess = False
grep = True
collect = True
csv_generate = True
csv_upload = True
chart_update = True


''' 
define lista de sites para teste - default FALSE
caso queira rodar uma tentativa de coleta somente nos sites listados abaixo
configurar a flag para True e colocar o nome dos sites na lista, os sites devem estar com
COLLECTED NULL no banco.
'''
test = {'teste': False,
        'test_list': ['m-br-rj-rjo-bdi-gws-01', 'm-br-rj-rjo-mhe-gws-01', 'm-br-rj-rjo-ecp-gws-01',
                      'm-br-rj-rjo-bbt-gws-01', 'm-br-rj-vrd-rtt-gws-02']}

if __name__ == '__main__':

    # carregando passwords/schema da configuração
    passwords = get_passwords()

    # processando coleta em todos os elementos
    try:

        # restaurar progresso salvo no backup (colocar o nome do backup que deseja restaurar)
        if restore_automation_progess:
            restore_auto_progess(passwords, 'AUTOMATION_PROGRESS_2022-09-23_092320.csv')

        # coleta grep da região
        if grep:
            ufs = ['sp']
            vendors = ['HUAWEI', 'CISCO', 'TELLABS', 'CORIANT']
            get_grep(ufs, vendors, passwords, reset_process=True, only_fusion=False)
            exit()

        if collect:
            # tentativa 1
            vendors = ['HUAWEI', 'CISCO']
            run_attempt(passwords, 1, vendors, test, definitive=True)
            # tentativa 2 (apenas tellabs)
            # run_attempt(passwords, 2, vendors, test, definitive=True)
            # tentativa 3 (não esta funcionando)
            # run_attempt(passwords, 3, vendors)

            # sobe csv ne_metro no banco
            ne_metro_csv_upload(passwords)

        if csv_generate:
            run_csv_generation(passwords)

        if csv_upload:
            # telegram_send_message('CSV Upload Start!')
            interface_csv_upload(passwords)
            mac_csv_upload(passwords)
            routing_table_csv_upload(passwords)
            full_routing_table_csv_upload(passwords)

        if chart_update:
            # telegram_send_message('Chart Update Start!')
            simple_chart_table_csv_upload(passwords)
            metro_steps_table_csv_upload(passwords)
            radio_steps_table_csv_upload(passwords)
            full_steps_table_csv_upload(passwords)

    except Exception as e:
        traceback.print_tb(e.__traceback__)
        telegram_send_message('Not mapped error... Execution canceled!\nPlease Check the log')
        print(f'Error {e} \nThen Finish')
