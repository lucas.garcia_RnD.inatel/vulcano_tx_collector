SELECT * FROM tx_ne_param_revolution.AUTOMATION_PROGRESS where vendor = 'cisco';

truncate tx_ne_param_revolution.AUTOMATION_PROGRESS;
insert into tx_ne_param_revolution.AUTOMATION_PROGRESS
	SELECT ip, NE_NAME, vendor,
		null as COLLECTED,
		0 as COL_ATTEMPT,
		null as CONNECTED_VIA,
		null as GESA,
		null as OM_LINK,
		CURDATE() as STARTED
	FROM tx_ne_param.NE_METRO where ne_name like "%-hl4-%" and VENDOR like "hua%" and OM_LINK = 'true'  limit 2;

# tellabs
select * FROM tx_ne_param.NE_METRO where VENDOR like "tel%" and OM_LINK = 'true' and CONNECTED_VIA = 'SSH' limit 2;
select * FROM tx_ne_param.NE_METRO where ne_name like "%-gwc-%" and VENDOR like "tel%" and OM_LINK = 'true' and CONNECTED_VIA = 'SSH'  limit 2;
# hauwei
select * FROM tx_ne_param.NE_METRO where ne_name like "m-br-%" and VENDOR like "hua%" and OM_LINK = 'true'  limit 5;
select * FROM tx_ne_param.NE_METRO where ne_name like "%-gwc-%" and VENDOR like "hua%" and OM_LINK = 'true'  limit 5;
select * FROM tx_ne_param.NE_METRO where ne_name like "i-br-%" and VENDOR like "hua%" and OM_LINK = 'true'  limit 5;
select * FROM tx_ne_param.NE_METRO where ne_name like "%-hl4-%" and VENDOR like "hua%" and OM_LINK = 'true'  limit 2;
# cisco
select * FROM tx_ne_param.NE_METRO where ne_name like "%-gwd%" and VENDOR like "cis%" and OM_LINK = 'true'  limit 5;
select * FROM tx_ne_param.NE_METRO where ne_name like "i-br-%" and VENDOR like "cis%" limit 5;
select * FROM tx_ne_param.NE_METRO where ne_name like "m-br-%" and VENDOR like "cis%" limit 10;
select * FROM tx_ne_param.NE_METRO where VENDOR like "cis%" and OM_LINK = 'true'  limit 2;
